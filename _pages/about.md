---
layout: single
title: About IOSA.it
tags: [about]
modified: 2014-08-08T20:53:07.573882-04:00
permalink: /about/
---

The IOSA project started in October, 2004 as an experimental research about the application of ICT to archeological problems, mostly focusing on these particular aspects of this large field of studies:

- Free/Libre Open Source Software, its actual (and potential) capabilites for enhancing lots of the steps usually performed during a complete archaeological research project.
- Free and open formats and standards, including the XML language family (SVG vector graphics, OASIS OpenDocument, X3D), PNG raster and more, which should give the users the freedom to choose the applications they like, without regard to data loss or compatibility problems.
- Open Access to scientific and scholarly publications, which includes the developing Science Commons initiative and together aims to getting archaeologists to publish more often their results and data, thus giving better chances to the development of the whole studies.

The original project goals are:

- a greater and better use of computers in archaeological research, also through better knowledge and consciousness;
- the spreading of open source not just as software, but as a philosophy too, which is similar to the scientific research model, and therefore is suitable to it;
- the education to the use of open source software, both generic software and scientific software;
- to promote open standards that are thought for being exchanged on the web, which represents a good way for sharing and pubblication of research results, at lower cost than traditional methods;
- to give students the opportunity to compare between open source software and proprietary software they use everyday, on ready-to-use computers, with generic and scientific software installed;
- to start archaeological research projects in which open source software and philosophy are part of the original design and not afterwards applied to it;
- to collect archaeologists who are interested in the use of free/libre open source software, through a web site that should work as a portal and discussion forum.

As you can see, this website is only part of a larger project, that takes place mainly at our headquarter in Genoa, Italy. We are currently setting up a laboratory including 5 workstations and a local network, where:

- we will be able to test many of the applications which are listed in our software directory, and give you some detailed info on what could be really useful for your purposes. This would be a great difference between the current, simple directory and a complete guide.
- We're gonna have enough room to keep some lesson for students, researchers, employees or whoever would like to know more about real use of open source software.
- Students will be able to spend their stages experiencing in some particular task, be it GIS, 3D modelling, web publishing, database administration, statistics.

But why are we doing all this? Couldn't we simply get along with some pieces of software, managing to get something out of them, and possibly not losing too much time on it? And what's all that speaking about freedom?

We decided to start this project with some clear, plain ideas in mind. We really do believe that there are a lot of similarities between free software development and scientific research so, despite some people claiming archaeology is not a science, we find natural and obvious to choose free software as our tool.
When we speak about freedom, it's not an abstract concept in our minds: it is your freedom to have the best tools that fit your needs, and modify them to have them better and better, and redistribute so everybody can take advantage of your work, just like you can do with others' ones.
We firmly believe that a lot of advantages can come to archaeology this way, resulting in a more scientific approach, better exchange with other disciplines that are already forward on the way, easier spreading of data and results.
