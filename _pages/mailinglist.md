---
layout: single
title: IOSA.it discussion list
tags: [about]
modified: 2014-08-08T20:53:07.573882-04:00
comments: true
permalink: /mailinglist/
---

There's a new mailing list, that can be easily accessed at <http://list.iosa.it> ( subscribe from there or just by sending an e-mail message to <mailto:archaeology-subscribe@lists.linux.it> ).
List archives of previous posts can be accessed freely at <http://lists.linux.it/pipermail/archaeology/> .
This list was created after the workshop in Grosseto to keep together everyone - be her/him archaeologist, informatic, or anything else - who has got interest in these topics that we focus on:

- archaeological computing, archaeoinformatics, quantitative methods and other similar topics
- discussion about the stardards for archaeological record and documentation
- free/open source software, open formats and standards, open access to scientific literature and data sharing

Currently there are 52 subscribers from all across Europe. The more we are, the more our discussion can be useful to the community and wide-minded. For such reasons, the main language will be english. Other languages will be accepted if really needed (provided that someone can translate in english for the others). The mailing list is kindly hosted for free by Italian Linux Society on their Mailman platform.

## Subscribing

Write to <mailto:archaeology-subscribe@lists.linux.it> . An empty message will do the work.

## Archives

Visit the list archives at <http://lists.linux.it/pipermail/archaeology/>
