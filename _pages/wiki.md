---
title: Quantitative Archaeology Wiki
permalink: /wiki/
sidebar:
  nav: "wiki"
---


This is the content that was previously published at the Quantitative Archaeology Wiki. These pages are not a wiki but the name is kept for consistency.

Please note that most of the content in this section is obsolete. Check the [CRAN Task View: Archaeological Science](https://github.com/benmarwick/ctv-archaeology) by Ben Marwick for a broad overview of current R usage in archaeology.
{: .notice--warning}

## Digging Numbers

Follow the exercises for the textbook "Digging Numbers" by Fletcher
and Lock using the R statistical software.

[Go to the Digging Numbers exercises](diggingnumbers){: .btn .btn--primary .btn--x-large}

## Sum of individual weighted means

This is a method to calculate with enhanced precision the chronological
distribution of archaeological artefacts that have long time spans. ☛
[Go to the weighted means page](sum_of_individual_weighted_means)

## Contingency Tables

This is a basic introduction to frequency and contingency tables using
R. A function for plotting Ford "battleship" diagrams is
presented. ☛ [Go to the contingency tables page](contingency tables)

## Spatial Analysis

Tutorials about spatial analysis techniques using free geospatial
software like GRASS, R and others. ☛ [Go to the spatial analysis
introduction page](spatial_analysis/)

## Plotting quantitative pottery data

☛ [Go to the quantitative pottery data page](pottery/)

## Archaeometry

Statistical analysis is very often already included in the archaeometric
study of materials. Here we try to perform some common analysis with the
R programming language. ☛ [Go to the archaeometry
page](archaeometry/)

## Quantitative Archaeology summer school

Some brief notes taken at the [I-QMDAA Summer School](qmdaa/) in
2006.

Bibliography
------------

We collected references to published manuals and journals about
Quantitative Archaeology [in this Zotero collection](https://www.zotero.org/steko/collections/2YFAAZDW).

Please note that this bibliography is not updated. Feel free to submit suggestions for new content.
{: .notice--warning}