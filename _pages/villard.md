---
layout: single
title: "Progetto *Villard*: una banca dati degli utensili medievali"
permalink: /villard/
sidebar:
  nav: "villard"
---


Referenti per il grupporicerche: Matteo Sicios, Stefano Costa

Nei mesi di ottobre 2004 - gennaio 2005 alcuni membri del grupporicerche
hanno partecipato all\'allestimento della mostra didattica "**I modi di
Costruire nel taccuino medievale di Villard d'Honnecourt**", in
collaborazione con il Dipartimento di Scienze per l\'Architettura
dell\'Università di Genova e l\'ISCUM.

Nell\'ambito delle attività didattiche e scientifiche connesse alla
mostra è stato dato avvio ad una attività di catalogazione degli oggetti
esposti. Si è perciò provveduto preliminarmente ad una documentazione
fotografica, nonché alla registrazione di alcuni parametri fisici per
ogni pezzo catalogato.

Materiale, dimensioni, peso ed eventualmente alcuni rapporti
significativi tra queste grandezze sono stati ritenuti dei parametri di
base validi, su cui impostare future ricerche che riguardino la storia
degli utensili stessi, ma anche di tutto ciò che con essi è stato
costruito, siano manufatti mobili od opere architettoniche. Addentrarsi
nelle scelte tecniche degli strumenti adoperati da un costruttore del
Medioevo per dare risposte a ciò che oggi possiamo osservare nei
manufatti, non solo architettonici, è una delle finalità della ricerca
che questa catalogazione può agevolare.

Il catalogo raccoglie, quindi, tutte le informazioni relative questi
utensili, accessibili rapidamente attraverso una unica interfaccia, e
interrogabili secondo diversi criteri. Il supporto informatico che è
stato scelto per contenere queste informazioni è un gestore di basi di
dati relazionali sul modello *server/client* (PostgreSQL, MySQL), anche
in previsione di una eventuale successiva pubblicazione online. Se
invece verrà privilegiato un utilizzo *stand alone* in un ambito di
ricerca più ristretto, ci si orienterà verso soluzioni più adatte a
questa scelta, come ad esempio SQLite. Gli utensili sono stati suddivisi
secondo la funzione, che è il medesimo criterio scelto nell'esposizione.
Le categorie principali sono:

1.  trasporto (orizzontale) e sollevamento (verticale);
2.  misura, tracciamento e verifica;
3.  lavorazione della pietra;
4.  lavorazione del legno.

Sarà possibile consultare la banca dati anche secondo altri criteri,
quale ad esempio la collezione di provenienza. Un ruolo importante viene
svolto dall'illustrazione fotografica che rende chiaramente
comprensibile la forma, permettendo di comprendere, anche attraverso il
confronto diretto con l'iconografia, come un utensile poteva essere
usato. Molte forme permettono, come è nel caso di picconi e squadre,
un'intuizione immediata circa la loro funzionalità, anche perché si
tratta di strumenti di uso comune ancora oggi; per alcuni invece questa
comprensione non è altrettanto immediata, e nuovamente la fotografia può
svolgere un importante ruolo documentario, nel caso in cui ritragga
anche la posizione e il movimento effettuato durante l\'utilizzo.

Gli utensili esposti sono in parte riproduzioni, in parte utensili
originali degli ultimi due secoli. Gli oggetti ricostruiti si basano
sugli oggetti esistenti e sulla iconografia, e sono corretti − e
accettabili come confronto scientifico − perché sono il frutto di una
ricerca filologica e non semplici riproduzioni delle sembianze degli
originali: l'utensile svolge il suo compito quando è realmente tarato
per il suo scopo su chi lo deve usare, cioè l'uomo. In questo senso il
catalogo assume anche un valore didattico oltre a rappresentare un
supporto alle attività di ricerca.

La denominazione di alcuni utensili, e in generale il lessico tecnico,
rappresentano un problema in quanto la maggioranza del materiale esposto
proviene dalla Francia. Il principale ostacolo non è la semplice
traduzione linguistica, quanto piuttosto la presenza di differenze
oggettive, più o meno marcate, tra gli strumenti, alcuni dei quali hanno
probabilmente diverse cronologie o non erano presenti in entrambe le
aree geografiche.

L\'attività svolta fino a questo momento, che per ora è consistita nella
documentazione fotografica e nelle misurazioni, rappresenta perciò solo
un punto di partenza. Le possibilità di arricchire questo catalogo
risiedono nel:

1.  dare spiegazione del loro uso;
2.  fornire un corredo iconografico ai diversi utensili
3.  inserirli nel contesto storico ad essi proprio.

### Collegamenti utili

-   [I modi di Costruire nel taccuino medievale di Villard
    d'Honnecourt](http://www.arch.unige.it/sla/archeo/pagmostra.htm "Pagina di presentazione della mostra"):
    presentazione della mostra
-   [Dipartimento di Scienze per
    l\'Architettura](http://www.dsa.unige.it "Pagina di presentazione del DSA")
-   [Il taccuino medievale di Villard de
    Honnecourt](http://classes.bnf.fr/villard/feuillet/ "Le carnet de Villard de Honnecourt")
    (francese)
-   [Maison de l\'Outil et de la pensée
    arrière](http://www.maison-de-l-outil.com "Maison de l'Outil et de la pensée arrière")
    (francese)

Questa relazione è pubblicata in forma cartacea sul
[*Notiziario di Archeologia Medievale*, 77](http://www.iscum.it/Iscum/pubblicazioni_files/NAM77completo-1.pdf), ESCUM, dicembre 2004.
