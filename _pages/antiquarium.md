---
layout: single
title: "IOSA Antiquarium: An archive of obsolete software developed for archaeological research."
modified: 2021-09-15
permalink: /antiquarium/
---

The IOSA Antiquarium was presented at ArcheoFOSS 2011 along with the [TIPOM](/software/tipom/) software rewrite.

Unfortunately, the proposal did not gain any traction from the community.

This page is kept for historical reference.

