---
title: Strumenti per ICCD
permalink: /iccd/
---

Un progetto del **grupporicerche** per la condivisione di strumenti e
modelli conformi agli standard dell'Istituto Centrale per il Catalogo
e la Documentazione (ICCD).

Il progetto, ora archiviato, è stato presentato al workshop
[ArcheoFOSS](https://www.archeofoss.org/) 2009. Può essere citato
come:

Pesce, Giovanni Luca Annibale, e Stefano Costa. «Condivisione e
diffusione dei dati nel settore dei beni culturali. Le potenzialità
dell’integrazione tra standard, formati aperti e licenze
libere». *ARCHEOFOSS Open Source, Free Software e Open Format nei
processi di ricerca archeologica. Atti del IV Workshop (Roma, 27-28
aprile 2009)*, a cura di Paolo Cignoni et al., vol. 2, 2009,
pagg. 71–76, [PDF scaricabile](http://www.archcalc.cnr.it/journal/id.php?id=oai:www.archcalc.cnr.it/journal/A_C_oai_Archive.xml:616&sup=true).

## Modelli

### SchedaICCD-I3.sql (versione 2008-12-29)

Schema SQL utile alla schedatura di tre tipi di bene secondo le
normative ICCD per le schede di livello "Inventariale". I beni sono: i
siti archeologici, i beni architettonici e i parchi/giardini. È
disponibile il relativo manuale.

- [schema SQL](SchedaICCD-I3.sql)
- [manuale PDF](ManualeUsoSchedaI.pdf)