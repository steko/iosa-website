---
layout: single
title: "IOSA and its contributions to the archaeology community"
permalink: /community/
---

The IOSA working group gave a substantial contribution to the archaeology community by organizing meetings and workshops such as:

- the 2007 workshop "Free software, open source e open format nei processi di ricerca archeologica" in Genoa (precursor to the [ArcheoFOSS](https://www.archeofoss.org/) workshop series)
- the 2009 seminar ["Diritti d'autore e banche dati per i beni culturali"](/diritti/) in Genoa
- the 2010 "r-chaeology" unconference at the University of Trento

In 2008 Luca Bianconi took part in the Google Summer of Code project with Oxford Archaeology, working on the Java SQLite driver. The collaboration with Oxford Archaeology lead to the first version of a mobile version of Total Open Station.