---
title: Total Open Station
---

Total Open station is a program to download and convert data from total station survey devices.

It was created in 2008 by Stefano Costa and Luca Bianconi.

It is currently maintained by Stefano Costa and Damien Gaignon and while it still uses some of the iosa.it infrastructure it has continued independently.

The website is at <https://tops.iosa.it/>.
