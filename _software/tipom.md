---
title: Tipom
header:
  overlay_image: /software/tipom/bagolini.png
  overlay_filter: linear-gradient(rgba(0, 0, 0, 0.0), rgba(0, 0, 0, 1.0))
  teaser: /software/tipom/bagolini-th.png
---

Tipom is a R package for typometry of lithic tools.

It is a complete rewrite of a BASIC program written in 1990 by Leone Fasani at the University of Milan.

The package is on CRAN at <https://cran.r-project.org/package=tipom> and it was presented at the ArcheoFOSS conference in 2011.

Costa, Stefano, et al. “TIPOM 2011: L’archeologia Del Software in Archeologia.” ARCHEOFOSS. Open Source, Free Software e Open Format Nei Processi Di Ricerca Archeologica, Atti Del VI Workshop (Napoli, 9-10 Giugno 2011), edited by Francesca Cantone, vol. 13, Naus, 2012, pp. 211–18 ([PDF download](https://dissem.in/p/120289533/tipom-2011-larcheologia-del-software-in-archeologia/))


