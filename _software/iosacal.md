---
title: IOSACal
header:
  overlay_image: /assets/images/iosacal.png
  overlay_filter: linear-gradient(rgba(0, 0, 0, 0.0), rgba(0, 0, 0, 1.0))
  teaser: /assets/images/iosacal.png

---

IOSACal is a radiocarbon calibration program and programming library, written in Python.

It was created in 2008 by Stefano Costa and is still actively developed.

As the name indicates, it is still an “official” IOSA software project.

The website is <http://c14.iosa.it/>.
