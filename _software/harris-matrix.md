---
title: Harris Matrix 
header:
  overlay_image: /assets/images/harris-matrix.png
  overlay_filter: linear-gradient(rgba(0, 0, 0, 0.0), rgba(0, 0, 0, 1.0))
  teaser: /assets/images/harris-matrix.png
---

The broader aim of generalizing the usage of open source tools for the generation of Harris Matrix, the most widely used conceptual tool for analysing archaeological stratigraphy data, has been tackled since 2008.

The Harris Matrix Data Package is a formal specification of a digital format, with a companion command line tool, where archaeological stratigraphy datasets are in CSV format, following the table schema developed by Thomas S. Dye for the [`hm` Lisp package](http://tsdye.online/harris-matrix/homepage/). The output images are based on Graphviz. The [Harris Matrix Data Package specification](/specs/harris-matrix-data-package/) is available for comments.

The command-line `hmdp` tool can check consistency of data with the format, create new data packages and prepare a visualization of the Harris Matrix. `hmdp` is available at <https://codeberg.org/steko/harris-matrix-data-package>.

A previous tool that is now called "legacy" can be found at <https://codeberg.org/steko/harris-matrix-legacy> with further links to the blog posts that introduced it.

## How to cite this work

If you use this software in your research, please provide a citation to
the paper introducing it:

Costa, Stefano. “Una proposta di standard per l’archiviazione e la condivisione di dati stratigrafici.” Archeologia e Calcolatori, 30, 2019, pp. 459–62, DOI: <https://doi.org/10.19282/ac.30.2019.29>
