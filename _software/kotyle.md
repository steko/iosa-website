---
title: Kotyle
---

Kotyle measures capacity of amphorae and other ceramic vessels.

It is made of various separate tools, the main tool is a plugin for the GIMP image editor.

The website is <https://kotyle.readthedocs.io/>.
