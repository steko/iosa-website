CREATE TABLE au_atb
(
   id_ambito_culturale Integer PRIMARY KEY NOT NULL UNIQUE,
   id_cd Integer REFERENCES cd(id_cd),
   atbr Character varying(50),
   atbd Character varying(50)
);
CREATE TABLE au_atbm
(
   id_ambito_culturale Integer references au_atb(id_ambito_culturale),
   atbm Character varying (250)
);
CREATE TABLE cd
(
   id_cd Integer PRIMARY KEY NOT NULL UNIQUE,
   tsk Character varying(4),
   lir Character varying(5),
   nctr Numeric(2,0),
   nctn Numeric(8,0),
   esc Character varying(25),
   ogtd Character varying(70),
   ogta Character varying(100),
   ogtn Character varying(70),
   pvcs Character varying(50),
   dtzg Character varying(50),
   cdgg Character varying(50),
   adsp Character(1),
   adsm Character varying(70),
   cmpd INTERVAL year(4)
);
CREATE TABLE cd_ecp
(
   id_ecp Integer PRIMARY KEY NOT NULL UNIQUE, ecp Character varying(25)
);
CREATE TABLE cd_cd_ecp
(
   id_cd Integer REFERENCES cd(id_cd), id_ecp Integer REFERENCES cd_ecp(id_ecp)
);
CREATE TABLE cd_cmcmpn
(
   id_cd Integer REFERENCES cd(id_cd), id_cmpn Integer REFERENCES cm_cmpn(id_cmpn)
);
CREATE TABLE cd_lc_pvcc
(
   id_cd Integer REFERENCES cd(id_cd),
   id_comune Integer REFERENCES lc_pvcc_ctsc(id_comune)
);
CREATE TABLE cm_cmpn
(
   id_cmpn Integer PRIMARY KEY NOT NULL UNIQUE, cmpn Character varying(70)
);
CREATE TABLE cm_fur
(
   id_fur Integer PRIMARY KEY NOT NULL UNIQUE, fur Character varying(70)
);
CREATE TABLE cs_ctsf
(
   id_cs_com_d Integer PRIMARY KEY NOT NULL UNIQUE,
   id_comune Integer REFERENCES lc_pvcc_ctsc(id_comune),
   id_cd Integer REFERENCES cd(id_cd),
   ctsf Character varying(25)
);
CREATE TABLE cs_ctsn
(
   id_cs_com_p Integer PRIMARY KEY NOT NULL UNIQUE,
   id_cs_com_d Integer REFERENCES cs_ctsf(id_cs_com_d),
   id_cd Integer REFERENCES cd(id_cd),
   CTSN Character varying(500)
);
CREATE TABLE do_fta
(
   idfta Integer PRIMARY KEY NOT NULL UNIQUE,
   id_cd Integer REFERENCES cd(id_cd),
   ftax Character varying(25),
   ftap Character varying(50),
   ftan Character varying(25)
);
CREATE TABLE dt_dtm
(
   id_cd Integer REFERENCES cd(id_cd), dtm Character varying(250)
);
CREATE TABLE lc_pvcc_ctsc
(
   id_comune Integer PRIMARY KEY NOT NULL UNIQUE,
   id_regione Integer REFERENCES lc_pvcr(id_regone),
   id_provincia Integer REFERENCES lc_pvcp(id_provincia),
   pvcc_ctsc Character varying(50)
);
CREATE TABLE lc_pvcp
(
   id_provincia Integer PRIMARY KEY NOT NULL UNIQUE,
   id_regione Integer REFERENCES lc_pvcr(id_regione),
   pvcp Character varying(3)
);
CREATE TABLE lc_pvcr
(
   id_regione Integer PRIMARY KEY NOT NULL UNIQUE, pvcr Character varying(25)
);
CREATE TABLE re_ren
(
   id_ren Integer PRIMARY KEY NOT NULL UNIQUE,
   id_cd Integer REFERENCES cd(id_cd),
   renr Character varying(50),
   rels Character varying(10),
   revs Character varying(10)
);
CREATE TABLE re_renf
(
   id_ren Integer REFERENCES re_ren(id_ren), renf Character varying(250)
);
CREATE TABLE voc_adsp ( adsp Character(1), Descrizione Character varying(50) );
CREATE TABLE voc_cdgg ( cdgg Character varying(50) );
CREATE TABLE voc_ctl ( ctl Character varying(40) );
CREATE TABLE voc_ftax ( ftax Character varying(25) );
CREATE TABLE voc_nctr_pvcr
(
   cod_reg Character(2),
   nom_reg Character varying(21),
   cod_reg_cat_rif Character varying(1)
);
CREATE TABLE voc_pvcp
(
   cod_prov Character(3),
   nom_prov Character varying(20),
   targa Character(2),
   cod_reg Character(2)
);
CREATE TABLE cd_cm_fur
(
   id_cd Integer REFERENCES cd(id_cd), id_fur Integer REFERENCES cm_fur(id_fur)
);
CREATE TABLE cs_cs_ctsc
(
   id_cd Integer REFERENCES cd(ic_cd),
   id_comune Integer REFERENCES lc_pvcc_ctsc(id_comune),
   ctl Character varying(40)
);
CREATE TABLE voc_lir ( lir Character varying(5) );
CREATE TABLE voc_tsk ( tsk Character varying(4) );
/*Create index statements */
CREATE INDEX codici ON cd(nctn, ogtn);
CREATE INDEX comune_catasto ON cs_ctsc(ctsc);
CREATE INDEX comune ON lc_pvcc_ctsc(pvcc);
CREATE INDEX provincia ON lc_pvcp(pvcp);
CREATE INDEX regione ON lc_pvcr(pvcr);
