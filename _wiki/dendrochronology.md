---
title: Dendrochronology
---

Some notes about dendrochronology, open data and free software.

## Open data

-   [NOAA Tree-Ring Data](http://www.ncdc.noaa.gov/paleo/treering.html)
-   [Cybis Wiki on dendrochronology](http://www.cybis.se/wiki/)
-   [TRiDaS - The Tree Ring Data Standard](http://www.tridas.org/) is an
    international community-wide effort to produce a universal data
    standard for the dendrochronological data

## Free software for dendrochronology

-   [Corina](http://dendro.cornell.edu/corina/) is an open source,
    freely available, dendrochronology program used and developed by the
    Cornell Tree-Ring Laboratory and has an extensive
    [user-guide](http://dendro.cornell.edu/corina-manual/FrontPage) with
    general information about tree-ring dating techniques
-   [The Dendrochronology Program Library in R
    (dplR)](http://myweb.facstaff.wwu.edu/bunna/dplR.htm) is a software
    package in the R statistical programming environment for tree-ring
    analyses developed by Andy Bunn (Western Washington University)

