---
title: Correspondence Analysis (CA)
---

Correspondence Analysis (CA) is a statistical technique that can be used
to compare assemblages of archaeological artifacts.

This tutorial is based on two papers by Baxter and Cool[^1][^2] and will guide you through CA in the R statistical environment.

The code shown in this tutorial is also available in a git repository[^3].

Loading the libraries
---------------------

TODO

[^1]: [Baxter and Cool 2010](http://www.progettocaere.rm.cnr.it/databasegestione/open_oai_page.asp?id=oai:www.progettocaere.rm.cnr.it/databasegestione/A_C_oai_Archive.xml:654)
[^2]: Baxter and Cool 201x
[^3]: <https://codeberg.org/steko/corresp-baxter>