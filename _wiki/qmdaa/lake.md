Agent-based Modeling in Archaeology
===================================

[Mark Lake](http://www.ucl.ac.uk/archaeology/staff/profiles/lake.htm)\
Institute of Archeology, University College of London\
[mark.lake\@ucl.ac.uk](mark.lake@ucl.ac.uk)

Mark\'s lectures and materials can be found at
<http://rinyo.arch.ucl.ac.uk/~tcrnmar/qmdaa/> so these brief notes
aren\'t that much useful.

Lecture \#1
-----------

Clarke 1972

System models use algorithms and sequences of conditional operations.

Simulation studies the progress or outcome of a process that has an
analogue in the modern world.

1.  Tactical
    -   cultural genetics are \"horizontally\" transmitted
2.  Hypothesis testing
3.  Heuristic, Theory building
    1.  Validation
    2.  Sensitivity analysis to parameters
    3.  Experimentation

Lecture \#2
-----------

How do agents choose to reproduce themselves?

How long do agents live?

What about the time dimension?
