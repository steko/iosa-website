Spatial Analysis: Human Settlement Pattern Analysis
===================================================

[Giancarlo Macchi](http://www.archeogr.unisi.it/asiaa/)\
Dipartimento di Archeologia e Storia delle Arti, Università di Siena\
[macchi\@unisi.it](macchi@unisi.it)

Lecture \#1
-----------

-   G function
-   Ripley\'s K function
-   Poisson point process
-   Poisson distribution: discrete variables

Lecture \#2
-----------

`spatstat` package for R.
