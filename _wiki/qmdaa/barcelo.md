---
title: Explaining the Shape, Size, Texture and Composition of Archaeological Artifacts
---

[Juan A. Barcelo](http://seneca.uab.es/prehistoria/Barcelo/index.html)\
Departament de Prehistòria, Universitat Autònoma de Barcelona\
[jbarcelo\@seneca.uab.es](jbarcelo@seneca.uab.es)

There are lots of papers and materials on [Barceló\'s web
pages](http://seneca.uab.es/prehistoria/Barcelo/index.html).

Lecture \#1
-----------

-   Association
-   Categorization
-   Intervention
-   Conscience

Lecture \#2
-----------

**Shape analysis**. Shape is any connected set of points:

1.  localist representation is based on descriptive shape parameters
2.  distributed representation is pixel-based

There are different methods for describing shape: circularity,
quadrature, irregularity, elongation, size measurements. Like
morphometric measurements in anthropology, we can also use curvature
changes\' points.

Surfaces are characterized by *texture*.

Sigmoid axon.

It is important to find a balance between globalisation and localisation
when describing shape.

C. Reeler: settlement classification using neural networks.
