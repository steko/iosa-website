Analyzing and Modeling Archaeological Distributions
===================================================

[Kenneth Kvamme](http://www.uark.edu/depts/anthinfo/kvamme.html)\
Department of Anthropology, University of Arkansas\
[kkvamme\@uark.edu](kkvamme@uark.edu)

Lecture \#1: Archaeological Location Modeling at Regional Level
---------------------------------------------------------------

1.  Analyze →
2.  Model

Then some random notes:

-   Francis Galton
-   Null hypothesis (\<m\>H\_0\</m\>)
-   Study area

Lecture \#2
-----------

Model accuracy vs Model precision

Lecture \#3
-----------

1.  Distance from roads vectors
2.  Logistic regression (2 classes of desired values)
3.  Random points, means and variances at sites and random non-sites
4.  Elevation + Slope + Distance from water
5.  Categorized vs Continuous variables
