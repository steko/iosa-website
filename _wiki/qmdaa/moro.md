Spatial Data, Complexity, and Introduction to Uncertainty
=========================================================

**Antonio Moro**\
Facoltà di Ingegneria, Università di Firenze\
[moro\@dicea.unifi.it](moro@dicea.unifi.it)

Lecture \#1
-----------

Maths for Archaeology → Dwight D. Read, S. E. Van Der Leeuw

Perfect knowledge is *reductionist*. Statistics try to find if there\'s
an order in disorder, but explanations are just valid at specific scales
of study. Entropy (Shannon) is a measure of disorder. In disordered
structures, measurements change a lot with respect to the chosen unit
and scale.

Modeling can be determined (while not necessarily deterministic) or
undetermined. Determined modeling deals with predictable future.
Indetermination can be a result of absence of information, accuracy,
arbitration, comprehension.

**Complex systems** are more than the sum of their parts. The properties
are subjective, and small changes can cause great effects.

  Statistics                                  ≠   Probability
  ------------------------------------------- --- --------------------------------------------------
  set of rules to deal with indetermination       mathematical theory to deal with indetermination

Dyes are a simple random system, with independence, and the probability
of drawing a number stays the same.

Estimators are functions of data. They need to be unbiased and
consistent. Distributions can be normal, gamma or beta. Remember that
not everything is normal.

Production of data (numbers) and extraction of information is done by
descriptive statistics. Forecast is done by (probabilistic) models. The
last step is represented by fit tests.

Bear in mind that data can be censored, have outliers, have a multimodal
distribution and other problems.

The *required sample size* is a way to determine the minimum amount of
required data. It is important to consider the correlation between
variables.

Basic statistics do not deal with causality. Introducing the *time*
parameter, things get more complex. **Time-series** involve fixed
intervals of time, and at each point you have some quantity.

Spatial processes can be stationary, e.g. the distributions are
invariant, given the originating point. If it is invariant also with
respect to direction, it is isotropic.

Spatial data examples:

-   spatial point patterns
-   geostatistical continuous data (kriging, tessellation)
-   lattice, area data (subregions)
-   spatial interaction data (i.e. transport systems), relationships
    between data

**Subjective**: each one\'s degree of belief about a fact is different.
Coherence is obtained through the \"bet\" clause. A subjective approach
can give us a way to handle situations where we have little data and a
great theory. The model can be updated, based upon experience, during
time.

Measures

Generating processes

Philosophical positions

Lecture \#2
-----------

Complex ≠ Complicated\
Simple ≠ Easy\
Multitude → complexity but also simplicity

1.  Chaotic systems
2.  Synergy systems
3.  Mesoscopic systems

Complexity depends on the context of study.

\<m\>y = x\^3 + a·x\</m\>

if \<m\>a=1\</m\> or \<m\>a=-1\</m\> the 2 resulting systems are
qualitatively completely different. Mathematics catastrophe theory knows
just 7 types of catastrophes described in mathematical terms.

C. Renfrew 1978

Cellular Automata \<m\>{S\_i}\^{t+1} = R
({S\^t}\_{i-1},{S\^t}\_{i},{S\^t}\_{i+1})\</m\>

Power laws

Independence → No prediction

Lecture \#3
-----------

Fractals

-   not a single statistics
-   not only descriptive
-   self-similar (scaling factors)
-   defined by recursive algorithms
-   fractional dimension compared to integer geometries (line=1,
    square=2, cube=3)

Fractional dimension D characterizes how the mean depends on the size of
the sample.

Zipf 1949

Strange attractors are fractals (Lorentz)

Self-organized Criticality
