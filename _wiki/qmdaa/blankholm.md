The State of the Art of Intra-site Spatial Analysis
===================================================

[Hans Peter Blankholm](http://uit.no/arkeologi/ansatte/3?Language=en)\
Institutt for arkeologi, Universitetet i Tromsø\
[hanspb\@sv.uit.no](hanspb@sv.uit.no)\

Lecture \#1 : Intra-site spatial analysis
-----------------------------------------

The archaeologist analytical toolkit

R. Whallon:

-   dimensional analysis of variance
-   variance / mean ratio
-   nearest neighbor analysis

It is important to understand that *behaviorally* significant and
*statistically* significant are not necessarily associated.

Grid or point data? Both have their pros and cons.

In the 1980s there was a spreading of heuristic approaches:

-   K-means analysis
-   unconstrained clustering (UC)
