Archaeological Data Analysis and Modeling with Open Source GRASS GIS
====================================================================

[Benjamin Ducke](http://www.uni-kiel.de/ufg/ufg_BerDucke.htm)\
Christian-Albrechts-Universität zu Kiel\
[benjamin.ducke\@ufg.uni-kiel.de](benjamin.ducke@ufg.uni-kiel.de)

A good number of materials on [Benjamin\'s
website](http://www.uni-kiel.de/ufg/ufg_BerDucke.htm).

Lecture \#1
-----------

O\'Sullivan

Lecture \#2
-----------

Renfrew\'s **Xtent model**.

The \"real\" distance between points on a cost surface is affected by
the \"English Coast length\" problem about fractals. Scale can be
defined as one man\'s step, for example. If we study trade tracks, one
mule step should be OK.

Also using existing known roads as network edges can work.

We even shoould focus on roads before sites, developing similar
predictive models. A path can also be made to go through a steep
hillside, if it gets excavated. But normally we are likely to exclude
steep zones from our possible road network.
