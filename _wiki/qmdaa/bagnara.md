---
title: Computer Science Concepts for Archaeologists
---

[Roberto Bagnara](http://www.cs.unipr.it/~bagnara/)\
Dipartimento di Matematica, Università di Parma\
[bagnara\@cs.unipr.it](bagnara@cs.unipr.it)

Lecture \#1
-----------

Abstract interpretation

Lecture \#2
-----------

**Constraint satisfaction problems** deal with the *inference of hidden
information*.

Qualitative Temporal Reasoning: Allen 1983 finds 13 possible relations
between two events A and B.

RCC8 (topology?): consistency checks about spatial relationships between
objects.

Qualitative reasoning deals with incomplete knowledge, imprecision.
Imprecision ≠ Uncertainty.

Emergence has to do with indetermination laws: if we measure all
possible variables, then we have modified the state of the system.

-   Can we actually modify past, finite systems?
-   This sounds like a conceptualization of archaeological excavation:
    the more you want to know, the more you have to dig (and destroy)
