---
title: Archaeometry
---

This series of tutorials deals with all data produced by means of
archaeometric analyses.

Statistical analysis is very often already included in the archaeometric
study of materials.

## R Tutorials
- **[cluster analysis](../cluster_analysis)** based on XRD and XRF data
- statistical approaches to the study of ceramic artefacts using geochemical and petrographic data (not written yet)
