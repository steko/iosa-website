---
title: Quantifying Archaeology - Chapter 5
---

In chapter 5 of "Quantifying Archaeology", Shennan provides some
examples and 2 exercises. In this page, I try to show the steps to
follow for obtaining the desired results and graphs with R.

## Graphs

First of all, when Shennan provides tables, you should probably enter
data in a `data.frame` object:

```R
> burials <- edit(data.frame())
```

As far as I can tell, there isn't a single function that does all that
we need for plotting a graph with cumulative frequencies for ordinal
data (age categories, distance rings). We are then going step by step.

Obtain proportions for each of the two categories we have to evaluate,
separately, and store them in a separate vector object:

```R
> bur1 <- prop.table(burials[[1]])
> bur2 <- prop.table(burials[[2]])
```

Then start by plotting the first category:

```R
> plot(cumsum(bur1), type="l", col="red", ylim=c(0,1))
```

The R graphics window will show up with the first cumulative graph. Note
that we are explicitly requesting the drawing of a line with `type="l"`
and setting the graph's area vertical limit to its maximum extension.
This is because the `lines()` function, used below, cannot change the
plot's appearance but just add objects to it.

The `lines()` command is, though, very similar in its syntax to
`plot()`:

```R
> lines(cumsum(bur2),col="blue")
```

