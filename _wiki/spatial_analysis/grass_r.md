---
title: GRASS and R
---

R is a program which is constructed from libraries, you need certain
libraries for certain types of analysis.

To add a library type

    library(library name)

for example

    library(rcmdr) Launches R Commander GUI
    commander() relaunches R Commander GUI

See also the [Digging numbers](/wiki/diggingnumbers/) section for an introduction to R.

## Using R with GRASS GIS

Start GRASS GIS
Open a mapset
To start R type:

    R

Firstly you need the library with allows R to access the GRASS data:
spgrass6
`library(spgrass6)` to check it has worked use

    str(gmeta6())

This will list various information about your current mapset
Even though you are working in R you can access GRASS commands using the
system command

    system('g.list')

This will bring up the g.list GUI

### Reading Vector Data

To access data from GRASS you need to get R to create a
SpatialGridDataFrame class object, in this case we call the DataFrame
*data*

    data <- readVECT6(c("mapA"),plugin=F)

use summary() to see check it has imported correctly

    summary(data)

This will give you:

-   Minimum
-   1st Quartile
-   Median
-   Mean
-   3rd Quartile
-   Maximum

for each of your columns in the attribute table
You can also plot the data using the plot() function

    plot(data)

to get a nicer plot of your data use

    library(maptools)
    spplot(data)

### Reading Raster Data

Raster data is imported in a similar way as vector data just by using
the readRAST6 function

    data<-readRAST6(c("mapA"),plugin=F)

### Visualising data

There are many ways to visualise data

#### Bars, lines and pies

This section will demonstrate how to create nice looking graphs from
your data.

    plot(data)
    hist(data)

Also these come with many options to change the defaults:
- graph layout
- main the title of the graph ...main="Main Title"...
- xlim the x axis limits ...xlim=c(1,10)...
- ylim the y axis limits ...ylim=c(1,100)...
- xlab the label for the x axis ...xlab="x axis label"...
- ylab the label for the y axis ...ylab="y axis label"...
- xaxt x axis type ...xaxt="n"... other options:
- yaxt y axis type ...yaxt="n"... other options:
- colours and symbols

#### More than one graph at a time
Some times you want to see a selection of graphs at the same time, R is
able to do this through the *par* command

```R
par(mfrow=c(1,1)) #this will set the environment to display one graph
par(mfrow=c(1,2)) #this will set 1 row 2 columns
par(mfrow=c(2,2)) #this will give a 2 by 2 appearance
```

you only need to do this once then the next graph you create will be in
the first position and any following graphs will follow after.

#### Removing data

Sometimes there is data which you do not want to display.

```R
newdata <- data[data>=1]
```

This will remove Zeros and minus numbers,
useful if you want to present numbers of artefacts ignoring when there
are none found in an area.

```R
length(data) this gives the amount of items in your dataset
data[2] selects the 2nd row
data[-2] all except the 2nd row
data[1:5] the first 5 rows
data[(length(x)-5):length(x)] last 5 rows
data[c(1,3,5)] exact rows
data[x>3] values greater than 3
data[ x< -2 | x > 2] greater or less than some values
which(data == max(data)) identifies which is the maximum value
```

## Spatial Analysis

### Getis and Ord's Gi*

Developed in 1992, this analysis helps identify clusters. It is part of
a group of analyses collectivly called LISA (**L**ocal **I**ndicators of
**S**patial **A**ssociation). It is known as Local G, Gi* or GiStar, it
can be found in the spdep library.

```R
localG(x, listw, zero.policy=NULL, spChk=NULL)
```

See [LocalG](http://cran.r-project.org/web/packages/spdep/spdep.pdf

### Hierarchical Cluster Analysis (HCA)

Here we look at the Agnes method (**Ag**glomerative **Nes**ting ), this
treats very observation as a single cluster (one artefact clusters at a
distance of zero). The two closest clusters are then combined, then the
next two closest are combined until only a single cluster (the dataset)
remains.

Other types are:

Hierarchical methods: Agnes, Diana, and Mona.

Partitioning methods: Pam, Clara, and Fanny.

    library(cluster)
    mydata <- read.table(file="mydata.txt", header=TRUE,row.names=1, sep=",")
    mydata.agnes <- agnes(mydata)
    mydata.agnes.euc <- agnes(mydata, metric="euclidean",method="ward", stand=TRUE)
    plot(mydata.agnes.euc)

The text file you read into the mydata dataframe must be a matrix in the
following form:

  -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- --
     A   B   C   D   E      A      2   5   4   2      B   2      7   5   1      C   5   7      9   8      D   4   5   9      1      E   2   1   8   1   

  -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- -- --- --- --- --- --- --- --- --

