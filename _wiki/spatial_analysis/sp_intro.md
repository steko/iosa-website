---
title: The R `sp` package for spatial analysis
---

## Importing data

Using `rgdal` or `spgrass6` to import existing GIS data is probably the
easiest way to get started. Both packages can also be used to export the
results of your analysis back to a GIS environment.

An example using `rgdal` to import an ESRI Shapefile named `points.shp`
located in the `/home/steko/gisdata/` directory.

``` {.R}
> library(rgdal)
> points <- readOGR(dsn="/home/steko/gisdata", layer="points")
OGR data source with driver: ESRI Shapefile 
Source: "/home/steko/gisdata", layer: "points"
with 61 features and 6 fields
Feature type: wkbPoint with 2 dimensions
```

Let\'s have a look at the imported data. Note that the `points` object
is a `SpatialPointsDataFrame`.

``` {.R}
> str(points)
Formal class 'SpatialPointsDataFrame' [package "sp"] with 5 slots
  ..@ data       :'data.frame': 61 obs. of  6 variables:
  .. ..$ RECNUM  : Factor w/ 61 levels "1","10","11",..: 1 12 23 34 45 56 59 60 61 2 ...
  .. ..$ NAME    : Factor w/ 61 levels "001","002","003",..: 1 2 3 4 5 6 7 8 9 10 ...
  .. ..$ LAT     : Factor w/ 61 levels "8023340.86","8023775.89",..: 19 5 10 9 13 16 17 20 22 31 ...
  .. ..$ LON     : Factor w/ 60 levels "715020.00","715327.00",..: 38 60 55 58 59 57 39 36 45 43 ...
  .. ..$ ALTITUDE: Factor w/ 37 levels "10","-115","16",..: 2 30 4 37 23 9 21 33 10 25 ...
  .. ..$ LONGNAME: Factor w/ 0 levels: NA NA NA NA NA NA NA NA NA NA ...
  ..@ coords.nrs : num(0) 
  ..@ coords     : num [1:61, 1:2] 747843 749457 749235 749365 749397 ...
  .. ..- attr(*, "dimnames")=List of 2
  .. .. ..$ : NULL
  .. .. ..$ : chr [1:2] "coords.x1" "coords.x2"
  ..@ bbox       : num [1:2, 1:2] 715020 8023341 749457 8072390
  .. ..- attr(*, "dimnames")=List of 2
  .. .. ..$ : chr [1:2] "coords.x1" "coords.x2"
  .. .. ..$ : chr [1:2] "min" "max"
  ..@ proj4string:Formal class 'CRS' [package "sp"] with 1 slots
  .. .. ..@ projargs: chr NA
```
