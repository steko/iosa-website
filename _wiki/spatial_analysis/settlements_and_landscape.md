---
title: "Settlements and landscape: analysis techniques"
---

The analysis is done with GRASS GIS.

## Input Data

1.  A raster DEM of the studied territory. You can obtain an aspect map
    from it using the `r.slope.aspect` module.
2.  A vector map of sites or buildings dating from a defined period.

## The steps to the analysis

### Preparing data

Convert the vector map to raster with the command:

```
v.to.rast input=edifici output=edifici.mask use=val layer=1 value=1 rows=4096
```

Remember that this conversion is run following the region settings. You
can view the region settings with:

```
g.region -p
```

Now the resulting raster map becomes our `MASK`:

```
r.mask input=edifici.mask maskcats=*
```

From now on, all subsequent operations will be limited to the cells
defined in our `MASK`, corresponding to built areas or settlements.

If you need to remove the `MASK`, remember it a raster map like any
other one:

```
g.remove rast=MASK
```

### Histograms

The simplest analysis we can run is `d.histogram`. These are the
histograms of **elevation** and **aspect** for the whole of our study
area. Aspect is calculated counterclockwise from east, thus 0 = 360 = E,
90 = N, 180 = W, 270 = S. See
<http://grass.itc.it/grass62/manuals/html62_user/r.slope.aspect.html>
for the manual page of `r.slope.aspect`.

![](nomask.elevation.jpg)

![](nomask.aspect.jpg)

Applying `built.areas` as `MASK` results in these histograms:

![](mask.elevation.jpg)
![](mask.aspect.jpg)

### Polar diagrams

Aspect values are counted from 0 to 360 in degrees. So it would be
better to plot aspect in a **polar diagram** instead of an histogram.

The `d.polar` module does exactly what we need. Its complete
documentation is at
<http://grass.itc.it/grass62/manuals/html62_user/d.polar.html> .

It's handy to directly save the diagram to an EPS file for subsequent
operations (inclusion in a LaTeX file, conversion to other graphic
formats with ImageMagick, \...). Then you can use this command to get
your diagram

```
d.polar map=aspect.5m@PERMANENT undef=0 eps=all.eps
```

These are the results:

![](nomask.aspect.polar.png)
![](mask.aspect.polar.png)

## Results

This simple exploratory analysis leads us to understand that in the
study area there\'s a strong preference for sites exposed S-SE, built
mostly around 700 meters above sea level.
