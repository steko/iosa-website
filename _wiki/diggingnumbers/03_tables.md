---
title: Tables
---

The straight way to get a table for your variable is the table command:

```R
> table(Cond)
```

If you want to have a contingency table (one variable against one
other), you just have to add it between the parentheses, with the
independent variable as first argument:

```R
> table(Mat,Cond)
```

