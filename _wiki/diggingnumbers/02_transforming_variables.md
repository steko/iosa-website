---
title: Transforming variables
---


To group the variable `Date` into a new variable `Period`:

``` r
> Period <- Date
> Period[(Date>650)&(Date<=1200)] <- 1
> Period[(Date>100)&(Date<=650)] <- 2
> Period[(Date<=100)] <- 3
> table(Period)
Period
 1  2  3 
20 18  2 
```

``` r
> barplot(table(Period))
```

``` r
> barplot(table(Mat,Period), beside=TRUE)
```

To create the new variable \"maximum length / maximum width ratio\",
`Lewirat`:

``` r
> Lewirat <- Maxle/Maxwi
```

To create the new variable \"socket length as percentage of maximum
length\", `Socperc`:

``` r
> Socperc <- (Socle/Maxle)*100
```

