---
title: Measures of position and variability
---

To produce the mean, median, minimum, maximum values, 1st and 3rd
quartiles for maximum length, use

```R
> summary(Maxle)
```

To calculate the standard deviation for a variable:

```R
> sd(Maxle, na.rm=TRUE)
```

The `na.rm=TRUE` parameter is necessary because we have missing values
in our measurements.

The boxplot is based on some of the values given by the `summary`
function. Its central line represents the average mean. To produce a
boxplot for maximum length:

```R
> boxplot(Maxle)
```

As above but for each material:

```R
> tapply(Maxle,Mat,summary)
> boxplot(split(Maxle,Mat))
> tapply(Maxle,Mat,sd,na.rm=TRUE)
```
