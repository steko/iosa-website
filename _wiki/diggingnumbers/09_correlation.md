---
title: Correlation
---


In R, basic correlation tests are executed with two commands: `cor()`
and `lm()` (where `lm` stands for linear model).

## Calculating correlation

To calculate product moment correlation coefficient between `Maxle` and
`Maxwi` for bronze spears:

```R
> Bronze = subset(spearhead, subset=Mat=="1")
> cor(Bronze$Maxle, Bronze$Maxwi)
[1] 0.6892216
```

To calculate Spearman\'s rank correlation coefficient between `Date` and
`Weight` for bronze spears:

```R
> cor(Bronze$Date, Bronze$Weight, method="spearman")
[1] 0.1269293
```

## Plotting correlation

To draw a scatterplot for `Maxle` and `Maxwi`:

```R
> plot(Bronze$Maxle, Bronze$Maxwi)
```

The scatterplot by itself is already interesting, but R gives us another
interesting function with the `lm()` command (where `lm` stands for
*linear model*).

```R
> result <- lm(Bronze$Maxwi ~ Bronze$Maxle)
> result

Call:
lm(formula = Maxwi ~ Maxle)

Coefficients:
(Intercept)        Maxle  
     1.5053       0.1277  

```

1.  note that the order of arguments to `lm()` is inverse: the basic use
    is `lm(y ~ x)` (with `y` as *dependent* variable)
2.  the result of `lm()` is a rect. You can see by yourself
    *plotting* it over the scatterplot

```R
> abline(result$coefficients, col="blue")
```

![The composition of scatterplot and linear model](../lm1.png)

Plotting the `lm()` result by itself like

```R
> plot(result)
```

gives you more informative graphs about the linear model, but their
content is beyond the scope of this tutorial.

