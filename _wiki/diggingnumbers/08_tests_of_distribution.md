---
title: Tests of distribution
---

```R
> Iron <- subset(spearhead, subset=Mat=="2")
> Uniform <- runif(19,0,600)
> ks.test(Iron$Date,Uniform)
```

Test for whether `Maxle` has a normal distribution with either
stipulated mean and standard deviation or those from the sample.

Graphical comparison:

```R
> qqnorm(Maxle)
> qqline(Maxle)
```

Kolmogorov-Smirnov test:

```R
> Normal = rnorm(40)
> ks.test(spearhead$Maxle,Normal)
```

