---
title: Sampling
---

In archaeology, the term "Sampling\ can be used in a variety of
meanings. Here we are dealing with *statistical* sampling.

To take a simple 25% random sample of weight and calculate descriptive
statistics of the sample:

```R
> WeightSample <- sample(Weight,10)
```

To evaluate differences between the sample and the population:

```R
> summary(WeightSample)
> summary(Weight)
```
