---
title: Digging Numbers - Quantitative Archaeology Wiki
---

![Book cover](cover.jpg)

These exercises are based on the textbook \"Digging Numbers\" by Mike
Fletcher and Gary Lock (ISBN 978-0947816698). This book is an
introduction to statistics written for archaeologists by archaeologists.
It consists of four parts, the third being about practical examples of
application using known commercial statistical software. These pages are
meant to provide a \"translation\" of the same exercises using free
software.

Please bear in mind that this tutorial is not a substitute for a
complete book of quantitative archaeology or statistics. Currently it\'s
going to be hard to follow the tutorials without having *Digging
Numbers* with you. Maybe in the future we will add more content to this
website, but at the moment it\'s quite useless if taken alone.

![R](rlogo.jpg)

Our first software choice
is the R statistical suite.

R (<http://www.r-project.org/>) runs on a variety of platforms,
including GNU/Linux, MacOS X, Microsoft Windows and others. Installation
should be straight-forward in most cases. On some platforms, R is
provided with a basic GUI, but we will be using the command line,
because it is more powerful and gives access to all the available
options. Moreover, the command line interface is the same on each
platform. Our examples are based on a GNU/Linux system.

Anytime you need to look at the documentation, you can use the
`help(command.name)` function inside the R environment, which
gives you a general overview of the command you asked for, the available
parameters, bibliographic references and examples. Here\'s an example:

``` r
> help(summary)
```

Syntax
------

These pages deal with entering commands typing with your keyboard. When
you start R, you get the so-called *prompt*. It\'s R\'s way
to say that it\'s ready to accept your commands.

The standard R prompt looks like this:

``` r
>
```

What you type goes after the `>` symbol:

``` r
> ls()
```

From now on, text that comes formatted like above means \"this is a
command\". We won\'t be omitting the initial `>`, so if you wish to copy
& paste commands from this tutorial straight into the R window,
you need to avoid highlighting the \"\>\". In general however, it is
best if you type out the commands. This is not as bad as it sounds and
will actually help you learn the program commands.

## Data

The authors of the book included a dataset that contains various
measurements and data about 40 spearheads from English archaeological
sites. The example data used here are the same used in Digging Numbers
itself, and by kind permission of Professor Gary Lock we can now provide
the complete dataset in XLS and CSV format. CSV is best suited for
importing the data straight into R.

- [spearheads.xls](spearheads.xls)
- [spearheads.csv](spearheads.csv)

## Table of Contents

These are the chapters that have been covered so far.

1.  [Data description](01_data_description)
2.  [Transforming variables](02_transforming_variables)
3.  [Tables](03_tables)
4.  [Pictorial displays](04_pictorial_displays)
5.  [Measures of position and variability](05_measures_of_position_and_variability)
6.  [Sampling](06_sampling)
7.  [Tests of difference](07_tests of difference)
8.  [Tests of distribution](08_tests of distribution)
9.  [Correlation](09_correlation)
10. [Tests of association](10_tests of association)
