---
title: R-chaeology
---

R-chaeology is a BarCamp / unconference about R, archaeology,
statistics, spatial analysis and all the stuff this wiki is about.

R-chaeology 1
-------------

The first R-chaeology was in Trento, 16 April 2010. Local organizers
were Fabio Cavulli and Anna Maria Marras.

People attending:

-   Enrico R. Crema
-   Stefano Costa
-   Denis Francisci
-   Francesco Carrer
-   Elisa Rosciano
-   Alessandro Fedrigotti
-   Paola Salzani
-   Giorgio Baratti
-   Paolo Forlin
-   Anna Tanzarella
-   Anna Maria Marras
-   Fabio Cavulli

Some examples shown:

-   <http://www.homepages.ucl.ac.uk/~tcrnerc/Enrico_R._Cremas_HomePage/Etc>..html
-   <http://bitbucket.org/steko/pottery-ggplot2>
-   [CircStats](http://cran.r-project.org/web/packages/CircStats/index.html)
    and
    [heR.Misc](http://exposurescience.org/?q=hosted-projects/human-exposure-research-software-package)
