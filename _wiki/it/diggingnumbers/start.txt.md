Digging Numbers (scavare numeri)
================================

![Digging Numbers\'
cover](http://ec1.images-amazon.com/images/I/51FS7FDC5BL._AA240_.jpg){.align-left}
Questi esercizi sono basati sul libro \"Digging Numbers\" scritto da
Mike Fletcher e Gary Lock (ISBN 978-0947816698). Questo libro fornisce
una introduzione alla statistica scritta da archeologi per archeologi.
Consiste di 4 parti la terza delle quali contenente esempi di
applicazioni pratiche attraverso \'uso di software statistici
commerciali. Queste pagine sono pensate per fornire una \'traduzione\'
di tali esercizi con software free.

Si precisa tuttavia che tale tutorial non va inteso come sostituto del
libro completo di archeologia quantitativa o statistica. Potrebbe
risultare anzi difficile seguire queste spiegazioni senza avere con te
il libro *Digging Numbers* . Probabilmente in futuro aggiungeremo altri
contenuti tratti da tale pubblicazione, ma per il momento potrebbe
risultare inutile tale tutorial se usato da solo.

![R](/diggingnumbers/rlogo.jpg){.align-right}Il primo software che
useremo è **`R`** statistical suite.

**`R`** (<http://www.r-project.org/>) può essere utilizzato con
piattaforme quali GNU/Linux, MacOS X, Microsoft Windows e altre. Su
alcune piattaforme **`R`** è fornito con una veste grafica di base, ma
qui utilizzeremo la riga di comando perchè è più \'potente\' e
garantisce l\'accesso a tutte le opzioni disponibili. Per di più la
linea di comando è identica per tutte le piattaforme.I nostri esempi
sono basati su un sistema GNU/Linux perchè il free software ci piace
davvero :-)

Ogniqualvolta avessi bisogno di maggiori informazioni puoi utilizzare il
comando `help(command.name)` all\'interno dell\'ambiente **`R`** che ti
fornisce uno sguardo rapido sulle opzioni del comando selezionato, i
parametri disponibili, riferimenti bibliografici ed esempi. Ecco un
esempio:

``` {.C}
> help(summary)
```

Sintassi
--------

Queste pagine si riferiscono all\'inserimento dei comandi tramite
normale tastiera. Quando avvii **`R`**, ti trovi di fronte al cosiddetto
*prompt*. E\' il modo utilizzato da **`R`** per dirti che è pronto ad
accettare i tuoi comandi.

Il prompt standard di **`R`** è come questo:

``` {.C}
>
```

Tutto quello che scrivi va dopo il simbolo `>` :

``` {.C}
> ls()
```

Da ora in avanti, il testo formattato come sopra significa \"questo è il
comando\". Non toglieremo l\'iniziale `>`, quindi **non puoi** copiare e
incollare i comandi direttamente nella finestra di **`R`**, ma piuttosto
dovrai riscriverli. In ogni modo non è così male come sembra.

Dati
----

Gli autori del libro hanno incluso un set di dati che contiene varie
misurazioni e informazioni riguardanti 40 punte di lancia da siti
archeologici inglesi. I dati utilizzati qui sono gli stessi dati del
libro, e per gentile concessione del Prof. Gary Lock questo set di dati
è ora disponibile nel pubblico dominio. Il file è disponibile in formato
![XLS](/diggingnumbers/spearheads.xls) o
![CSV](/diggingnumbers/spearheads.csv) format. CSV è la scelta migliore
per importare direttamente i dati in R.

Contenuti
---------

Questi sono i capitoli al momento disponibil.

1.  [Data description](/diggingnumbers/data_description)
2.  [Transforming variables](/diggingnumbers/transforming_variables)
3.  [Tables](/diggingnumbers/tables)
4.  [Pictorial displays](/diggingnumbers/pictorial_displays)
5.  [Measures of position and
    variability](/diggingnumbers/measures_of_position_and_variability)
6.  [Sampling](/diggingnumbers/sampling)
7.  [Tests of difference](/diggingnumbers/tests of difference)
8.  [Tests of distribution](/diggingnumbers/tests of distribution)
9.  [Correlation](/diggingnumbers/correlation)
10. [Tests of association](/diggingnumbers/tests of association)
