Wiki di Archeologia Quantitativa
================================

Benvenuti. Questo wiki è dedicato alla raccolta e stesura di
documentazione libera sui metodi quantitativi per l\'archeologia,
utilizzando software libero e open source. Questo significa che:

1.  tutti possono contribuire e migliorare la documentazione esistente
2.  preferiamo parlare di software libero e non di software proprietario

I contenuti del wiki sono disponibili sotto la [Creative Commons
Attribution-Share Alike 3.0 Unported
License](http://creativecommons.org/licenses/by-sa/3.0/). Questo dà a
tutti la libertà di modificare e redistruibuire copie dei contenuti.

Per coordinare il lavoro, abbiamo predisposto una breve
**[lista](/todo)** delle cose da fare.

Questo wiki utilizza [DokuWiki](/wiki/dokuwiki) come motore. Una guida
completa è disponibile sul sito di Dokuwiki, in aggiunta alla breve
*[guida di riferimento](/wiki/syntax)* accessibile all\'interno di ogni
pagina durante la modifica.

Digging Numbers
---------------

La prima iniziativa che abbiamo intrapreso è la riscrittura degli
esercizi del manuale \"Digging Numbers\" (Fletcher & Lock 1994)
utilizzando il software statistico **`R`**. Questa iniziativa è ancora
in corso, perciò se vuoi contribuire sei il benvenuto.

☛ [Vai agli esercizi di Digging Numbers](/it/diggingnumbers/start)

Medie Ponderate Individuali
---------------------------

Si tratta di un metodo per calcolare con precisione la distribuzione
cronologica complessiva di classi di manufatti che hanno una durata
molto estesa nel tempo.

☛ [Vai alla pagina sulle Medie Ponderate
Individuali](/sum of individual weighted means)

Tabelle di contingenza
----------------------

Una introduzione di base alle tabelle di frequenza e contingenza
utilizzando **`R`**. È inclusa una funzione per disegnare diagrammi di
Ford (*battleship diagram*)

☛ [Vai alla pagina sulle tabelle di contingenza](/contingency tables)

Analisi spaziale
----------------

Sezione sulle tecniche di analisi spaziale realizzate utilizzando
software geospaziale libero come GRASS, R e altri.

☛ [Vai alla pagina introduttiva sull\'analisi
spaziale](/spatial_analysis/start)

Bibliografia
------------

Stiamo raccogliendo riferimenti bibliografici a manuali e riviste
inerenti l\'archeologia quantitativa in una sezione dedicata.

☛ [Vai alla raccolta bibliografica](/bibliography/start)

Lista di discussione
====================

Esiste una **[lista di discussione
italiana](http://gfoss.it/cgi-bin/mailman/listinfo/archeologia)**
dedicata alla discussione dei metodi quantitativi, del software e dei
formati liberi per l\'archeologia e altro ancora.

Esiste anche una **[mailing list internazionale](http://list.iosa.it)**
(in lingua inglese) dedicata agli stessi temi ma rivolta ad un pubblico
più ampio.

Questa è una lista degli ultimi interventi sulla lista internazionale:

![//rss.gmane.org/topics/excerpts/gmane.science.archaeology.quantitative-archaeology author date 12h](/rss>http///rss.gmane.org/topics/excerpts/gmane.science.archaeology.quantitative-archaeology author date 12h){.align-left}

======= Sul Web =======

Esistono altri siti web dedicati ad alcuni dei temi che affrontiamo su
questo wiki:

-   <http://www.iosa.it/> (inglese)
-   <http://www.arc-team.com/> (inglese, tedesco, italiano)
-   <http://www.archeogr.unisi.it/asiaa/> (italiano)
