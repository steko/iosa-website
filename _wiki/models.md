---
title: Archaeological models
---

A place to collect information about archaeological modeling, both from
a formal and theoretical point of view and as source code.

## Basic list of models

### Gravity models

#### Background


In geography, gravity models are used to determine the relative strength
of a bond between two or more places. They specify the potential of
places to attract people, commodities and ideas by taking into account
both the size of places, and their distance. Gravity models are a
modification of Newton's Law of Gravitation. The basic specification of
a gravity model is:

PᵢPⱼ / Dᵢⱼ²

Where *P* is the population of place *i* and *j*, and *D* their
distance. Instead of Euclidean distances, relative distances can be
used, and population could be replaced by any other indicator of
importance (like settlement size). An important aspect in the gravity
model is the form of the distance decay curve, which in its original
form is assumed to be quadratic. However, other specifications could be
used as well.

A well-known archaeological application of gravity models is the **XTent
model** (Renfrew & Level 1979). It is specified as follows:

I = Ca - k\*d

where *C* is the settlement size (or population, or hierarchical level)
and *k* defines the distance decay curve. Unlike in the original gravity
model, distance decay is assumed to be linear, not quadratic. An
essential aspect of the XTent model is that it is not calculating
interaction between settlements, but the extent of their influence
(*I*). By modifying the value of *a*, the importance of site hierarchy
can be adapted. The XTent model results in the specification of
\'territories\' for each settlement with values of *I* that are
decreasing outward from the settlement. For each location in an area,
there will be a settlement that provides the highest value of *I*, and
then this location will be classified as being inside the site\'s
territory. This might include other, smaller settlements.

#### Implementations

In archaeology, gravity models have not been used to great extent,
mostly because of the problems associated with the estimation of
population of ancient settlements (see however Nuninger et al. 2006).

Ducke & Kroefges (2008) describe an implementation of the XTent model
with the use of cost surfaces that is available as an add-on in GRASS:
<http://grass.osgeo.org/wiki/GRASS_AddOns#r.xtent>

Creating the XTent model or other gravity models in GIS can however also
be approached through scripting, although this would quickly become slow
with larger amounts of sites to include.

#### Literature

Ducke, B., and P.C. Kroefges (2008). From Points to Areas: Constructing
Territories from Archaeological Site Patterns Using an Enhanced Xtent
Model. In Posluschny, A., K. Lambers, and I. Herzog (eds.), Layers of
Perception. Proceedings of the 35th International Conference on Computer
Applications and Quantitative Methods in Archaeology (CAA), Berlin,
Germany, April 2--6, 2007 Dr. Rudolf Habelt GmbH, Bonn, pp. 245-251.

Nuninger, L., L. Sanders, F. Favory, P. Garmy, C. Raynaud, C. Rozenblat,
L. Kaddouri, H. Mathian, and L. Schneider (2006). La modélisation des
réseaux d\'habitat en archéologie: trois expériences. Mappemonde
83:3-2006. <http://mappemonde.mgm.fr/num11/articles/art06302.html>

Renfrew, C., and E.V. Level (1979). Exploring Dominance: Predicting
Polities from Centers. In Renfrew, C., and K.L. Cooke (eds.),
Transformations: Mathematical Approaches to Cultural Change, Academic
Press, New York, pp. 145-167.
