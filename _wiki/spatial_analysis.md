---
title: Spatial analysis
---

This section contains tutorials about spatial analysis techniques with
hands-on exercises using GRASS, R, QGIS and other free geospatial
software.

-   [Introduction to the "sp" R package](sp_intro)
-   [Feature count](feature_count) : how to count the number of points
    (e.g. archaeological sites) that fall within areas (e.g. survey
    regions), generating summary statistics that can be used also for
    plotting graphs on a map
-   [Settlements and landscape](settlements_and_landscape) is a tutorial
    showing how you can draw basic information about settlement choices,
    based on landscape variables as elevation and aspect.
-   [k-means](k-means) : basic tutorial for classification of objects\'
    position using the K-Means method

## GRASS and R

-   [Introduction to GRASS](grass)
-   [GRASS and R](grass_r)

