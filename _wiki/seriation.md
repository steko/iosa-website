---
title: Seriation
---

Seriation is a relative dating method in which assemblages or
artifacts from numerous sites, in the same culture, are placed in
chronological order. Seriation has a rich history in archeology. Petrie
(1899) was the first to use seriation as a formal method. He applied it
to find a chronological order for graves discovered in the Nile area
given objects found there.

You should read the Wikipedia article for a detailed explanation of
[seriation in archaeology](https://en.wikipedia.org/wiki/Seriation_(archaeology)).
Here's just the technical part, with some hints at using the
R `seriation` package for seriation and generation of plots.

- [CRAN - Package seriation](http://cran.r-project.org/web/packages/seriation/index.html)

## Describing your data

The simplest form of seriation is when your data are just
presence/absence information, e.g. presence of a particular artifact
inside a tomb burial.

TODO
