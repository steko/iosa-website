---
title: I-QMDAA, Quantitative Methods and Data Analysis in Archaeology
---

1st international summer school on quantitative methods for archaeology.
The Summer School official website is at
<http://www.archeogr.unisi.it/qmdaa/>

Here are some brief notes taken during the classes. They are far from
complete but could be useful for a start.

Courses
-------

1.  [Computer Science Concepts for Archaeologists](bagnara) (R. Bagnara)
2.  [Spatial Data, Complexity, and Introduction to Uncertainty](moro)
    (A. Moro)
3.  [The State of the Art of Intra-site Spatial Analysis](blankholm)
    (H. P. Blankholm)
4.  [Explaining the Shape, Size, Texture and Composition of
    Archaeological Artifacts](barceló) (J. A. Barceló)
5.  [Archaeological Data Analysis and Modeling with Open Source GRASS
    GIS](ducke) (B. Ducke)
6.  [Analyzing and Modeling Archaeological Distributions](kvamme) (K.
    Kvamme)
7.  [Agent-based Modeling in Archaeology](lake) (M. Lake)
8.  [Spatial Analysis: Human Settlement Pattern Analysis](macchi) (G.
    Macchi)
