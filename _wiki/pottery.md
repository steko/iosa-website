---
title: Quantification of archaeological pottery
---

This is a complex and articulated topic, that has been studied by many
authors with a variety of approaches. There are no ready solutions, just
a large set of techniques that can be used together when studying
archaeological pottery. The most important author for quantitative
studies on pottery is certainly **Clive Orton**.

## Some case studies with (Late) Roman pottery

-   African Red Slip Ware (ARSW) from Northern Italy (400 -- 700 AD)
-   Gaulish and Hispanic Samian Ware and African Red Slip Ware from
    *Tingitana*

## Scripts to automate plot drawing

You can find some scripts that create good-looking plots for
quantitative pottery data at
<https://codeberg.org/steko/pottery-ggplot2>

These scripts are written in the R programming language and use the
[ggplot2](https://ggplot2.org/) library. They assume that data are
standardized in *flat* ("tidy") tables with variables like:

-   shape of vessel
-   reference to existing typologies (e.g. Hayes for ARSW)
-   number of sherds, minimum number of vessels
-   chronological lifespan of each type (expressed as two separate
    columns of *beginning* and *end* of production)
-   fabric
-   and others

## Analysing assemblages

-   [Chi-squared test](/pottery/chisq.test)
