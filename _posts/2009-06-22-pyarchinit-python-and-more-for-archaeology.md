---
layout: single
title: ! 'pyArchInit: Python (and more) for Archaeology'
author: steko
created: 1245684138
---
Ten days ago Luca Mandolesi <a href="http://lists.faunalia.it/pipermail/gfoss/2009-June/013054.html" title="[Gfoss] qgis plugin per l'archeologia - pyArchInit">released</a> his <a href="http://www.pyarchinit.altervista.org/">pyArchInit</a> QGIS plugin.
pyArchInit is a free/open source tool for the management of excavation data (compatible with Italian <acronym title="Istituto Centrale per il Catalogo e la Documentazione">ICCD</acronym> standard record sheets), that integrates itself inside the QGIS environment and gives you a highly portable system designed by an archaeologist for his daily work in rescue excavations.
pyArchInit was presented at the last workshop on archaeology and free/open source software in Rome, you can see Luca's presentation <a href="http://www.youtube.com/watch?v=ZPtDXmkQBeY" title="YouTube - pyArchInit - presentazione">here</a>.
There's also a dedicated <a href="http://groups.google.com/group/pyarchinit-users">mailing list</a> for those who need help and support.
I am very satisfied with pyArchInit being released, also because I was at some extent involved in convincing Luca that this way the way to go, while I was trying to provide Francesco de Virgilio with an <a href="http://lists.linux.it/pipermail/archaeology/2009-June/000190.html" title="Database dati archeologici - archaeology@lists.linux.it">answer</a> to his quest for a free/open source recording system. This is very good news in the end, even though it might sound mostly interesting for an Italian audience (which isn't obviously the case, pyArchInit has already been <a href="http://arkegeomatica.es/IdeARK/?p=1220" title="pyArchInit: Proyecto para el Desarrollo de Aplicaciones Multiplataforma en Python para la Gestión de Datos Arqueológicos">covered</a> by the IdeArk spanish blog).
