---
layout: single
title: SNNSraster Manager
author: steko
created: 1158254441
---
SNNSraster is a utility for quick ANN analysis of raster GIS maps with the use of Stuttgart Neural Network Simulator trained network files. It was developed to read and write binary raster files.
