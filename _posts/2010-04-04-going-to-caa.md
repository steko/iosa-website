---
layout: single
title: Going to CAA
author: steko
created: 1270409814
---
Tomorrow in the early morning I'm leaving for Granada. CAA 2010 starts on Tuesday. I'm giving two talks, unfortunately both are in <a href="http://www.caa2010.org/index.php/draft-programme/99-open-source">the same session</a> so I hope it's not going to be a one-man show (I don't like the idea of an open source <em>ghetto</em> session). As a side note, I noticed that three out of four authors are from Italy.
If you're already around tomorrow in the afternoon, drop me an e-mail and maybe we can arrange for a dinner.
