---
layout: single
title: Late Antique Archaeology 2010
author: steko
created: 1269000176
---
<p>These are some rough notes taken last week at <a href="http://lateantiquearchaeology.wordpress.com/2010/01/20/conference-laa-2010/">Late Antique Archaeology conference 2010</a> about <em>Local economies? Production &amp; exchange of inland regions</em>, that took place at King's College, London, Friday 12th to Saturday 13th March 2010. Overall, this conference was interesting, and I had a chance to meet lots of nice people working in Late Antique Archaeology. Inspiration for my PhD research was just great.</p>
<div class="contents topic" id="contents">
<p class="topic-title first">Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#whittow" id="id3">Whittow</a></li>
<li><a class="reference internal" href="#sarris" id="id4">Sarris</a></li>
<li><a class="reference internal" href="#macmahon" id="id5">Macmahon</a></li>
<li><a class="reference internal" href="#lavan" id="id6">Lavan</a></li>
<li><a class="reference internal" href="#bowes" id="id7">Bowes</a><ul>

<li><a class="reference internal" href="#comments" id="id8">Comments</a></li>
</ul>
</li>
<li><a class="reference internal" href="#vaccaro" id="id9">Vaccaro</a></li>
<li><a class="reference internal" href="#mulvin" id="id10">Mulvin</a></li>
<li><a class="reference internal" href="#fentress" id="id11">Fentress</a><ul>
<li><a class="reference internal" href="#id1" id="id12">Comments</a></li>
</ul>
</li>
<li><a class="reference internal" href="#mattingly" id="id13">Mattingly</a></li>
<li><a class="reference internal" href="#haeverbaeke" id="id14">Haeverbaeke</a></li>

<li><a class="reference internal" href="#bonifay" id="id15">Bonifay</a><ul>
<li><a class="reference internal" href="#amphorae" id="id16">Amphorae</a></li>
<li><a class="reference internal" href="#fine-wares" id="id17">Fine wares</a></li>
<li><a class="reference internal" href="#lamps-and-other" id="id18">Lamps and other</a></li>
<li><a class="reference internal" href="#tiles-and-bricks" id="id19">Tiles and bricks</a></li>
</ul>
</li>
<li><a class="reference internal" href="#poblome" id="id20">Poblome</a></li>
<li><a class="reference internal" href="#vokaer" id="id21">Vokaer</a><ul>
<li><a class="reference internal" href="#id2" id="id22">Comments</a></li>

</ul>
</li>
</ul>
</div>
<div class="section" id="whittow">
<h3><a class="toc-backref" href="#id3">Whittow</a></h3>
<p>Unfortunately I missed Whittow's lecture.</p>
</div>
<div class="section" id="sarris">
<h3><a class="toc-backref" href="#id4">Sarris</a></h3>
<p>Sarris talked about “<em>Rural production dynamics: autarchy, tax and
forms of exchange, seen from papyri</em>”.</p>

<p>Hay and meadowlands weren't actually very important, compared to
production of food and textiles (linen, flax) for supplying towns.</p>
<p>Guilds as seen from papyri are much more “medieval” self-organized
groups of craftsmen than Roman or Byzantine top-down corporations.</p>
<p>Obviously, Apion's estates were not economicaly isolated.</p>
<p>Early Islamic Egypt is normally described as a “decapitated” society,
because of the disappearance of aristocracy. But this situation left
room for middle-class landowners, at the same time enabling a
stronger central state, efficient collecting taxes.</p>
</div>
<div class="section" id="macmahon">
<h3><a class="toc-backref" href="#id5">Macmahon</a></h3>
<p>Macmahon gave a declaratively explorative talk about retail
structures, workshops, markets, seeing all these elements as strongly
associated with urban landscapes. Their nature is such that if we're
not able to find clear evidence for them, they're invisible.</p>
<p>Social shaping of antiquity includes economic factors: for this reason
he's trying to use theoretical models to explore ancient society:</p>
<ol class="arabic simple">

<li>&lt;missed&gt;</li>
<li>business, it's not necessarily a modernistic model</li>
</ol>
<p>Consumption is a social act, in which people do distinguish between
goods that are appropriate and not.</p>
<p>A market is defined by a number of factors:</p>
<ol class="arabic simple">
<li>location</li>
<li>income</li>
<li>demographics (age, sex, racial/ethnic mix)</li>
<li>lifestyles (marked for example by clusters of specialised stores in
urban areas)</li>

</ol>
<p>Boundaries between markets, and between societies in general, can be
defined in terms of:</p>
<ol class="arabic">
<li><p class="first">institutional boundaries</p>
</li>
<li><p class="first">natural boundaries</p>
</li>
<li><p class="first">spatial density / distribution of households</p>
</li>
<li><p class="first">spatial decay linked to transport costs</p>
</li>
<li><p class="first">competition</p>

<p>With regard to competition, i.e. Central Place Theory doesn't take
into account intra-site distributions and it's too demand-focused
(places are primarily consumption sites) rather than retail-focused</p>
</li>
</ol>
<p>The key contrast when studying local exchange is between individual
small resellers and large resellers. However, reseller units are part
of the urban landscape in the Roman empire, even if they change over time.</p>
<p>Warehousing didn't only mean storage structures, they could have been
used also as auction houses. Unfortunately specialisation is poorly
visible through archaeology. Literary sources are more important but
the two are difficult to integrate.</p>
<p>A single journey to buy or sell a single product can be used as an
extreme definition of specialisation. While there's a segmentation of
the empire in the later period, autosufficiency in provinces is much
more widespread in the early empire.</p>
<p>An important point is that there's apparently no development in
transportation networks, that are important in shaping
markets. Looking to a contemporary example, the car industry in the US
is spatially concentrated in a small area, but distribution is
wide. On the contrary, the production of pottery in the Roman world
was very scattered. This difference can be explained in terms of different
transportation networks.</p>
<p>Competition between retailers, producers, even the State developing
productions on its own, is a social element and a major factor of
economic development. Luxury goods can survive until there's a market
for them.</p>
</div>
<div class="section" id="lavan">
<h3><a class="toc-backref" href="#id6">Lavan</a></h3>

<p>Questions:</p>
<ul class="simple">
<li>volume of intra-regional trade?</li>
<li>evidence for large-scale specialised trade?</li>
<li>topography of social control?</li>
<li>role of artisans in regional exchange?</li>
</ul>
<p>Generally speaking, pottery is an easy study object, but what about i.e.
the regional distribution of metal objects?</p>
<p>Examples:</p>
<ul class="simple">

<li>regional “markets” in architecture, sculptures and sarcophagi</li>
<li>there are some productions that have to be local, like bakers (most
bakeries were small-scale enterprises), butchers, goldsmiths</li>
</ul>
<p>Large coastal cities can have highly specialised artisans (images of
Corinth, Philippi, Stobi, Scythopolis, Sagalassos). Being by the coast
determines the scale of production and retail. Are things at the same
scale or not between coast and inland sites? E.g. textile production.</p>
</div>
<div class="section" id="bowes">
<h3><a class="toc-backref" href="#id7">Bowes</a></h3>
<p>Spain experiences a collapse in oil prodution during the 3rd
c. accompanied by a modest growth in garum. In the 4-5 c. there's a
boom of (mostly monumental) villas.</p>
<p>Questions:</p>
<ul class="simple">
<li>is there a regional economy?</li>

<li>are villas a straighforward economic barometer?</li>
</ul>
<p>Existing models for Spain during Late Antiquity:</p>
<ol class="arabic">
<li><p class="first">non-particularity of Spain</p>
<p>Bowes argues that monumental villas are much more numerous in
Spain, Aquitania and Britain in the 4-5 c. Particularity applies,
if we consider the empire as a whole, because this phenomenon
happens only in some regions &lt;someone commented that this might
just reflect a bias in field research&gt;</p>
</li>
<li><p class="first">poverty of Spain</p>
<p>Bowes sees poverty as nonsense given the amount of monumental
villas and the overall richness of material culture.</p>
</li>

<li><p class="first">villa-driven economy</p>
<p>Bowes thinks that the concentration of landholdings in the hands of
a few is not a convincing model.</p>
</li>
</ol>
<p>The Serpa survey showed a high density of villas &lt;even though I
couldn't get if all of them are contemporary&gt;, there are very rich
buildings like at La Olmeda, but in general the spectrum is very wide
and there's a sort of regional architectural “discourse” (between
either artisans or elites). Mosaics have lots of inscriptions, and
personal names (representing display of the self).</p>
<p>Villas are found in clusters, they are not everywhere and
particularly:</p>
<ul class="simple">
<li>not on the coast</li>
<li>not in the Mediterranean region</li>

</ul>
<p>Are these symptoms of a regional economy? Monumental villas are
spatially concentrated, while productive sites are also on the
coast. But why these local differences?</p>
<p>Fortifications and state investments in the N and W regions are the
context for these local disparities, together with the presence of
troops. There is some overlap between villas' clusters and roads &lt;this
didn't look very convincing to me&gt;, and there are some theories about
this overlap:</p>
<ol class="arabic simple">
<li>an annona route in the context of supplying the Rhineland limes by
seaborne trade in the Atlantic, and villas exploiting the annona
route (e.g. there are granaries near roads in villas). Two new
provinces get created (Gallaecia, Novumpopuloniae). Is Spain
producing horses for the Rhineland limes ? Theodosian AE coins have
a similar distribution with villas &lt;why in the world they should
not have a similar distribution? why is this relevant?&gt; Lots of
milestones account for importance of roads. There are problems with
this theory, for example no Spanish amphorae in Rhineland forts.</li>
<li>routes as arteries of competitive opportunity: State investment is
selective, so there is social competition around it. Fortifications
are not necessarily signs of danger, but certainly &lt;a display&gt; of
power. There are diverse elites at work: dux, governor, curiales,
landowners all competing and not just in cities (Spain is much less
urban than other provinces). Villas may be agents of social
competition. TSHT is concentrated in this same area.</li>

</ol>
<p>Bowes thinks that the engine of the villa boom is not regional, but
rather it's the State.</p>
<p>Previous models for villas as an economic indicator include:</p>
<ol class="arabic simple">
<li>tenurial relationships and rise of colonate (Carandini)</li>
<li>self-display</li>
</ol>
<p>Instead, the proposed model defines villas as places for competition,
not a straightforward economic indicator. Other competitive landscapes
of the same kind are then concentrated bureaucracies (either military
or civil) and supply regions.</p>
<p>Major previous models for Roman economies and State are:</p>
<ol class="arabic simple">
<li>Finley, who refused to accept state-driven economy as “real economy”</li>

<li>those who see taxation and coinage as an engine for economy</li>
</ol>
<p>In Bowes's words, both seem to fail, because they assume a fully
working and discrete state.</p>

<div class="section" id="comments">
<h4><a class="toc-backref" href="#id8">Comments</a></h4>
<p>Mattingly observed that Spain has always been a huge producer of
metals.</p>
<p>Sarris commented that generally we see an “expanding state” during
Late Antiquity.</p>
</div>
</div>
<div class="section" id="vaccaro">
<h3><a class="toc-backref" href="#id9">Vaccaro</a></h3>

<p>Major events in history of Sicily are the conquest of Egypt and the
creation of Constantinople.</p>
<p>Sub-coastal sites:</p>
<ul class="simple">
<li>Segesta</li>
<li>Himera</li>
<li>Platani survey</li>
</ul>
<p>Inland:</p>
<ul class="simple">
<li>Milena: continuity from 2 c., increase in 4-5 c.</li>

<li>Margi valley: peak of settlements density in Late Roman period</li>
<li>Ramacca area</li>
<li>Simeto valley</li>
<li>Morgantina: peak in Late Antique period</li>
<li>Campanaio</li>
</ul>
<p>In general, Late Antiquity in Sicily sees a widespread increase in
number and size of rural sites.</p>
<p>Sofiana is a small agro-town founded in the Augustan age, during Late
Antiquity it is still 21 hectares. Vaccaro thinks that the old theory
about a satellite settlement depending on the Villa del Casale should
be re-evaluated.</p>
</div>
<div class="section" id="mulvin">

<h3><a class="toc-backref" href="#id10">Mulvin</a></h3>
<p>Mulvin presented archaeological evidence for rural settlements in
Pannonia, including villas and other productive sites.</p>
</div>
<div class="section" id="fentress">
<h3><a class="toc-backref" href="#id11">Fentress</a></h3>
<p>The economy of Numidia was heavily based on the presence of the army.</p>
<p>If one has to pay taxes in cash, it means there's a “free market” of
some sort where one can actually get cash.</p>
<p>The pastoral element of economy was pushed away from areas now
exploited with intensive agriculture.</p>
<p>Speaking of the chronology of sites as seen by survey, and of the
apparent peak of sites in the 5 c., it's important to state again that
we cannot be sure that we see all the early pottery from site surveys.</p>
<p>This 5 c. peak has to do with textile production, it's something that
could have been done in domestic contexts.</p>

<p>Diana Veteranorum's position would fit in a network with
cash-producing provincial towns where bureaucracy is dwelling. Horses
and slaves too are an obvious production for Numidia.</p>
<p>In the 6 c. there's a general abandonment of sites, a phenomenon that
Fentress describes with Wickham's “drop of complexity”.</p>
<div class="section" id="id1">
<h4><a class="toc-backref" href="#id12">Comments</a></h4>
<p>Bruce Hitchner observed that during the Kasserine Survey a large number
of animal enclosures were found.</p>
</div>
</div>
<div class="section" id="mattingly">
<h3><a class="toc-backref" href="#id13">Mattingly</a></h3>
<p>Garamantes are a good example of trade and production outside the
Roman empire - something that already Wheeler had recognized when
facing with terra sigillata in India or Scandinavia.</p>
<p>The basic question he poses is whether or not there is trade in Sahara
prior to the Islamic period.</p>

<p>Oasis' network demands regular contacts between them, allowing
movements of goods, people, animals, ideas along trans-saharian
routes.</p>
<p>The Garamantes are an urban, agricultural society that represent the
emergency of a powerful polity in Central Sahara. Their location is
central for movement from the Mediterranean to sub-saharian regions.</p>
<p>Three families of traveling goods:</p>
<ul>
<li><p class="first">Mediterranean imports (including Punic)</p>
<p>1 c. tombs at Germa have some TSI. There's a sherd with a graffito
reading “Nimir”, maybe a trader. Later tombs have a few imports,
like Keay 62 amphorae and a Hayes 67 dish. Glass is a very valuable
item. Some amphorae are enormous for travelling by camel!</p>
</li>
<li><p class="first">Fazzan exports</p>
<p>Beautiful textiles.</p>
</li>

<li><p class="first">sub-Sahara imports (including e.g. gold)</p>
<p>Migrations of people from these regions are well seen in graves,
e.g. a woman buried following Garamantes' ritual but with
characteristic objects of sub-saharian peoples.</p>
</li>
</ul>
</div>
<div class="section" id="haeverbaeke">
<h3><a class="toc-backref" href="#id14">Haeverbaeke</a></h3>
<p>Haeverbaeke talked about oil production in Anatolia, focussing in
particular on the hinterland of Sagalassos.</p>
<p>In a few points, she thinks that:</p>
<ul class="simple">
<li>oil production was for local consumption</li>

<li>presses weren't used just for olive oil</li>
<li>continuity of village culture is the most important factor for
explaining the diffuse wealth of this region in Late Antiquity</li>
</ul>
<p>According to Mitchell, olive is an important cultivation in the region
during the Hellenistic and Roman period. Olive cultivation requires a
stable political and economical condition, because of the time needed
before the trees are adult. Mitchell believes the during the Late
Roman period oil is instead produced for export.</p>
<p>Instead, palynology has shown the prevalence of mixed orchards,
instead of olive monoculture. This would better fit with a production
intended just for local consumption.</p>
</div>
<div class="section" id="bonifay">
<h3><a class="toc-backref" href="#id15">Bonifay</a></h3>
<p>Bonifay's lecture started with a question: “To what extent do ceramics
reveal markets?”</p>
<p>In Late Roman Africa, there's a completely different situation between:</p>

<ul class="simple">
<li>Tunisia vs other modern states</li>
<li>coastal vs inland sites</li>
<li>quantified and stratified (mostly in the region of Carthage and
Nabeul) vs poorly indagated sites</li>
</ul>
<p>Needless to say, the former cases are respectively much better known
than the latter. In inland regions, cities are mostly studied by means
of excavation, but there are some large surveys.</p>
<div class="section" id="amphorae">
<h4><a class="toc-backref" href="#id16">Amphorae</a></h4>
<p>There are lots of them on coastal sites, from Spain, Crete, Sicily and
of course the majority from Africa itself.</p>
<p>Inland regions have some Mediterranean amphorae, but they are very
rare, only a few sherds. Most amphorae are African, but from where ?</p>

<ol class="arabic simple">
<li>continental</li>
<li>small globular</li>
<li>storage amphorae</li>
</ol>
</div>
<div class="section" id="fine-wares">
<h4><a class="toc-backref" href="#id17">Fine wares</a></h4>
<p>Imports are very rare everywhere. ARS was produced in many workshops,
the main source for Africa was Northern Tunisia. Tripolitania has its
own RSW, but was also supplied by the continental workshop of Sidi
Aïch.</p>
<p>Inland sites are mostly served by central Tunisian products, and
almost no Northern RSW. There are lots of local and regional products,
generally classified as “ARS D” with very bad results for the general
understanding of production.</p>
</div>

<div class="section" id="lamps-and-other">
<h4><a class="toc-backref" href="#id18">Lamps and other</a></h4>
<p>Same as above.</p>
</div>
<div class="section" id="tiles-and-bricks">
<h4><a class="toc-backref" href="#id19">Tiles and bricks</a></h4>
<p>Surprisingly, there are bricks from Italy in Africa, also in some
inland sites (Haïdra).</p>
<p>In general, inland looks very different from coastal regions, both in
quality and quantity of imports.</p>
</div>
</div>
<div class="section" id="poblome">
<h3><a class="toc-backref" href="#id20">Poblome</a></h3>

<p>Poblome gave a very interesting and inspiring lecture about Asia Minor
and Greece, focusing on problems of complexity and multi-scalarity in
understanding the nature of local and regional economies.</p>
<p>Most examples were referred to either Sagalassos or Beotia.</p>
</div>
<div class="section" id="vokaer">
<h3><a class="toc-backref" href="#id21">Vokaer</a></h3>
<p>Vokaer's lecture showed archaeological evidence from Syria. The
overall number of published archaeological sites is very low, but
there are interesting patterns in the spatial distribution of both
imported wares (from Mediterranean and from Parthic region) and local
wares.</p>
<p>Brittle ware, thanks to archaeometric analysis, shows distinctive
patterns of distribution, with significant turnover among the most
important production centres over time. In particular, the most active
regions are the Euphrates area and North-Western Syria, but the city
of Antiochia was always served by a local production.</p>
<p>In the later period, the same forms of some cooking vessels are
produced by different workshops.</p>
<div class="section" id="id2">
<h4><a class="toc-backref" href="#id22">Comments</a></h4>

<p>At the end of this last session, I made two related questions, namely:</p>
<ul class="simple">
<li>how much room is there in our interpretative models for choice and
taste by the “final user” ?</li>
<li>to what extent did final users know the source of pottery they were
buying? and did artisans know about the destination of their work ?</li>
</ul>
<p>Poblome answered briefly to both questions, mostly focussing on taste
(rather than other paradigms for choice) and local trade.</p>
<p>After that, it was 6 o'clock and discussions went on at the nearest
pub.</p>
</div>
</div>
