---
layout: single
title: new year, everything is just the same
author: steko
created: 1136232880
---
Well, seems like 2006 started and 2005 got archived. :)
The iosa.it site is alive and well, with the usual updates and links to useful stuff that one finds here and there surfing the web.
Right now I'm quite involved in <acronym title="Geospatial Information Systems">GIS</acronym>, two weeks ago I followed a class kept by Faunalia people. Check out <a href="http://www.gfoss.it" title="Geographical Free Open Source Software web portal [it]" onclick="window.open(this.href); return false;">www.gfoss.it</a>, it is a new website about open source mapping, free geodata and standards.
<!--break-->
I collect many useful resources as I look for useful stuff on the web, those who aren't related to archaeology, and those for which I didn't manage yet to put herein, are at <a href="http://del.icio.us/steko" onclick="window.open(this.href); return false;" title="my del.icio.us page">del.icio.us/steko</a>. Give it a look, if you like.
I'm putting up the website for a meeting to be held in autumn, I will surely let you know about it. Of course, as usual, the real pain is to have a cool and "right" design, adherence to <acronym title="World Wide Web Consortium">W3C</acronym> standards as <acronym title="eXtensible HyperText Markup Language">XHTML</acronym> and <acronym title="Cascading Style Sheets">CSS</acronym>.
On the 3D side, I'm trying to learn something about X3D and its implementation as a production (read: everyday) format in archaeological 3D modelling. 3D formats can be so confusing, there are dozens and dozens and noone seems to care about this confusion, it's a real and tough pain when you try to share your work. Open Source should always be one step forward in the implementation of open standards and free formats.
And now we have this great thing, a true meeting of true people (the web can be sooo boring sometimes), archaeologists that will discuss and possibly lay down the path for a brighter future for both archaeology, <acronym title="Information and Communication Technology">ICT</acronym> and maybe these half-life creatures we are ;)
Anyway, my lessons will start in 10 ten days, and february brings <strong>exams</strong>!
