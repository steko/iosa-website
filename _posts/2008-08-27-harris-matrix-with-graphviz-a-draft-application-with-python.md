---
layout: single
title: ! 'Harris matrix with Graphviz: a draft application with Python'
author: steko
created: 1219848860
---
[After 8 months](/content/harris-matrix-graphviz) of silence about this interesting topic, at last I've found some time to put online the small draft application I have been writing to demonstrate how to automate the use of Graphviz to generate Harris matrix diagrams for your excavation (or anything else you can study by stratigraphy). The code can be retrieved at http://bitbucket.org/steko/harris/ and there you'll be able to get also future updates.

The application is far from complete and has no <acronym title="Graphical User Interface">GUI</acronym> yet, but at least it shows the model I have developed from the first examples, where all steps were to be performed “by hand”.

From the theory of E. C. Harris, we all know that all stratigraphic relations are bound to what I called an **ABC** model:

- **A** fter
- **B** efore
- **C** ontemporary

This might sound silly but it's a way to make things simple and clear. Data about relations are stored in a SQLite database (accessible with standard Python module since version 2.5). A Graphviz DOT file is generated inside the application and the result can be saved as PNG or SVG image, just change the specific line in the code.

I'm continuing the development of this proof of concept application, but the idea is that this kind of elaboration could be inserted right into one of the many archaeological information systems dedicated to excavation data.
