---
layout: single
title: Irrlicht Engine
author: steko
created: 1128339843
---
The Irrlicht Engine is an open source high performance realtime 3D engine written and usable in C++ and also available for .NET languages. It is completely cross-platform, using D3D, OpenGL and its own software renderer, and has all of the state-of-the-art features which can be found in commercial 3d engines. It has got a huge active community, and there are lots of projects in development that use the engine. You can find enhancements for Irrlicht all over the web, like alternative terrain renderers, portal renderers, exporters, world layers, tutorials, editors, language bindings for java, perl, ruby, basic, python, lua, and so on. And best of all: It&#39;s completely free.
