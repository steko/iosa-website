---
layout: single
title: v.count.points.sh
author: steko
created: 1179755075
---
<p>Last week I was looking at some slides from a lecture here at university. One slide caught my attention more than others, it was a map with some pie charts representing the distribution of different vegetal species in quadrats of an archaeological sites. Quite common and ordinary stuff in publications, indeed.</p>
<p><img src="http://wiki.iosa.it/dokuwiki/_media/spatial_analysis:countpoints-pies.jpg?w=300&amp;h=&amp;cache=cache" alt="the result using d.vect.chart" /></p>
<p>The first question rising to my mind was: <em>how do I do this in GRASS?</em>. It turned out that there wasn't a module to automagically count the number of points within an area, and generate a report that keeps also information about the distribution of different classes. The chart stuff is already available since GRASS 6.0 and before, but I needed a module that could generate data from which I could plot charts.</p>
<p>Thanks to Giovanni Allegri, Markus Neteler I could end up with a bash script that does all this quite well, even though a bit slowly. Hamish Bowman helped a lot ointing out some unsafe pieces of code and helping for a faster code. <a href="http://wiki.iosa.it/dokuwiki/spatial_analysis:feature_count">v.count.points.sh</a> is now the first of a series of tutorials about spatial analysis (this being a very simple form of analysis) for archaeology on the <a href="http://wiki.iosa.it/">Quantitative Archaeology</a> wiki.</p>
<p>Check out the script page on the wiki and let me know if you find errors. Patches are welcome!</p>
