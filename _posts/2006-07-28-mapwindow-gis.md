---
layout: single
title: MapWindow GIS
author: giofish
created: 1154073002
---
The MapWindow application is a ready-to-use spatial data viewer, and a tool that can be modified into a new custom application. Customization is done by editing the MapWindow Configuration File, and/or a MapWindow Project File. These are the two files that control the look and feel of MapWindow. Both are XML-format files that can be edited directly through notepad or an XML editor. MapWindow is an Open Source GIS. For more informations: <a href="http://www.mapwindow.org/mapwinapp.php" target="_blank">http://www.mapwindow.org/mapwinapp.php</a>.
