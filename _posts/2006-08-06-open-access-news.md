---
layout: single
title: Open Access News
author: steko
created: 1154872274
---
[Sayonara, Dyabola](http://www.stoa.org/?p=451), says the latest <a href="http://www.stoa.org">stoa.org</a> news entry.

Herzlich Wilkommen, Open Access. Bibliographies are not everything in archaeology, but they envolve a large part of everyday's work for scholars and professionals without chronological exceptions. So this is welcome news. indeed.

Another important webpage I'd like to put under your attention is <a href="http://sciencecommons.org/literature/scholars_copyright">Scholar's Copyright Project</a> at <a href="http://sciencecommons.org">Science Commons</a>, which deals about a recent effort in providing standard, responsible copyright agreements ensuring the right of scholars to archive their work on the public Internet.
