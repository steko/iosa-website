---
layout: single
title: Open Source applications in archaeology
author: steko
created: 1106945115
---
This is a presentation by <em>A. Bezzi, et al</em> for <abbr title="home page of CAA2004, the XXXII CAA - Computer Applications and Quantitative Methods to Archaeology Conference">CAA 2004</abbr> (PDF - 4.6 Mb).
It describes very well how to perform every single step of archaeological research using open source software. It's worth reading even tough you will find weblinks in our <a href="http://www.iosa.it/directory" title="software directory">directory</a> for most of the software here mentioned.
