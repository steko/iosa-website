---
layout: single
title: geomatics workbooks & archaeology
author: steko
created: 1136580470
---
One notable thing I forgot to mention lately is the following:
At this URI http://geomatica.como.polimi.it/workbooks/n5/list.php you can find an abstract and the full text for all the proceedings of the 6th GRASS Italian Users Meeting. Among these you will find <em>Bezzi et al</em> text, but check out also <em>Pirotti and Vettore</em>  who deal with cultural heritage too.
Last note: to get access to the full text, you have to register to their website: it's free.
