---
layout: single
title: Meeting "Copyright vs Community in the Age of Computer Networks" --Genoa's
  University, in Savona's campus(Italy)
author: luca
created: 1210066789
---
On the 7th of May 2008, there will be a meeting with Richard
Stallman, one of the Free Software's main "philosophers", in Savona's Campus of the University of Genova(Italy).

Richard Stallman is going to discuss about "Copyright vs Community in the Age of Computer Networks" .

It would be a pity to loose the opportunity to meet one of the gurus that has founded GNU Linux and the Free Software Foundation.

More informations writing here : spes@campus-savona.it   .

Thus let's go guys!!!!
