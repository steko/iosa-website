---
layout: single
title: JOONE - Java Object Oriented Neural Engine
author: steko
created: 1150266952
---
Joone is a FREE Neural Network framework to create, train and test artificial neural networks. The aim is to create a powerful environment both for enthusiastic and professional users, based on the newest Java technologies.  Joone is composed by a central engine that is the fulcrum of all applications that are developed with Joone. Joone&#39;s neural networks can be built on a local machine, be trained on a distributed environment and run on whatever device.  Everyone can write new modules to implement new algorithms or new architectures starting from the simple components distributed with the core engine. The main idea is to create the basis to promote a zillion of AI applications that revolve around the core framework.
