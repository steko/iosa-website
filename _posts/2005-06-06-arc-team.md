---
layout: single
title: Arc-Team
author: steko
created: 1118080315
---
The Arc-Team website is online. You should give a look at it because there are lots of documents about the use of open source software in archaeology, in English, Italian and German language. You can also find there the "Open Source Applications in Archaeology" presentation, as the authors are the owners of this website, our good friends.
