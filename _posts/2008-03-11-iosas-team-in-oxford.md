---
layout: single
title: Iosa's Team in Oxford
author: luca
created: 1205252946
---
Thanks to a grant we've got and to the kindness of an archaeological enterprise's manager, a person of Iosa's team is finally go abroad, in England, and he's collaborating actually with the IT-team of a very important European archaeological company.

We hope  that, thanks to the work he's doing and the lots of things he's learning, we could improve the services we offer and the links we're aware of.
This is a great opportunity for us to better know english archaeology's world and to get in touch with more and more people to enlarge the network we bring with Iosa's website.
The hope is obviously also to strengthen even more the know-how of our team and give to you the latest news about open archaeology and free  archaeological software.

Maybe, also thanks to the experiences of our hyperactive staff, a day we will be able to implement FREE archaeological software on our own.
