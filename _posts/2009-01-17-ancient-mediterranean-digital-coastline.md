---
layout: single
title: Ancient Mediterranean digital coastline
author: steko
created: 1232194502
---
I am in the process of writing/building my dissertation, and dealing with pottery distribution maps and such things, I've just noticed that I miss something important: a ”background” map.
Using 21st maps isn't that good, mostly because coastline went under major changes between Roman times and today. In the area of which I'm concerned, I see problems in places like:
<ul>
<li><em>Albingaunum</em></li>
<li><em>Luna</em></li>
<li><em>Pisa</em> and <em>Portus Pisanus</em></li>
<li>large portions of central Tyrrhenian coast (today's Maremma), like at Populonia and Orbetello</li>
<li>the Po delta area and surroundings like at Ravenna</li>
</ul>
All these areas saw huge sedimental processes in action during perhaps Late Antiquity and the Early Middle Ages, changing in substantial ways the coastline shape, the course of rivers and the distance of ancient settlement sites from either sea, rivers or both.
I am looking for an already-digitized version of the Roman Mediterranean coastline, otherwise I'm going to digitize it myself using local studies about coastline change. Any suggestion is highly appreciated!
