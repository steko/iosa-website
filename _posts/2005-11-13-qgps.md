---
layout: single
title: QGPS
author: steko
created: 1131877933
---
QGPS is a small GPS status program that is designed to work with a NMEA-compliant GPS device that outputs the GGA, GSA, GSV, and RMC strings. Aside from displaying latitude, longitude, and elevation, it also reports satellite signal strength (signal to noise ratio) and positions in the sky using a custom widget (QSatelliteTrack). 
