---
layout: single
title: Some (random) thoughts after the workshop
author: steko
created: 1179241425
---
<p>After the 2nd edition of the Italian Workshop "Open Source, Free
Software e Open Format nei processi di ricerca archeologica" [1], which
was held in Genova on May 11th, 2007, I'd like to point out some key
ideas that emerged mostly during the final discussion.</p>

<p>The workshop was not an outstanding success as the first one, but all
the talks presented were very interesting and showed a number of
advances since last year. We can really say that all these projects did
take one step further, as it was asked they should do in Grosseto.</p>

<p>I was glad we had invited to speak Andrea Glorioso from Politecnico di
Torino. His talk about Creative Commons, Science Commons and
copyright/diritto d'autore was illuminating in many parts and his
contribution to the final discussion has been substantial.</p>

<p>The presence of Walter Kuntner and Sandra Heinsch from the University of
Innsbruck was absolutely great and their support for free software as
part of an open knowledge project is really something other universities
should try to emulate.</p>

<p>I'm not going to comment on all talks, even if they would deserve it. At
the end of the day, I tried to summarise in 4 points the desirable
roadmap until next year:</p>

1. Create networks, working side by side whenever we have an opportunity
to do so. We have wikis, blogs, websites to share our experience, but
physical meetings can play a major role in enhancing our "koiné". Let's
try to have more "hands-on" workshops, on the BarCamp model, where talks
are just the start and not the objective.
2. Whenever we are teaching, let's try to introduce students into this
network. Knowing each other is the best opportunity for
cross-fertilization and augmenting of knowledge. In this sense, Benjamin
Ducke's initiative to collect information about courses in quantitative
archaeology & co is one of the first efforts we should contribute to.
3. Many of us are writing code. Let's share it! We have a simple way to
do so, the GPL license, and I'm not going to repeat here the importance
of this best practice that prevents useless work.
4. Sharing our data is a request that many of us are asking since last
year. The situation here is not as clear as for software, and "licenses"
for archaeological data can be different from country to country, as
much as the actual amount of shared data. There are many hypotetical
benefits of a widely spread sharing practice, even though I would
exclude from the list the requests for transparency and objectivity of
archaeological documentation (mostly from excavations) that are usually
used as arguments for data sharing. I think we should look at monumental
existing frameworks, such as the Corpus Inscriptionum Latinarum, to
really get the picture of what can be achieved when all existing data
are out and available.
