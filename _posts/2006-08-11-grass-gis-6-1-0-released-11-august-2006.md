---
layout: single
title: GRASS GIS 6.1.0 released 11 August 2006
author: steko
created: 1155293000
---
The GRASS Development Team announces:

<strong>GRASS GIS 6.1.0 released 11 August 2006</strong>

A new technology preview version of GRASS GIS is being released today in preparation for the next benchmark release, GRASS 6.2. This feature release, version 6.1.0, improves upon the previous stable version of GRASS by adding hundreds of new features as well as support for the latest GIS data formats.

The Geographic Resources Analysis Support System, commonly referred to as GRASS, is a Geographic Information System (GIS) combining powerful raster, vector, and geospatial processing engines into a single integrated software suite. GRASS includes tools for spatial modeling,
visualization of raster and vector data, management and analysis of geospatial data, and the processing of satellite and aerial imagery. It also provides the capability to produce sophisticated presentation graphics and hardcopy maps.

GRASS is currently used around the world in academic and commercial settings as well as by many governmental agencies and environmental consulting companies. It runs on a variety of popular hardware platforms and is Free open source software released under the terms of the GNU General Public License.

Joining GRASS's well-developed raster engine the GRASS 6 series introduced a new topological 2D/3D vector engine featuring support for vector network analysis and SQL-based DBMS management of linked attributes. This release improves the integration and functionality of the raster and vector engines, and 3D raster (voxel) support has been greatly enhanced.

A new graphical GIS manager and menu system has been implemented, while the old GUI display manager has been retained and improved for legacy support. The NVIZ visualization tool has been enhanced to display 3D vector data and voxel volumes and supports the creation of on-the-fly MPEG animations. Additional improvements include substantial message translation (i18n) with support for FreeType fonts, including multi-byte Asian characters, and tools to allow new project locations to be automatically generated from input data or EPSG code.

This is the first release of GRASS as a proposed founding project of the new Open Source Geospatial Foundation. In support of the movement towards consolidation in the open source geospatial software world, GRASS is tightly integrated with the latest GDAL/OGR libraries. This enables access to an extensive range of raster and vector formats, including OGC-conformal Simple Features. GRASS also makes use of the highly regarded PROJ.4 software library with support for most known map projections and the easy definition of new and rare map projections via custom parameterization.

Full announcement:
 http://grass.itc.it/announces/announce_grass610.html

Short project description
 http://grass.itc.it/announces/abstract_grass610.txt
