---
layout: single
title: ADS and adblocker
author: steko
created: 1207130547
---
Today I was looking for a <acronym title="Late Roman Amphora 1">LRA1</acronym> link on the amphorae catalogue on the <acronym title="Archaeological Data Service"><a href="http://ads.ahds.ac.uk/">ADS</a></acronym> website. I quickly found what I was looking for http://ads.ahds.ac.uk/catalogue/archive/amphora_ahrb_2005/details.cfm?id=236 but I noticed that my browser (<a href="http://www.gnome.org/projects/epiphany/">Epiphany</a>) wasn't showing the pictures, neither on the summary page nor in the detailed pictures page. I tried with Iceweasel^W<a href="http://www.getfirefox.com">Firefox</a> and the pictures were there. The HTML source was a plain <code>&lt;img&gt;</code> tag. I started thinking about a bug in Epiphany: this is what free software makes you learn - that when things don't work as expected most of the times it's just another bug. But the solution was quite different...
Computers are really good at their dumb work, and the adblocker is no exception, so the images from the <code>http://ads.*</code> domain were being blocked, just like any other spam ad service. Solving the problem was just a matter of adding a whitelist rule to the adblocker configuration, but it was funny enough to share it with you.
<div style="text-align:center"><img src="http://ads.ahds.ac.uk/catalogue/adsdata/amphora_ahrb_2005/ahds/dissemination/jpg/photos_small/PEC109.jpg" alt="Late Roman Amphora 1" /></div>
