---
layout: single
title: ANS Digital Publications Project
author: steko
created: 1159908210
---
The <a href="http://www.numismatics.org">American Numismatic Society</a> is developing an infrastructure for the digital publication of numismatic catalogs, exhibitions, articles, and other materials. As a general rule, this system will take advantage of existing standards and tools whenever possible. This means that texts will be encoded using the Extensible Markup Language (XML) and that the Text Encoding Initiative&#39;s (TEI) xml dtd&#39;s and schemas will be used where appropriate.
