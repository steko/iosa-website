---
layout: single
title: Public Domain Dedication & Licence
author: steko
created: 1198262257
---
The new Open Data Commons blog has published the first draft of the <a href="http://www.opendatacommons.org/2007/12/17/licences-now-available-for-comment/">Public Domain Dedication and Licence</a>. This license basically does what the Open Access Data Protocol states: the only sane way to have open access databases is a full public domain (in countries where this is allowed), with <em>no rights reserved</em>, waiving all rights based on intellectual property.

This means two things that could sound bad for those who like free/open source software and creative commons:

- there are no restrictions, thus no _share-alike_ clause: people should be able to do anything with data
- also the citation of the source cannot be enforced, quite differently from academic standards where citation of your sources is, at least, a good practice

How many archaeologists are disposed to give away entire databases that are often the results of years of work? I hope that lots of us take the time to think about this proposal. In the first place, public officers should at least consider this as an option.
