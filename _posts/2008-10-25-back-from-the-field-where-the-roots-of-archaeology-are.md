---
layout: single
title: Back from the field, where the roots of archaeology are
author: steko
created: 1224968281
---
You might have noticed a lack of updates here at iosa.it, and the cause is simple: I was again on the field doing the second excavation campaign of this year.

I spent 5 weeks in Vignale (Piombino - LI), near Populonia for those who know this more famous Roman site. Our excavation investigates a Roman <em>villa/mansio</em> which lives from the 1st BC century to at least 5th AD.

Among the most interesting things I have been doing while digging are:

- developing <a href="http://totalopenstation.sharesource.org/">Total Open Station</a>, which is now fully functional on GNU/Linux systems (Windows and Mac OS X testing still to come)
- investigating interpolation algorithms for creating a DEM of the pre-Roman palaeosoil at the site
- acting as protagonist in a 5-minute short film set <em>in our excavation</em> (could be uploaded to Youtube in the future)

And, most importantly, it's the first year since 2005 in which I had more than one campaign (Gortyna being the “master” one). It's vital not to become too tied just to one site.
