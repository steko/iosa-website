---
layout: single
title: Total Open Station
author: steko
created: 1205515364
---
I started a new software project recently. The project is called Total Open Station. From the README:

```
I started writing this program for making it easier to manage different
models of total stations in a GNU/Linux environment. However, I try to
develop following standards, so porting to Windows or Mac shouldn't be
difficult.
Every model has its own quirks. So, I decided to use a modular
structure, that is based on an abstract interface. Each time a new model
is added to the program, you create a new instance of this base class,
with all needed data.
```

Total Open Station is developed with the <a href="http://www.python.org/">Python</a> programming language.

For those who are interested in participating the project website is at http://sharesource.org/project/totalopenstation/ 

The (few lines of) code that I have written as of today are here in a Mercurial repository: http://hg.sharesource.org/totalopenstation/ 

Let me know if you want to help - even a test with your total station is welcome!
