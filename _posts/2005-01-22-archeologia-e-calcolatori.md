---
layout: single
title: Archeologia e Calcolatori
author: steko
created: 1106415987
---
This journal was set up with the idea of publishing in an organic and systematical way the results of computerised research carried out in the field of historical archaeology; to offer an up-to-date edition of ongoing projects both in Italy and abroad; to pave the way for new developments in computer applications. The chronological period under examination does not exclude, but rather subordinates, the more specialised sectors of prehistoric archaeology, to the advantage of requirements and applications relative to the classical and post-classical period.
