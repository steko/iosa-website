---
layout: single
title: New radiocarbon calibration data, and of the importance of standards (and of
  software that follows them)
author: steko
created: 1264073997
---

The last special issue of the <a
href="http://www.radiocarbon.org/index.html">Radiocarbon journal</a>
marked a big step forward in radiocarbon dating. The IntCal04
calibration curve was available for a period that goes back to 26000
years BP, while the new IntCal09 data extends calibration back in time
until 50000 years BP, pretty much covering the entire time span that
can be obtained by means of <sup><small>14</small></sup>C. Radiocarbon
scientists believe the availability of this new calibration curve,
together with some adjustments and updates for already covered
periods, will allow a lot of archaeological sites to get better
absolute dates, including those from the age of transition between
Neanderthals and modern humans in Europe and the Mediterranean. The
IntCal working group will continue to enhance the available data and a
new issue is already planned for 2011.

The <a href="http://www.radiocarbon.org/IntCal09.htm">new curves</a>
follow the same plain <acronym title="comma separated
values">CSV</acronym> data format. Since 2008, I have been writing a
simple <a href="http://c14.iosa.it/">programming library for the
calibration of radiocarbon dates</a>, in the Python language (this
program is still highly experimental and is going to see its name
changed soon). The program was written to read calibration data in
<tt>.14c</tt> format, given that this is the current standard used by
almost all other calibration programs like OxCal, CALIB and others. As
soon as I found that new calibration data were released, I knew I
wanted to try out my program with them. And I couldn't help a moment
of satisfaction when I could seamlessly add the two calibration files
in the data directory and do a test calibration:

<a href="http://www.flickr.com/photos/steko/4292951584/" title="Schermata-IOSA Radiocarbon Calibration Service di archeosteko, su Flickr"><img src="http://farm5.static.flickr.com/4052/4292951584_6388b936d3.jpg" width="500" height="313" alt="Schermata-IOSA Radiocarbon Calibration Service" /></a>

Let alone my satisfaction, I think there are two good things in action here

- data that follows _consistently_ and _continuously_ a clear, plain,
  standard format
- software that follows the same standards in a sleek, transparent way

We don't need much more than this to be able to exploit the rich
amount of archaeological data that will become available in the next
years.
