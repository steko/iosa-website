---
layout: single
title: KPovModeler
author: steko
created: 1117319399
---
KPovModeler is a modeling and composition program for creating POV-Ray(TM) scenes. It is based on the KDE graphic libraries.and of course there's only a GNU/Linux version.

For most of the modelers, POV-Ray is nothing but a rendering engine and they bring a lot of limitations to the innate possibilities of POV-Ray scripted language. This is not the case for KPovModeler which allows you to use all the features of POV-Ray through the translation of POV-Ray language into a graphical tree.

Almost all options of POV-Ray's script language can be used within KPovModeler. "Almost" because variables, loop instructions, macros and some operators can't directly be, unfortunately. On the other hand, KPovModeler allows you to include a part of a script with the "Raw POV-Ray" tool; such a raw code will only be taken into account by POV-Ray during the rendering stage. 
