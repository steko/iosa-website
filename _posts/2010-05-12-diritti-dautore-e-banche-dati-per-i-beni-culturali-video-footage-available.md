---
layout: single
title: ! '“Diritti d''autore e banche dati per i beni culturali”: video footage available'
author: steko
created: 1273674830
---
We have finally managed to put online the video footage of the seminar "Diritti d'autore e banche dati per i Beni Culturali" that we had last year in Genoa on 7 May 2009, organized by <a href="http://www.grupporicerche.it">grupporicerche</a>.
You can read a brief report and watch videos at this URL: http://www.iosa.it/diritti/ - video files are in the open and free <a href="http://www.theora.org/">OGG Theora</a> format, and hosted at the <a href="http://www.archive.org/">Internet Archive</a>. It's all in Italian, we haven't managed to create subtitles yet. If you need help, don't hesitate to ask.
We really hope that this material will be useful to anyone trying to push for open archaeology. More meetings like this one will certainly help clarifying the main issues in the field of copyright assignment and dissemination of archaeological data under free licenses.
