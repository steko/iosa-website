---
layout: single
title: Archaeological remote sensing with GRASS imagery modules
author: steko
created: 1201688307
---
[PerryGeo](http://www.perrygeo.net/wordpress/?p=104) has an interesting post on his blog about using the GRASS <code>i.*</code> modules to find impervious surfaces. I have no experience in archaeological remote sensing (even tough here at my department there's a <a href="http://www.lapetlab.it/">great lab</a>), but this tutorial should be useful for those who have large survey regions to cover and want to find possible settlement sites, given that even a small village needs a few “usable” acres.

If you have good resolution aerial images of your study region, you might want to try this.
