---
layout: single
title: ! 'ArcheoFOSS 2010: posticipata al 19 febbraio la scadenza per presentare abstract'
author: steko
created: 1265059612
---
Forward from the Scientific Committee:
> [V]i comunico che la scadenza per la presentazione degli abstract è stata **posticipata dal 30 gennaio al 19 febbraio**

AGGIORNAMENTO PRINCIPALI SCADENZE:

- 19 febbraio 2010: Termine per la presentazione abstracts
- 8 marzo 2010: Notifica dell'accettazione e pubblicazione web degli abstract
- 15 aprile 2010:Termine per la consegna dei paper camera ready


E' possibile proporre contributi su ogni aspetto inerente il progetto, lo sviluppo e l'uso di formati liberi e/o di software libero o open source in archeologia.

Sono previste sessioni per la presentazione di papers soggetti a recensione.

E' prevista anche una sessione poster per la presentazione di progetti in corso o recentemente conclusi.

Anche in questa edizione sarà organizzata una specifica sessione di laboratorio (OpenLAB). Quest'anno i laboratori dedicati all'incontro diretto con gli strumenti Open Source saranno dedicati a modellazione 3D
applicata all'archeologia, gestione di mesh 3D con particolare riferimento ai Beni Culturali e creazione e gestione di Database e applicazione in campo archeologico.

Per tutte le informazioni sull'evento vi invito a visitare il sito www.archeologiadigitale.it/archeofoss/2010.html
