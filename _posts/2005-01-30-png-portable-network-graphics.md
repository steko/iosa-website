---
layout: single
title: PNG (Portable Network Graphics)
author: steko
created: 1107101862
---
PNG is an extensible file format for the lossless, portable, well-compressed storage of raster images. PNG provides a patent-free replacement for GIF and can also replace many common uses of TIFF. Indexed-color, grayscale, and truecolor images are supported, plus an optional alpha channel for transparency. Sample depths range from 1 to 16 bits per component (up to 48bit images for RGB, or 64bit for RGBA).

The PNG specification was first issued as a W3C Recommendation on 1st October, 1996 (press release) and updated to a second edition incorporating all errata on 10 November 2003. This edition is also an ISO standard, ISO/IEC 15948:2003 (E).

This means it is a mature document that is considered to contribute towards realising the full potential of the Web. Viewers for PNG are available on many platforms; there are an increasing number of content creation tools available; and thus modern browsers implement support for it also.
