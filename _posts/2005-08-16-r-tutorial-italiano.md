---
layout: single
title: R tutorial (italiano)
author: steko
created: 1124201909
---
Italian set of downloadable and online tutorials on R, the "GNU S" application for statistical computing. Quite outdated but still very useful, the language has evolved but the basics are the same.
