---
layout: single
title: Archaeology as text and archaeology as image
author: steko
created: 1290583663
---
Last April I started to collect some sketch notes about archaeology as text. It's not about reading archaeological remains as a text to be read (this could be considered quite a standardized post-processual approach) but rather about how we gain new archaeological knowledge. I quickly came to affirm that:

> *§ A large part of archaeological knowledge is transmitted by means of text, including books, journals, excavation reports and diaries, database alphanumeric records, and other.*

So, rather than being a *primary source* (speaking in terms of historical archaeology), archaeology itself becomes a *secondary source*, that needs to be managed and approached from another point of view. Reading long descriptions of soil layers on top of collapsed walls is not the same as digging the same for yourself. Going through massive tables of quantified data about archaeological pottery is another thing than working for months on a ceramic assemblage to produce those tables. One might argue that for experienced archaeologists there is no difference between the two, as they know exactly what the author of a certain text wants to tell ‒ I accept the fact that textual communication is taking place without any errors in such cases, but I question the identity between written archaeology and material archaeology. Rather than falling back into the 12th century and the the problem of universals, I'm interested in a reflexive approach to the creation of archaeological knowledge.

Text dominates the transmission of archaeological knowledge (and sometimes I've heard words of blame towards glossy books with lots of images and little text providing a structured discourse). Especially in the sub-domain of excavation reports, there is a distinct, formalized ‒ artificial we might say ‒ language, that is targeted to bureaucracy rather than to conveying meaning. I like to read those reports and I think they are the main source for what I know about Late Antique Italy, for example. I'm always surprised at their diversity, and still I can find striking similarities among most of them, first and foremost in how the description of excavated contexts and features is kept logically and physically separated from the presentation of finds (like ceramics and coins).

It wasn't always like this. According to [Gavin Lucas](http://www.hi.is/is/simaskra/3199), there has been a clear change in how archaeological publications (particularly excavation reports) mix text and images. There's a quote attributed to [Augustus Pitt Rivers](http://en.wikipedia.org/wiki/Augustus_Pitt_Rivers) that captures the distance:

> Don't illustrate your descriptions. Describe your illustrations.

(G. Lucas, *[Critical Approaches to Fieldwork](http://openlibrary.org/works/OL5709682W/Critical_Approaches_to_Fieldwork)*, p. 211)

In my research group at the University of Siena, we have been using a like-minded approach for two years now, and we use semi-aerial photographs of our excavation areas as drawing boards for taking notes and sketching interpretive plans of building rooms. We call them “annotated maps” in a consciously critical view of how GIS is currently used for on-site archaeological data recording, and try to find a mix of text, objective representation of materialities and (multiple) interpretations. It's not always perfect, and there are some things that can go wrong.

This debate also involves how archaeological photography is used. As with any (disruptive) technology, my view is that it's far from being an objective recording technique, unless we deliberately adopt *very detailed* instructions on how to take photographs: this is more or less what has happened with images of trenches and contexts. Personally, I can't see any advantage in applying such mechanical procedures, apart from a reassuring homogenization. It's not by chance that both text and images have undergone the same process of formalization. The prevalence of text above images is likely explained by its abstract nature, when compared to the (apparent) fidelity of photography to the materiality of archaeology.

*This is the first of two posts dealing with archaeology, text and media. The next post will appear next week and is entitled “Archaeology beyond text and media”*
<!--break-->
