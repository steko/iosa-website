---
layout: single
title: Ubuntu is the open source platform of choice for Oxford Archaeology
author: steko
created: 1212499586
---
[Oxford Archaeology](http://thehumanjourney.net/) (which I've visited last week by the way) is covered on the Ubuntu website as a successful case study for the migration of a whole company IT services to free/open source software and operating systems: http://www.ubuntu.com/products/casestudies/oxford-archaeology

I guess there are more companies and departments out there that share common goals and problems with OA, so the point is: has Chris Puttick and all of his colleagues gone totally mad, even being one of the largest contract archaeology companies in the UK? Or are they rather making the Right Choice™ in the long term, choosing not only to use free software but also to develop new custom solutions and give something back to the community?
