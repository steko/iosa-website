---
layout: single
title: GRASS Development - QGis modules
author: steko
created: 1114874818
---
It is very easy to add new QGIS-GRASS modules and it would be nice to have as many modules as possible in the coming 0.7 release.
Radim would like to invite everybody to add new modules. No programming skills are necessary.
Please see <a href="http://www.faunalia.it/grass/" title="GRASS Development" onclick="window.open(this.href); return false;">http://www.faunalia.it/grass/</a>.
