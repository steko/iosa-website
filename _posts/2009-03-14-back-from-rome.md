---
layout: single
title: Back from Rome
author: steko
created: 1236985597
---
I'm back in Siena, after 3 days spent in Rome (hosted by Elena). On Wednesday  I went to the American Academy, even though I was missing an important piece of the requirements they were so nice to let me in for one day. I planned to go for 3 days there, but I managed to go to the British School Library yesterday and today. Of course it's all for my dissertation, you know.
<!--break-->
I was in desperate need of dozens of excavation reports and potsherd counts, and I came back pretty satisfied with regard to that, now I think the main problem will be to stick with the Siena department's library, which is not as large as the AAR or BSR ones. Having <em>any</em> book you need available is a powerful drug, believe me.
The following notes are mainly a reference for myself, but I thought they might be useful to someone else.
If you need to go the the <a href="http://www.aarome.org/library.php">American Academy</a>, just take Metro B until Circo Massimo then bus 75 and get off at Carini-Bonnet. There's no need to worry about food because there's a bar <em>inside</em> the American Academy. You can even have a complete lunch there, if you feel like having it. I tried to spend 100% of my time there on books, so I just took a sandwich. If you ever want to go to the American Academy's Library, make sure you contact the Librarian Office by means of the contact form on their website. Otherwise, they won't probably let you any way in, unless you come from a far away land like me.
The <a href="http://www.bsr.ac.uk/bsr/sub_libr/BSR_Libr_01library.htm">British School</a> policy is less strict, so you just need a letter of introduction.  I think they have less books than at the American, but the BSR is more specialized towards archaeology so most books I was looking for were in both libraries. The BSR can be reached with Metro A, get off at Flaminio, then it's just some hundred meters away (follow Via di Villa Giulia).  For lunch, be sure you understand that the door is closed from 1 PM to 2 PM (so you're either in or out), and find your sandwich at the Arch-Bar in via Gramsci.
