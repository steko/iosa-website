---
layout: single
title: ! 'Archaeology and Computing meetings: the "epic fail" year'
author: steko
created: 1246388346
---
Every year we try to go at least to one large archaeoinformatics meeting in Europe (other than our Italian workshop). It's a neat way to meet new and old friends, keep ourselves (and our readers) updated about the latest achievements in the field, and let the world know what we have been doing lately, possibly gaining an increasingly wider audience for the whole “Open Archaeology” concept.
This year marks an <em>epic fail</em> in the organization of such conferences/meetings/workshops. Between November, 16th and 18th you can choose where you want to be: Heidelberg or Wien, “Archäologie und Computer” or “Scientific Computing and Cultural Heritage”. Both venues will do because both events have the very same dates, you see.
Now, I have been myself in the organizing committee of a couple of such events, and I know that once your dates are fixed it is almost impossible to change them. And of course I'm not even trying to say the organizers of the two conferences weren't good at doing their work.
It's just that there aren't so many <em>interesting</em> conferences: the Heidelberg one is relatively “young” compared to the Wien workshop, but both are probably much more ”human” than CAA. I don't know yet if I'm going, however.
For some bad coincidence, nobody will be able to follow both, and it's really a pity. I hope next year we'll have more luck!
