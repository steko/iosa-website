---
layout: single
title: What have I done in the past 4 months?
author: steko
created: 1241460653
---
The <em>aficionados</em> readers and users of iosa.it, if any, were probably asking themselves what was going on, given the substantial lack of updates here.

To make a long story short, I got my master degree <em>cum laude</em> in Late Antique Archaeology two weeks ago, on April, 21st (2762 a.U.c.). It took me several months of hard work, and obviously everything else, including this website and activity around it, hasn't been receiving lots of care. In a future post, I'll try to describe in detail my current situation and wishes concerning university, job and research.

My dissertation was entitled <em>Defining complexity. The “circulation” of african red slip ware in Italy 400-700 AD</em>. I've approached the subject by collecting a quite large dataset of published pottery from more than 100 archaeological sites in Northern and Central Italy. In the meanwhile, I've done my best to understand the most popular quantification methods (as usual, the good things about standards is that there are so many you can choose from) and their respective implications, even though in the end I've done very little work using quantification data, concentrating more on presence data (which have their bias problems, too). My analysis has been concentrated mainly on 3 directions:
<!--break-->

- the importance of the Byzantine presence in Italy after 533 (advocated in first place by Enrico Zanini, my supervisor, already more than 10 years ago)
- differences between coastal sites and inland (taking into account navigable waterways as a third variable)
- differences between urban, rural and <em>other</em> settlement types (like military ones)

I've briefly touched lots of other topics, like the relationships between “Men and pottery” (an expression stolen from the great book by Enrico Giannichedda) from the point of view of users , lifespan of objects and different distribution patterns between productions C and D of african red slip ware between 400 and 700 AD.
I've enjoyed spending these months reading literally dozens of excavation reports, from which I've incidentally learned a lot about late antiquity and the early medieval centuries in Italy. I've not studied pottery directly, but I have nevertheless studied some important phenomena that pottery can explain and give some insight of.
Complexity has not been the actual subject of my research, but rather a paradygm for it. For my part, I've tried to define complexity as a result of global historical and cultural processes mixed with regional trends and the immutable patterns of Mediterranean history, in a melting pot of different people, objects, cultures and changes usually defined as the fall of the Roman Empire.

My plans for this research include, among the other things:

- publishing a “standard” academic paper, discussing my results and methods;
- making my database freely available online for anyone to use and remix, even though I've yet to clearly understand which (combination of) licenses I should use for it;
- sharing the software tools I've developed for collecting and analyzing the collected data under a free/open source license (probably a BSD-like license, given that most of what I've done is a <a href="http://geodjango.org/">GeoDjango</a> app;
- continuing research, particularly concerning quantification methods and their potential for the evaluation of absence of particular types (e.g. Hayes 104) or entire productions (e.g. ARS), and different tableware consumption between town and countryside.

I'll give news about all these things on this blog as usual. I would be very glad to receive comments from anyone interested.
