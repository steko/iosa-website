---
layout: single
title: K-3D
author: steko
created: 1122492703
---
K-3D is the free-as-in-freedom 3D modeling, animation, and rendering system for GNU / Linux, Posix, and Win32 operating systems. K-3D features a robust, object-oriented plugin architecture, designed to scale to the needs of professional artists, and is designed from-the-ground-up to generate motion-picture-quality animation using RenderMan-compliant render engines.
It is avalaible under the GNU GPL license for GNU/Linux and Microsoft Windows.
