---
layout: single
title: Blender and Yafray as an Integrated External Renderer
author: steko
created: 1124657427
---
Yafray integration is one of the best features added to Blender. The current release of Blender 2.35 has a very neat integration with Yafray from within Blender, and is fairly stable to use. Unfortunately, Yafray usage is limited to those who are already comfortable with the Blender interface or can hack their way through. The first part of the tutorial deals with the basic steps needed to render from Yafray and later parts serve as a guide for the rest of the Yafray's available features.
