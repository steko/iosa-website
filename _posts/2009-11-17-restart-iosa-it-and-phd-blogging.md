---
layout: single
title: Restart iosa.it, and PhD blogging
author: steko
created: 1258471226
---
It's been a long time now since my last update here. However this website, the IOSA project and its head (that is, me) are not dead at all.
First of all, the good news. As some friends already know, I'm now a PhD candidate in Archaeology at the University of Siena, with a research project about the anthropology and sociology of late antique societies as seen by an archaeologist (which first of all means pottery, pottery and again pottery). I'm going to stay in Siena 3 more years, but I'll make my best to travel around for conferences and studying purposes in general.
This partly explains the lack of news here, and why I've been really busy until now - not that I won't be from now on of course ... I'm doing a serious promise about PhD blogging, and I'm going to post regularly about my research and findings (if any).
On the “archaeoinformatics” side of the story, we're right now defining the skeleton programme for the next “Archaeology and Free Software/Data/Formats“ workshop, that will take place in Foggia next spring. 
And we're also planning an R-chaeology unconference. And we'll probably send out a workshop proposal for the next CAA confence in Granada (April 2010). And many other things. I'll write in detail one piece at a time.
That's all for today.
