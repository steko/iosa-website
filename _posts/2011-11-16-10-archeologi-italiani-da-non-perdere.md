---
layout: single
title: ! '10 archeologi italiani da non perdere '
author: steko
created: 1321463071
---
Il blog [Archeologia 2.0](http://archeologiaduepuntozero.blogspot.com/) ha stilato un [elenco](http://archeologiaduepuntozero.blogspot.com/2011/11/10-archeologi-italiani-da-non-perdere.html) dei 10 archeologi italiani più attenti all'innovazione digitale, alle nuove tecnologie, e agli sviluppi dei media sociali.

IOSA.it è tra questi dieci, e si parla anche del [wiki di archeologia quantitativa](http://wiki.iosa.it/). Ma non è tutto qui, perché tra questi dieci c'è anche Giuliano De Felice che con me e con molte altre persone ha
portato avanti ArcheoFOSS negli ultimi anni. Lo considero un giudizio positivo su ArcheoFOSS nel suo complesso e sulla comunità che vi ruota intorno, con esperienze consolidate (tra cui IOSA) e quelle in crescita (gNewArchaeology, ad esempio). E altri due tra i "nominati" sono passati da ArcheoFOSS, ovvero Helga Di Giuseppe (Padova 2008) e Gabriele Gattiglia (Roma 2009).

Ma c'è anche uno spunto per riflettere, per porsi domande e cercare insieme le parole per costruire le risposte più adatte a quello che ognuno di noi fa. Innovazione non è solo crescita tecnologica e progresso luccicante: ci sono da creare competenze a partire dalla formazione di base, c'è una nuova dimensione pubblica e non intellettuale dell'archeologia ancora tutta da pensare (a meno di non voler diventare noi stessi reperti archeologici). C'è, tra le altre cose, da iniziare a pensare cosa diamo in cambio del software libero che usiamo.

Voi cosa ne pensate? Io penso che ArcheoFOSS sia una fucina di
innovazione e sperimentazione, e questo elenco è una piccola cosa ma sembra andare in quella direzione. Avanti così, allora.
