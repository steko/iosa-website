---
layout: single
title: Museo&Web
author: steko
created: 1144165076
---
Produced by the EU MINERVA project, this is a <acronym title="Content Management System">CMS</acronym> built specifically for small/medium museums. It's prototype that is fully compliant with XHTML, CSS and WAI recommendations. 

Museo&Web is distributed under the GNU GPL license.
