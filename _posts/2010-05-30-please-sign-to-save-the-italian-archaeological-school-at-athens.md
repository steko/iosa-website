---
layout: single
title: Please sign to save the Italian Archaeological School at Athens
author: steko
created: 1275234853
---
<p>We're unfortunately getting used to this kind of announcements about cuts for research and education bodies. Let's not get used to culture as a minor, unessential ingredient of our society.</p>

<blockquote><p><em>Anche quest'anno la cultura dovrà pagare le mancate riforme strutturali del paese. 
La SAIA è tra gli enti, istituti e fondazioni che non riceveranno più finanziamenti dallo Stato, decretando così la morte della ricerca archeologica italiana in Grecia, che dura da più di 100 anni, e la fine della più importante scuola di formazione archeologi italiani.</em></p>

<p><em>Da oltre un secolo, dapprima come spedizione scientifica di singoli studiosi, poi come Missione stabile ed, infine, nella qualità di Sede ateniese per ricerche e scavi archeologici in Grecia e nelle aree di civiltà ellenica e per la formazione e la specializzazione di giovani studiosi, la Scuola Archeologica Italiana di Atene è il punto di riferimento di tutti gli archeologi e gli storici dell'antichità che dalle Università, dal CNR o dalle Soprintendenze svolgono attività di ricerca in Grecia.</em></p>
<p><em>Nata per favorire l'alta formazione dei funzionari delle Soprintendenze archeologiche italiane (ruolo che ha svolto in passato in modo egregio e che continuerà a svolgere in futuro) e come centro di coordinamento delle Missioni italiane in Grecia (e per un certo tempo anche in Oriente) la Scuola ha costituito sin dall'inizio la sintesi tra due funzioni basilari: formazione e ricerca, ospitando anche laureati in architettura che si occupano di restauro, conservazione e studio dei monumenti.</em></p></blockquote>

<p>If you can, please sign the <a href="http://www.petizionionline.it/firme/cessazione-dei-finanziamenti-alla-saia-cronaca-di-una-morte-annunciata/1363">petition</a> to save the Italian Archaeological School at Athens.</p>
<!--break-->
