---
layout: single
title: Stereo is back (?)
author: steko
created: 1217244089
---
It looks like the old Stereo photogrammetry software has been revamped recently. There's a new page on Sourceforge: http://stereo.sourceforge.net/

But it looks like, at least for now, it's just the same old version (11 years old) by Paul Sheer, which requires GCC 2.95 to compile and a number of other obsolete requirements. The software is also said to be stable and usable, but as far as I remember, it's really far from stable, especially for what concerns the application GUI. I guess probably the algorithms can still be re-used, but the software definitely needs a true revamping IMHO.

Otherwise, better wait for something better to come out from Luca's experiments at Oxford Archaeology with the BIAS C++ Imaging Library...
