---
layout: single
title: SVG (Scalable Vector Graphics)
author: steko
created: 1107101713
---
SVG is a language for describing two-dimensional graphics and graphical applications in XML. SVG 1.1 is a W3C Recommendation and forms the core of the current SVG developments. SVG 1.2 is the specification currently being developed as is available in draft form (comments welcome). The SVG Mobile Profiles: SVG Basic and SVG Tiny are targetted to resource-limited devices and are part of the 3GPP platform for third generation mobile phones. SVG Print is a set of guidelines to produce final-form documents in XML suitible for archiving and printing. sXBL is a binding language for SVG content.
