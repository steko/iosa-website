---
layout: single
title: Blender Conference 2005
author: giofish
created: 1119625475
---
The Blender Foundation is pleased to announce that the Blender Conference 2005 will be taking place at De Waag in Amsterdam, the Netherlands from October 14-16, 2005.
For more information: <a href="http://www.blender3d.org/cms/Blender_Conference.52.0.html" target="_blank">http://www.blender3d.org/cms/Blender_Conference.52.0.html</a>.
