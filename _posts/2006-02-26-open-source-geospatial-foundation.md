---
layout: single
title: Open Source Geospatial Foundation
author: steko
created: 1140944872
---
The Open Source Geospatial Foundation has been created to support and build the highest-quality open source geospatial software. The foundation's goal is to encourage the use and collaborative development of community-led projects. The website serves as a portal for users and developers to share their ideas and contribute to project development.
Among the founding projects, GRASS GIS, UMN Mapserver, the GDAL libraries, Mapbuilder, and more.
