---
layout: single
title: A look at pollen data in the Old World
author: steko
created: 1276602897
---
Since the 19th century, the study of archaeobotanical remains has been very important for combining "strict" archaeological knowledge with environmental data. Pollen data enable assessing the introduction of certain domesticated species of plants, or the presence of other species that grow typically where humans dwell. Not all pollen data come from archaeological fieldwork, but the relationship among the two sets is strong enough to take an interested look at pollen data worldwide, their availability and most importantly their **openness**, for which we follow the [Open Knowledge Definition](http://www.opendefinition.org/).

The starting point for finding pollen data is the [NOAA website](http://www.ncdc.noaa.gov/paleo/pollen.html).

The [Global Pollen Database ](http://www.ncdc.noaa.gov/paleo/gpd.html) hosted by the NOAA is a good starting point, but apparently its coverage is quite limited outside the US. Furthermore, data from 2005 onwards aren't available via [FTP](ftp://ftp.ncdc.noaa.gov/pub/data/paleo/pollen/) in [simple documented formats](http://www.ncdc.noaa.gov/paleo/pollen/polfileformat.html), but are instead downloadable *as Access databases* from [another external website](http://www.neotomadb.org/). Defining Access databases as a Bad Choice™ for data exchange is perhaps an euphemism.

Unfortunately, a large number of databases covering single continents or smaller regions is growing, and the approaches to data dissemination show marked differences.

### Americas

For both North and South America, you can get data from more than one thousand sites directly via FTP. There are no explicit terms of use. Usually, data retrieved from federal agencies are public domain data.

The [README document](ftp://ftp.ncdc.noaa.gov/pub/data/paleo/pollen/readme_pollen.txt) only states ``NOTE: PLEASE CITE ORIGINAL REFERENCES WHEN USING THIS DATA!!!!!``. Attribution is consistent with the requirements of the [Open Knowledge Definition](http://www.opendefinition.org/), and it's also good scholarly practice.

* [North American Pollen Database](http://www.ncdc.noaa.gov/paleo/napd.html)
* [Latin American Pollen Database](http://www.ncdc.noaa.gov/paleo/lapd.html)
* [Global Pollen Database](http://www.ncdc.noaa.gov/paleo/gpd.html)

### Europe

From the GPD website we can easily reach the [European Pollen Database](http://www.europeanpollendatabase.net/), that is found at another website tough (and things can be even more confusing, provided that the NOAA website has some dead links).

You can download EPD data in PostgreSQL dump format (one file for each table, with a separate SQL script ``create_epd_db.sql``). Data in the EPD can be restricted or unrestricted. That's fine, let's see how many unrestricted datasets there are. Following the database documentation, the ``P_ENTITY`` table contains the use status of each dataset:

<code>
steko@gibreel:~/epd-postgres-distribution-20100531$ cat p_entity.dump | awk -F "\t" {' print $5 '} | sort | uniq -c
    154 R
   1092 U
</code>

which is pretty good because almost 88% of them are unrestricted (NB I write most of my programs in Python but I *love* one liners that involve ``awk``, ``sort`` and ``uniq``). We could easily create an "unrestricted" subset and make it available for easy download to all those who don't want to mess up with restricted data.

But what do "unrestricted" mean for EPD data? Let's take a more careful [look](http://www.europeanpollendatabase.net/wiki/doku.php?id=new) (emphasis mine):

> 3. Data will be classified as restricted or unrestricted. All data will be available in the EPD, although restricted data can be used only as provided below.
> 4. Unrestricted data are available *for all uses*, and are included in the EPD on various electronic sites.
> 5. Restricted data may be used only by permission of the data originator. Appropriate and ethical use of restricted data is the responsibility of the data user.
> 6. Restrictions on data will expire three years after they are submitted to the EPD. Just prior to the time of expiration, the data originator will be contacted by the EPD database manager with a reminder of the pending change. The originator may extend restricted status for further periods of three years by so informing the EPD each time a three-year period expires.

Sounds quite good, doesn't it? "for all uses" is reassuring and the short time limit is a good trade off. The horror comes a few paragraphs below with the following scary details:

> 9. The data are available only to non-profit-making organizations and for research.
>
> Profit-making organizations may use the data, even for legitimate uses, only with the written consent of the EPD Board, who will determine or negotiate the payment of any fee required. 

Here the false assumption that only academia is entitled to perform research is taken for granted. And there are even more [rules](http://www.europeanpollendatabase.net/wiki/doku.php?id=new#d._users) about the "normal ethics": basically if you use EPD data in a publication the original data author should be listed among the authors of the work. I always thought citation and attribution were invented just for that exact purpose, but it looks like they have distinctly different approach to *attribution*. The EPD is even deciding what are "legitimate" uses of pollen data (I can hardly think of any possible unlegitimate use).

* [European Pollen Database](http://www.europeanpollendatabase.net/)

### Africa

You write "Africa" but you read "Europe" again, because most research projects are from French and English universities. For this reason, the situation is almost the same. What is even worst is that in developing countries there are far less people or organizations that can afford buying those data, notwithstanding the fact that in regions under rapid development the study and preservation of environmental resources are of major importance.

Data are downloadable for individual sites using a [search engine](http://medias3.mediasfrance.org/apd/tools/fossile/index.html), in Tilia format (not ASCII unfortunately). The problems come out with the license:

The wording is almost exactly the same as for the EPD seen above:
> Normal ethics pertaining to co-authorship of publications applies. The contributor should be invited to be a co-author if a user makes significant use of a single contributor's site, or if a single contributor's data comprise a substantial portion of a larger data set analysed, or if a contributor makes a significant contribution to the analysis of the data or to the interpretation of the results.
> The data will be available only to non-profit-making organisations and for research. Profit-making organisations may use the data for legitimate purposes, only with the written consent of the majority of the members of the Advisory board, who will determine or negotiate the payment of any fee required. Such payment will be credited to the APD.

* [African Pollen Database at NOAA](http://www.ncdc.noaa.gov/paleo/apd.html)
* [Banque Africaine de Données Polliniques/African Pollen Database](http://medias3.mediasfrance.org/apd/)

### Conclusions

As for [dendrochronological data](http://www.iosa.it/content/open-source-dendrochronology-not-yet), there is a serious misunderstanding by universities and research centers of their role in society as places of research, innovation that is *available for everyone*. In other words, academia is a closed system producing data (at very high costs for society) that are only available inside its walls, but it's all done with public money.

### Appendix: Using pollen data

Pollen data are usually presented in forms of synthetic charts where both stratigraphic data and quantitative pollen data are easily readable. Each "column" of the chart stands for a species or *genus*. You *can* create this kind of visualization with free software tools.

The [stratigraph](http://cran.r-project.org/web/packages/stratigraph/index.html) package for [R](http://www.r-project.org/) can do

> plotting and analyzing paleontological and geological data distributed through through time in stratigraphic cores or sections. Includes some miscellaneous functions for handling other kinds of palaeontological and paleoecological data.

