---
layout: single
title: ! 'SVG Pottery: pottery drawings on the Web'
author: steko
created: 1306752016
---
A few months ago I posted here a *[vision](http://www.iosa.it/content/pottery-drawings-and-semantic-web)* to put pottery drawings on the Semantic Web. Now I'm trying to follow up on that vision, going into the details and problems that it brings to light.

I have created a [new repository at bitbucket](https://bitbucket.org/steko/svg-pottery) to give this idea more substance than a series of blog posts. For now there is nothing but a small set of pages written in reStructured Text for use with [Sphinx](http://sphinx.pocoo.org/). The plan is to add SVG samples, possibly some XLST snippets, and I know that I'm going to write some Python code.

There are some strong assumptions here:

- you want to publish your drawings on the Web
- you want to do it in a standards-compliant, sustainable way
- you are ready to *change* your current publishing workflow
- you like to experiment

These four reasons explain why SVG is the holy grail we should aim for. There is almost no prior art, so I'm adopting an incremental approach, from simple to complex:

- use SVG for new work or convert your legacy drawings to SVG
- add metadata to SVG (possibly by editing source code directly)
- deal with units and coordinate systems
- experiment with extracting data from SVG for further processing

Should you want to join me in this experiment, you are very welcome!
