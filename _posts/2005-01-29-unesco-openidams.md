---
layout: single
title: UNESCO OpenIDAMS
author: steko
created: 1107019012
---
The objective of the Free Open Source IDAMS (OpenIDAMS) project is to produce, keep up-to-date and disseminate a free open source software package for data management, exploration and archiving, with report generation facilities, by transforming IDAMS into an evolving, inter-related set of end-user and developer services. The UNESCO Secretariat is the promoter, co-ordinator and supervisor of the project.

The complete description is on the [UNESCO website](http://portal.unesco.org/ci/en/ev.php-URL_ID=15653&URL_DO=DO_TOPIC&URL_SECTION=201.html).
