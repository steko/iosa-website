---
layout: single
title: FastiOnline
author: steko
created: 1141029465
---
The site is a web-based GIS database that will contain a basic record sheet for each site excavated by <acronym title="Associazione Internazionale di Archeologia Classica">AIAC</acronym> for any period in a given year with, at the discretion of the site director, the interim report for the year, together with any further documentation the director wishes to add. There is no chronological limitation to the sites listed, which range from the Upper Palaeolithic to the nineteenth century, and reports on restoration projects and new museum displays are also welcomed. The site is multilingual, with editions currently available English, Italian, Bulgarian, Macedonian and Romanian, and Arabic and French versions in progress. Sites are searchable by project name, by period and type of site, and by clickable maps which provide an overview of the excavation locations.
This webgis application is based on <a href="/node/11">UMN Mapserver</a>.
