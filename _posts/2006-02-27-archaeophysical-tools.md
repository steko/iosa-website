---
layout: single
title: Archaeophysical tools
author: steko
created: 1141058484
---
On this site, the authors, a team of one archaeologist, two physicists, one mathematician and one computer scientist, hope to start a meeting point for software tools, scripts and short programs developed to help anthropological sciences by mathematical means. All software is published under the GNU GPL.
