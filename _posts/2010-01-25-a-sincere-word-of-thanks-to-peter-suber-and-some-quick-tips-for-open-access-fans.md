---
layout: single
title: A sincere word of thanks to Peter Suber, and some quick tips for open access
  fans
author: steko
created: 1264428035
---
I can't remember exactly when I started following <a href="http://www.earlham.edu/~peters/fos/fosblog.html">Open Access News</a>, the most important source of news for everything open access, from literature to public sector information. I can say for sure that it was long before OAN became a <em>blog</em> (that is, one with feeds and more-than-weekly updates). I remember IOSA.it being covered on OAN as a 
Good things evolve over time and this one makes no exception: Open Access News, starting from last week, <a href="http://www.earlham.edu/~peters/fos/2010/01/housekeeping-future-of-oan.html">has been “superseded”</a> by the already running <a href="http://oad.simmons.edu/oadwiki/OA_tracking_project">Open Access Tracking Project</a>. This means basically that most “retweets” like links to relevant breaking news from academia or governments will take their way in <a href="http://www.connotea.org/">Connotea</a>. OAN will continue to exist, but with a low volume &mdash; I'm sure this will mean in-depth discussion of major issues or advancements. In Suber's words:
<blockquote><p>OATP is more comprehensive than a large blog because it is crowdsourced and distributes the labor to all who want to take part.  It's leaner than a large blog because most of its news alerts are just citations, links, and brief descriptions. </p></blockquote>
The mere fact that one single person can't follow alone the entire flow of news about Open Access is great, by the way.
So, to make a long story short:
<ol>
<li>thanks, thanks a lot to Peter Suber and <a href="http://www.earlham.edu/~peters/fos/fosblog.html">Gavin Baker</a> for their restless work during these past years</li>
<li>go to http://www.connotea.org/tag/oa.new and subscribe to the newsfeed (incidentally, please note that most of the content there is submitted daily by Peter Suber himself)</li>
<li>(optional, but recommended) get yourself an account on Connotea and start tagging relevant open access news</li>
<li>don't forget to check out also http://www.connotea.org/tag/oa.archaeology and maybe share news about open access at your lab, department, university, company or office</li>
<li>finally, when you see that the oa.* prefix can be used for anything, look for your country, field of study/work, etc</li>
</ol>
<!--break-->
