---
layout: single
title: CAA 2010 Full Papers deadline extended
author: steko
created: 1260806823
---
From the [CAA 2010 website](http://www.caa2010.org/index.php/component/content/article/7-scientific-programme-news/65-full-papers-deadline-extended):

> After our recent e-mail that reminded the deadline for extended abtracts,we have received a huge amount of e-mails requiring an extension of the deadline, due to the new format of submission (up to four pages instead of a short abstract) and all the burocratic issues in which researchers are involved during december.
> The CAA2010 organizing commitee is aware of such difficulties, and in order to facilitate the participation of researchers and students via full papers presentations instead of just short papers (that cannot be selected for the journals that have offered to publish best full papers), we have agreed, as a very exceptional decission and assuming that it will cause us an enormous overload in the reviewing process and books formatting to: EXTENDED THE FULL PAPERS ABSTRACTS DEADLINE UNTIL JANUARY 31st

Enjoy this extended deadline to send your abstracts. I'm not actually sure to go, but I'm thinking about proposing a round table / workshop.
