---
layout: single
title: SIMILE Project
author: steko
created: 1155422528
---

SIMILE is a joint project conducted by the <a href="http://www.w3.org/" title="Web Consortium">W3C</a>, <a href="http://libraries.mit.edu/" title="MIT Libraries">MIT Libraries</a>, and <a href="http://csail.mit.edu" title="MIT Computer Science and Artificial Intelligence Laboratory">MIT CSAIL</a>. SIMILE seeks to enhance inter-operability among digital assets, schemata/vocabularies/ontologies, metadata, and services. A key challenge is that the collections which must inter-operate are often distributed across individual, community, and institutional stores. We seek to be able to provide end-user services by drawing upon the assets, schemata/vocabularies/ontologies, and metadata held in such stores.

SIMILE will leverage and extend <a href="http://www.dspace.org" title="DSpace">DSpace</a>, enhancing its support for arbitrary schemata and metadata, primarily though the application of RDF and <a href="http://www.w3.org/2001/sw/" title="W3C Semantic Web Activity">semantic web</a> techniques. The project also aims to implement a digital asset dissemination architecture based upon web standards. The dissemination architecture will provide a mechanism to add useful &quot;views&quot; to a particular digital artifact (i.e. asset, schema, or metadata instance), and bind those views to consuming services.

To guide the SIMILE effort developers will focus on well-defined, real-world use cases in the libraries domain. Since parallel work is underway to deploy DSpace at a number of leading research libraries, with the hope that such an approach will lead to a powerful deployment channel through which the utility and readiness of semantic web tools and techniques can be compellingly demonstrated in a visible and global community.

The SIMILE Project and its members are fully committed to the open source principles of software distribution and open development and for this reason, it releases the created intellectual property (both software and reports) under a BSD-style license.  The SIMILE Project team members gladly welcome community efforts and would particularly like to recognize SIMILE&#39;s contributors.
