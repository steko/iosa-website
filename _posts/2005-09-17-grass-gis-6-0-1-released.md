---
layout: single
title: GRASS GIS 6.0.1 released
author: giofish
created: 1126957362
---
A bugfix release of GRASS GIS has been published. For more information: <a href="http://grass.itc.it/announces/announce_grass601.html" target="_blank">http://grass.itc.it/announces/announce_grass601.html</a>.
