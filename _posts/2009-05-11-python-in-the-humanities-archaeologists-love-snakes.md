---
layout: single
title: Python in the Humanities? Archaeologists love snakes!
author: steko
created: 1242036299
---

[Titus Brown](http://ivory.idyll.org/) is looking for examples of <a href="http://ivory.idyll.org/blog/may-09/lazyweb-python-in-hum">how Python is used in the Humanities</a>. Archaeologists love snakes!
I have pointed out our experience with Python programming, which we also presented on April, 27th in Rome, but you can submit more examples, as I'm sure there are dozens of them, particularly in the field of database management.
