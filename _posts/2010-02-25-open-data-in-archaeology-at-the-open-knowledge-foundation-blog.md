---
layout: single
title: Open Data in Archaeology at the Open Knowledge Foundation blog
author: steko
created: 1267095941
---
Today the <a href="http://blog.okfn.org">Open Knowledge Foundation blog</a> features an article written by myself introducing <a href="http://blog.okfn.org/2010/02/25/open-data-in-archaeology/">“Open data in archaeology”</a>, the working group started a the OKF last month and some arguments that highlight why open data is good for building archaeological knowledge in the twenty-first century.
Please find 15 minutes to read it and “retweet” to your colleagues, students, teachers. You don't need to be an hacker or a programmer to understand and contribute, it's all about your daily work.
