---
layout: single
title: Compiling Stereo Photogrammetry Software
author: steko
created: 1172505824
---
<p><a href="http://grass.itc.it/gdp/stereo-grass/index.html">Stereo</a> is an old free photogrammetry software, that was developed in 1997 by <a href="http://linux.2038bug.com/">Paul Sheer</a> during his PhD. At the moment it's one of the few free software programs that do photogrammetry (correct me if I'm wrong), that was due to be integrated into <a href="http://grass.itc.it">GRASS</a> but is now stuck.</p>
<p>Yesterday I found out that errors I had compiling Stereo from source code were due to using GCC 4.1 (too recent for 10 years old code). Compiling it with GCC 3.4 gave just some warning but now I have it “working”. Of course there's no magic in it so the interface is still very very buggy and unstable.</p>
