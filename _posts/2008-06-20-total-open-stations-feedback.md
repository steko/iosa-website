---
layout: single
title: Total Open Station's feedback
author: luca
created: 1213956703
---
Iosa's team is developing (already quoted some posts ago) the software called Total Open Station, for surveying and data recording from total stations. In the May 2008's Italian edition of the IT-magazine "PCProfessionale" you can find a VERY little box speaking about TOPS! It says that this application should be useful not just for archaeologists but even for other people using total stations as any kind of people involved in surveying. It's at the same time a satisfaction for the developers and a good feedback for going on with the development and the conception of new parts of the software. Thanks "PCProfessionale".
