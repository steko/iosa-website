---
layout: single
title: Mark Lake home page
author: steko
created: 1131215641
---
Mark Lake is a lecturer at Institute of Archaeology, University College London. His interests are mainly in the field of quantitative methods, mostly focused on <acronym title="Geographical Information Systems">GIS</acronym> and computer simulation.
He has written some GRASS GIS modules related to spatial analysys and pattern recognising in early human settlements. These modules are open source and can be freely downloaded from his website.
On the side of simulation, he developed the MAGICAL software, made of three GRASS modules that deal with the study of resources exploitation on a spatial basis.
Please note that these modules require old versions of the GRASS GIS and might not work with the latest stable version.
