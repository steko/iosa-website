---
layout: single
title: ! ' PostGIS 1.0 Support for QGis 0.6'
author: steko
created: 1109365880
---
The PostGIS 1.0 release candidates do not work with QGIS 0.6. A patch is available for those that want to use PostGIS 1.0 and build QGIS from source.
You can find it on the <a href="http://community.qgis.org/"  title="QGis Community Website" onclick="window.open(this.href); return false;">QGis Community Website</a>.
