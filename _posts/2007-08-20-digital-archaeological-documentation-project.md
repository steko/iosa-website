---
layout: single
title: Digital Archaeological Documentation Project
author: steko
created: 1187633970
---
<p>The main purpose of the <strong>Digital Archaeological Documentation Project</strong> is is to create a number of tutorials about the use of Free Software in archaeology, mainly focusing on field and excavation best practices. Each tutorial is based on the real experience of the “Aramus Excavations and Field School”, that takes place every year at the Aramus archaeological site in Armenia and is carried on by the University of Innsbruck, the University of Salzburg, the University of Yerevan and Arc-Team.</p>
<p>The DAD Project uses a wiki as its e-learning platform.</p>
