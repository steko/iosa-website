---
layout: single
title: DBDesigner
author: giofish
created: 1115216025
---
DBDesigner 4 is a visual database design system that integrates database design, modeling, creation and maintenance into a single, seamless environment.
It combines professional features and a clear and simple user interface to offer the most efficient way to handle your databases.
DBDesigner 4 compares to products like Oracle's Designer©, IBM's Rational Rose©, Computer Associates's ERwin© and theKompany's DataArchitect© but is an Open Source Project available for Microsoft Windows© 2k/XP and Linux KDE/Gnome. It is release on the GPL.
DBDesigner 4 is developed and optimized for the open source MySQL-Database to support MySQL users with a powerful and free available design tool.
All MySQL specific features have been built in to offer the most convenient way to design and keep control of your MySQL-Databases.
For more information you can see at URI: <a href="http://www.fabforce.net/dbdesigner4/" onclick="window.open(this.href); return false;">http://www.fabforce.net/dbdesigner4/</a>.
