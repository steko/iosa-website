---
layout: single
title: happydigger 2.0 released (Default branch)
author: steko
created: 1124921646
---
Version 2.0 of Happydigger has been released. All URLs and other useful information can be found at http://www.xs4all.nl/~pa4tu/happydigger/happydigger.html

The changes in this release are as follows:
Happydigger has migrated to version 3 of SQLite, which provides much smaller databases. Additional tables can now be created for storing information. Image display has been improved.  

Happydigger is a program which can be used for cataloging archaeological finds. It is intended both for semi-professional use and by amateurs (e.g. metal detector users) who want to keep track of their finds. Data is stored in a database with extensive find and findspot details. If images are available, they will be displayed together with the find information. 

