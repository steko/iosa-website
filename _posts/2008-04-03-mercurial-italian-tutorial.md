---
layout: single
title: Mercurial Italian Tutorial
author: luca
created: 1207178902
---
The Iosa's team is proud to inform that we have just completed the Italian Tutorial of one of the most common and useful software we use in the development of our tools: Mercurial. It is a Distributed CSM, it allow us to contribute to the implementation of our softwares in parallel wherever we are. Because of it's efficacy, We have decided to translate the tutorial of one of our favourite program to help our Italian colleagues to enjoy it during the software development's processes.
You can find out Tutorial in Mercurial's Tutorial with other languages' ones :  http://www.selenic.com/mercurial/wiki/index.cgi/Tutorial

We hope to put on the Iosa's site any software we will develop, we hope soon.
