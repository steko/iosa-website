---
layout: single
title: Archäoinformatik - Benjamin Ducke
author: steko
created: 1130133236
---
Info page of young German researcher Benjamin Ducke at University of Kiel (in german, sorry). Here you can find some useful extensions for the GRASS, among these the <strong>GRASS Extension Manager</strong> and an implementation of Dempfer Shafer Theory, aimed at archaeological studies.
