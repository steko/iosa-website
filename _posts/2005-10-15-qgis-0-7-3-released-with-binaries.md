---
layout: single
title: QGis 0.7.3 released with binaries
author: steko
created: 1129390204
---
The last release of QuantumGIS is 0.7.3, which features just bugfixes and enhancements of previous 0.7.0, but now there are also binaries for Linux (Debian and Ubuntu), Windows, MacOS X, plus source tarballs that compile on FreeBSD and OpenBSD. Go get a copy [here](http://qgis.org/index.php?option=com_content&task=view&id=65&Itemid=71 "QGIS download page"), it's more friendly than ever with GRASS toolbox that makes really easy the access to many of the GRASS functions, Postgres/PostGIS integration, support for on-the-fly projection and the raster georeferencer plugin.
