---
layout: single
title: white_dune
author: steko
created: 1122495183
---
white_dune is a graphical VRML97 editor, simple NURBS/Superformula 3D modeller and animation tool. VRML97 (Virtual Reality Modeling Language) is the ISO standard for displaying 3D data over the Web via browser plugins ("HTML for realtime 3D"). It has support for animation, real-time interaction, and multimedia (images, movies, and sounds). white_dune can read, create and display VRML97 files and let the user change the scenegraph/fields. It also has support for stereoscopic view via "quadbuffer"-capable stereo visuals, and support for 3D input devices like a joystick, spaceball, or magnetic tracker.
