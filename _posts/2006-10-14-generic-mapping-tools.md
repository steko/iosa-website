---
layout: single
title: Generic Mapping Tools
author: steko
created: 1160843285
---

GMT is an open source collection of ~60 tools for manipulating geographic and Cartesian data sets (including filtering, trend fitting, gridding, projecting, etc.) and producing Encapsulated PostScript File (EPS) illustrations ranging from simple x-y plots via contour maps to artificially illuminated surfaces and 3-D perspective views. GMT supports ~30 map projections and transformations and comes with support data such as coastlines, rivers, and political boundaries. <strong>GMT</strong> is developed and maintained by <a href="http://www.soest.hawaii.edu/pwessel/pwessel.html" target="_blank">Paul Wessel</a> and <a href="http://ibis.grdl.noaa.gov/SAT/people/walter.html" target="_blank">Walter H. F. Smith</a> with help from <a href="http://gmt.soest.hawaii.edu/gmt/gmt_team.html" target="_blank">a global set of volunteers</a>, and is supported by the <a href="http://www.nsf.gov/" target="_blank">National Science Foundation</a>. It is released under the <a href="http://www.gnu.org/copyleft/gpl.html" target="_blank">GNU General Public License</a>. Current version is 4.1.3, released June 1, 2006.
