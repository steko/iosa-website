---
layout: single
title: Science Commons
author: steko
created: 1125742846
---
Science depends upon the ability to observe, learn from, and test the work of others. Without effective access to data, materials and publications, the scientific enterprise becomes impossible.
The goal of Science Commons is to encourage stakeholders to create areas of free access and inquiry using standardized licenses and other means: a 'Science Commons' built out of voluntary private agreements.
