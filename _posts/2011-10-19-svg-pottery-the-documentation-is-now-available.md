---
layout: single
title: ! 'SVG Pottery: the documentation is now available'
author: steko
created: 1319019054
---
Thanks to [readthedocs.org](http://readthedocs.org/), the SVG Pottery documentation project is now available directly at <http://svg-pottery.readthedocs.org/en/latest/>, and it is always updated to the latest version.

We are looking for examples of SVG usage for pottery and other archaeological finds in digital publications. You have got your own drawings and would like to publish them? Let us know by commenting here!
