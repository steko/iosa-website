---
layout: single
title: Open Database Licence
author: steko
created: 1190820593
---
<p>From <a href="http://www.opencontentlawyer.com/">http://www.opencontentlawyer.com/</a>:</p>

<blockquote><p>This licence covers copyright and database rights over databases. It doesn’t cover the rights over the contents of the database — this is so that the licence can be useful for databases that have contents with different sets of rights. More on this aspect can be found in this post, “<a href="http://www.opencontentlawyer.com/2007/09/10/thoughts-on-drafting-an-open-data-licence/">Thoughts on drafting an open data licence</a>“.</p>
<p>The posting of the licence is the first step in what will hopefully be an ongoing conversation about the development of the licence. Many more posts will follow this one, as well as conversations on a variety of websites and blogs.</p>

<p><a href="http://www.opencontentlawyer.com/open-data/">Open Data Home</a> — This is the homepage for links and resources for the licence.</p>
<p><a href="http://www.opencontentlawyer.com/open-data/open-database-licence/">Open Database Licence</a> — The draft version of the licence itself.  This is the core licence applied for databases.</p>
<p><a href="http://www.opencontentlawyer.com/open-data/open-data-commons-factual-info-licence/">Open Data Factual info licence</a> — This is a supplemental licence for those that deal with factual information, or  for copyrighted works that the Licensors want to allow as broad a use as possible. This licence is included because of the separation of ‘Data’ and ‘Database’ in the main licence.</p>
<p><a href="http://www.opencontentlawyer.com/category/open-data/">Open Data blog posts</a> — Does what it says on the tin — all the blog posts on this site in the category ‘Open Data’.</p>
<p>There is a discussion list for the licence, which you can sign up for here:</p>

<p><a href="http://lists.opencontentlawyer.com/listinfo.cgi/tcl-discuss-opencontentlawyer.com">http://lists.opencontentlawyer.com/listinfo.cgi/tcl-discuss-opencontentlawyer.com</a></p>
</blockquote>
