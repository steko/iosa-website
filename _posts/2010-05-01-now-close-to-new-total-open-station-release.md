---
layout: single
title: Now close to new Total Open Station release
author: luca
created: 1272717339
---
We're very pleased to announce that the new release of Total Open Station is almost ready. Within a few days we're going to release the 0.2 version of the application.

It will be available for windows, linux and (a little later) apple systems. 

There will be a windows exe, already available and packaged.

It has been quite interesting  packaging it.
We have used pyInstaller ( http://www.pyinstaller.org/ ) .

When the cmd is open ( on a win sys ofcourse ) and we've reached pyinstaller folder, the comands we've typed are just: 


python Makespec.py --tk --noconsole --icon="..\tops.ico" ..\totalopenstation\scripts\totalopenstation-gui.py

python Build.py totalopenstation-gui\totalopenstation-gui.spec

Cool stuff pyinstaller: easy and working!
