---
layout: single
title: GRASS WIKI
author: steko
created: 1160927882
---
**GRASS** is a Geographic Information System (GIS) used for geospatial data management and analysis, image processing, graphics/maps production, spatial modeling, and visualization. GRASS is currently used in academic and commercial settings around the world, as well as by many governmental agencies and environmental consulting companies. GRASS WIKI offers the official community platform of the GRASS GIS project. On GRASS WIKI, you can get and contribute to GRASS related information, documents and add-ons programs.
