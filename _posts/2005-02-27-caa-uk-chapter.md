---
layout: single
title: CAA UK Chapter
author: steko
created: 1109514233
---
The CAA UK chapter is intended as a forum for national research in the area of archaeological computing and quantitative methods. The chapter meeting aims to facilitate exchange of ideas between researchers and cultural resource managers, and is particularly focussed on the presentation of new and innovative research areas. The meeting organisers are also particularly keen to encourage new researchers to present their work for the first time.
