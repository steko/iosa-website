---
layout: single
title: Archaeological geophysics with free open source software - tpq
author: steko
created: 1278310163
---
As many readers will know, since 2008 we've been working to enable archaeologists and their colleagues with a chance to do professional surveying on free operating systems. What sounded like a silly idea is now reality in the [Total Open Station](http://tops.berlios.de/) project.

Total stations do a nice job, but that's you do something more when you're interested in what's *beneath* the soil rather than on top of it. Enter geophysics, with another load of costly hardware tools that are locked in proprietary combinations of software and drivers.

In case you're using GNU/Linux or another free operating system, you might find quite difficult to interact with your geophysical device, without any dedicated software. As you might expect, you're not the first to encounter this problem (you're *never* going to be the first) and someone else already started working on a solution.

[Tractatus Post Quem](http://code.google.com/p/tpq/) (tpq) by John Donovan is a

> collection of tools useful to the archaeological geophysicist and surveyor, focussing on the conversion of data from proprietary formats, georeferencing them, and tagging with metadata.
<!--break-->
Wow, seems very promising. You find two different programs in the repository:

  * `rm15dump` downloads data from Geoscan Research RM15-D resitivity meters
  * `tpq` proper is a package of Python tools for processing archaeological geophysical data, integrated with [GDAL](http://gdal.osgeo.org/)

### `rm15dump`

`rm15dump` is a command-line UNIX program written in C++. It downloads data from [Geoscan Research](http://www.geoscan-research.co.uk/) RM15-D resitivity meters, and outputs it into a variety of useful formats. According to the author, it has only been tested on non-multiplexed RM15-D units, but other hardware (i.e. RM4 with DL10 and RM15) should also work.

It is quite easy to build it from source, with the provided makefile.

### `tpq`

`tpq` is instead a Python package with dedicated modules like

  * `destripe`: removes stripes from tiles of geophysical data, AKA Zero mean traverse
  * `from_xyz`: converts from a text xyz file to a TPQ image
  * `highpass`: a highpass filter
  * `to_geotiff`: converts an image to GeoTIFF

All these tools implement standard processing techniques in a very low-level fashion. Your mileage may vary: if you're used to point-and-click operation and you don't actually know what you're doing when processing geophysical data, these tools won't work. On the other hand, if you're willing to operate *and* understand the analytical process, if you like to hack your tools, or if you just like software freedom, this should be your way to go.

I like a lot the choice of Python as a programming language for archaeological purposes. In this case, [NumPy](http://numpy.scipy.org/) proves again to be a powerful numerical programming environment, allowing for high performance and clear code.

Integration with GDAL is very convenient, because it allows for a seamless workflow to GIS processing. I'll be doing some tests in the next months with my department colleagues, if you do the same please publish your results and let the author know.
