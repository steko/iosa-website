---
layout: single
title: Blender 2.37 released
author: steko
created: 1117631073
---
Just released; a version with over 5 months of development on new features and fixes. The most noteworthy additions are the Soft Body system, the new incremental Subdivision Surface code, and the full recode of the internal Transformation system, allowing much more control and flexibility. 
The complete changelog is on the <a href="http://www.blender3d.org/cms/Blender_2_37.496.0.html" title="Blender 2.37 changelog" onclick="window.open(this.href); return false;">Blender site</a>.
