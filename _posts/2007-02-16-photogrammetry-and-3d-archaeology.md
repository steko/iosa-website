---
layout: single
title: Photogrammetry and 3D Archaeology
author: steko
created: 1171639623
---
<p>Yesterday we had a lecture by <a href="http://www.photogrammetry.ethz.ch/" title="Chair of Photogrammetry and Remote Sensing">Fabio Remondino from ETHZ</a> on Photogrammetry and 3D in Archaeology. It has been very interesting, despite the hot temperature inside the room.</p>

<p>Photogrammetry has been much developed in recent years and now automatic methods are available that result in very fast processing of image data and generation of high-detail 3D textured models. The great advantage of photogrammetric methods versus 3D laser scanning and active sensor techniques seems in first place their intrinsic low cost, both in terms of time and money. A common 300 € camera is already capable of producing good images to be processed and become a photo-realistic 3D model with sub millimetric precision. Why aren't we using it on a daily basis to collect documentation?</p>

<p>There are 2 main issues with photogrammetry right now:</p>


1. No one has the skills to use it. Using a technology means to have its knowledge. Time and money need to be put in learning **yet another one** technical tool for archaeology.
2. Commercial software isn't that good for complex scenes to be portraited and processed, though it can be somewhat expensive. Dr. Remondino told us that the software he's developing is far more better than commercial ones, but it isn't going to be released any time soon. That's why at the moment if you would like to have your images automatically processed by his software, you shouldjust send him your images. Not that convenient, indeed. Software is still buggy and incomplete. _Why isn't he releasing the code then?_

<p>This way, I don't think photogrammetry is going to gain success in the archaeological field.</p>
