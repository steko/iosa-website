---
layout: single
title: Aqsis
author: steko
created: 1124656948
---
Aqsis is a high quality, photorealistic, 3D rendering solution. It complies with the Renderman® interface standard defined by Pixar.

Aqsis comprises a command line rendering tool, a tool for compiling shaders in the RSL language, a tool for preparing textures for optimal use, and various developer libraries to enable integration with third party tools.

Aqsis is licensed under the GPL license, with some parts under the LGPL.
