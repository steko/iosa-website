---
layout: single
title: ! 'GIS and Remote Sensing for Archaeology: Burgundy, France'
author: steko
created: 1133450538
---
For over twenty years research has been conducted in the applications of remote sensing and GIS in the Burgundy region of France. This long term research project: "Applications of Geomatics for Long Term Regional Archaeological Settlement Pattern Analysis" is the work of Dr. Scott Madry of Informatics International, Inc. and the University of North Carolina at Chapel Hill Department of Anthropology (formerly of the Center for Remote Sensing and Spatial Analysis of Rutgers University and the International Space University of Strasbourg, France).
The GRASS GIS software application is being used in this research project.
