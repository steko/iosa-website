---
layout: single
title: Blender Tutorials
author: steko
created: 1123330506
---
This is the official Blender Tutorial archive. Noteworthy are the Video Tutorials (in english) that will guide you through your learning of this powerful open source 3D software. The Blender community is large and great, and you can also find lots of good unofficial tutorials, as on http://www.elysiun.com/ 
