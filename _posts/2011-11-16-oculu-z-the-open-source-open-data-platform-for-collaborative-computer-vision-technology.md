---
layout: single
title: oculu-Z, the open source, open data platform for collaborative Computer Vision
  technology
author: steko
created: 1321427071
---
[Oculu-Z](http://oculu-z.sourceforge.net/) is a new effort to gather a community of developers and users (is there still a difference between the two?) around open source techniques of 3D reconstruction from digital images. Community means YOU and me, and the Oculu-Z team is lead by Benjamin Ducke, one of the most prominent experts in archaeological computing.

This looks like a very good opportunity for experimenting with your own images (either dig or finds, it doesn't matter) and develop shared best practices.
