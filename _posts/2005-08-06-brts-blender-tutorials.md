---
layout: single
title: B@rt's Blender Tutorials
author: steko
created: 1123331107
---
These tutorials deal with some of the most important functions in Blender, like mesh editing, skinning tools, animated textures, object extrusion, procedural objects, NURBS editing and more. Tutorials are written in a plain english, are easy to learn, they're the basics of 3D modelling with Blender.
