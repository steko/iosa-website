---
layout: single
title: ! 'First Python 3 program: extracting archaeological data'
author: steko
created: 1260803672
---
A few days ago I've started my first <a href="http://www.python.org/download/releases/3.1.1/">Python 3</a> program. It's written from scratch using modules from Python's standard library. I'm using the <a href="http://packages.debian.org/experimental/python3">Debian package from experimental</a>, which works fine.

The program is meant to extract meaningful data about Samian Pottery in Britain from a dataset available at <acronym title="Archaeological Data Service">ADS</acronym>, published by Steven Willis. I discovered this dataset some months ago through an <a href="http://www.etana.org/abzu/abzu-displayarticle.pl?RC=21675">ABZU entry</a> that pointed to the <a href="http://ads.ahds.ac.uk/catalogue/projArch/samian_willis_2005/">ADS page</a> (you have to agree to the site's use conditions before you can access the page).

Given my <a href="http://www.iosa.it/content/what-have-i-done-past-4-months">past</a> and <a href="http://www.iosa.it/content/restart-iosait-and-phd-blogging">current work</a> about pottery distribution studies, I'm interested in playing with data very alike those I produced, albeit from a different period and region.

I plan to report about my progress in analyzing Willis' data both from a digital and archaeological point of view in the next few weeks. I currently have no access to <a href="http://intarch.ac.uk/journal/issue17/willis_index.html">Willis' paper</a> so this makes my efforts <em>blind</em> in the sense that I don't know which kind of analysis he performed and which conclusion he came to eventually. However, this sounds to me like an interesting experiment about the nature of archaeological data and the assumptions we make about them.
