---
layout: single
title: Technical guidelines for (archaeological) data - DAI
author: steko
created: 1262730964
---
A few months ago, we found out through <a href="http://groups.google.com/group/antiquist/browse_thread/thread/9acfb82687a9c6c7" title="Fwd: [DIGITALCLASSICIST] German Archaeological Institute IT guidelines - Antiquist | Google Groups">Antiquist</a> that DAI (Deutsches Archäologisches Institut, i.e. the German Archaeological Institute) has published a set of IT Guidelines for all its research projects. Even though it's primarily intended for a German audience (German-speaking too, obviously) it is a very interesting piece of standards. You can get it <a href="http://www.dainst.org/index_892d6521bb1f14a113030017f0000011_de.html">from the DAI website</a> in PDF. With some help from tools like <a href="http://translate.google.com/#de|en|http%3A%2F%2Fwww.dainst.org%2Fmedien%2Fde%2FIT-Leitfaden_Teil1_Vorgaben_v1_0_3.pdf">Google Translate</a>, one can even get an idea of what these guidelines say also without being a native speaker. We did, and here is a partial summary of the document.
The DAI IT guidelines refer to other similar standards, among them the <a href="http://www.minervaeurope.org/interoperability/technicalguidelines.htm">MINERVA EC Technical Guidelines</a>, which are going to be addressed in another post.
The main technical areas addressed in the DAI guidelines are those one would expect. For most data types, the base concept is that the most well-known standards should be used, even though there are some differences between the various fields (most notably DWG is an ugly proprietary format &mdash; even though recently <a href="http://www.gnu.org/software/libredwg/" title="LibreDWG - GNU Project - Free Software Foundation">some efforts</a> have been done to support it with free/libre software).

<table>
  <tr>
    <th>&nbsp;</th>
    <th>DAI guidelines</th>
  </tr>
  <tr>
    <th>text data</th>
    <td>UTF-8 or UTF-16, XML (including ODT), PDF/A</td>
  </tr>
  <tr>
    <th>raster graphics</th>
    <td>TIFF, DNG (JPEG to be avoided)</td>
  </tr>
  <tr>
    <th>vector graphics</th>
    <td>DWG, DXF (<em>de facto</em> standard)</td>
  </tr>
  <tr>
    <th>3D data</th>
    <td>U3D, VRML 2.0</td>
  </tr>
  <tr>
    <th>databases</th>
    <td>(L)AMP (desktop databases to be avoided)</td>
  </tr>
  <tr>
    <th>GIS</th>
    <td>GeoTIFF, ESRI Shapefiles</td>
  </tr>
  <tr>
    <th>audio and video</th>
    <td>WAV, AIFF, MPEG-1, MPEG-2</td>
  </tr>
</table>

One could say these recommendations aim to a lowest common point of interoperability and preservation, and it is probably so for a large part (but, see recent <a href="http://spatialgalaxy.net/2010/01/04/the-shapefile-reports-of-my-deprecation-have-been-greatly-exaggerated/" title=" The Shapefile – Reports of My Deprecation have been Greatly Exaggerated ">notes about Shapefile by QGIS's Gary Sherman</a> who doesn't accept the definition of Shapefile as a <em>dead format</em>). However the DAI guidelines look good, not just in the details but also in the general requirements for procedures and prior planning and the clear remark that the adoption and use of open source software has to be the necessary aim of any project design.

There is no mention in this document of using software formats like those used by Microsoft Word or Microsoft Access, and there's a good reason behind this of course. If you ever tried opening a <code>.docx</code> document with Microsoft Word 2003 you should already know why.

As stated by Olla Mulholland:
<blockquote><p><em>as the DAI is one of the biggest players in classical archaeological research, the guidelines may well come to have an influence beyond the institute's own projects.</em></p></blockquote>
We hope so.
