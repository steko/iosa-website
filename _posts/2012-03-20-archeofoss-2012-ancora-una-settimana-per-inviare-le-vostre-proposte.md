---
layout: single
title: ! 'ArcheoFOSS 2012: ancora una settimana per inviare le vostre proposte'
author: steko
created: 1332235467
---
C'è ancora una settimana per presentare relazioni e poster al workshop ArcheoFOSS 2012. La scadenza è lunedì 26 marzo 2012. La call for papers si trova sul sito web [archeofoss.org](http://www.archeofoss.org/) insieme alle informazioni sull'evento e sull'invio delle vostre proposte.

Quest’anno puntiamo ad avere una ampia partecipazione: questo significa che da un lato ci aspettiamo da parte di tutti gli interessati una collaborazione alla diffusione di questa call e l’invio di proposte, ma dall’altro lato dovremo necessariamente selezionare le proposte più interessanti per mantenere un livello qualitativo soddisfacente.
