---
layout: single
title: PostGIS 1.0.0 Released!!!
author: steko
created: 1114026516
---
After a long series of release candidates, the much anticipated 1.0.0 version of PostGIS has been released. The 1.0.0 version includes substantial changes over the original 0.X series, most importantly a new on-disk format which is both more compact and faster to access. 
Read more on <a href="http://postgis.refractions.net" onclick="window.open(this.href); return false;">postgis.refractions.net</a>
