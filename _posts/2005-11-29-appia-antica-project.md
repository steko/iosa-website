---
layout: single
title: Appia Antica Project
author: steko
created: 1133255214
---
The project <q>Archaeological Park of ancient via Appia. From the field to Virtual Reality applications</q> regards activities, technologies and digital solutions connected with an interdisciplinary approach, carried out by CNR ITABC and the Archaeological Superintendency of Rome.<!--break-->
The goal of the project is to realise a digital archive of the monuments of the park, employing many different technologies for 3D representation of the landscape and integrating instruments for topographic relief and methodologies of surveying on site (DGPS, laser total station, photogrammetry, 3D laser scanning, photo and video acquisition), according to the level of detail required. All the data are successively elaborated to obtain a correct geometric model of the landscape, implemented in a Real time OpenGL application where the user can interact with many hierarchical levels of contents.
Since 2003 started the experimentation of an open-source approach even to territorial data. Thank's to the cooperation with CINECA supercomputing centre of Bologna a first dissertation was assigned, with the goal of testing the possibility to publish large terrain datasets on the Web. The result conducted the team to use a powerful open-source software called OpenSceneGraph .
The success of the session "Open Source and Archaeology" organised in occasion of the international conference CAA2004, held in Prato (Italy) persuaded that that could be a promising opportunity for Cultural Heritage, for several reasons: for the low economical impact of the software solution, for the great possibility of personalisation of the tools and for the cooperative strength towards that this kind of approach leads. In 2004 has started a new phase of testing with a tool based on OpenSceneGraph: Virtual Terrain Project .
The results are surprising and the goal is the creation of an interactive and real-time landscape designer.
