---
layout: single
title: A Short Preview On Modeling With K-3D
author: steko
created: 1124629831
---
In <a href="http://www.gnomejournal.org/article/24/a-short-preview-on-modeling-with-k-3d" title="GnomeJournal.org - A short preview on K-3D" onclick="window.open(this.href); return false;">this article</a> from the latest issue of <a href="http://www.gnomejournal.org/article/24/a-short-preview-on-modeling-with-k-3d" title="GnomeJournal.org home page" onclick="window.open(this.href); return false;">GnomeJournal</a>, Claus Schwarm takes a look at the history and the current development version of K-3D, a tool for 3D modeling, rendering, and animation.
