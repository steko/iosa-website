---
layout: single
title: GIS Knoppix
author: steko
created: 1114330706
---
GIS-Knoppix is a bootable Linux CD with pre-installed GIS software.
It is based on Knoppix. It can be very useful for classes without a working Linux installation, or just for demo purposes.

<!--break-->
It features:
<dl>
<dt>Linux with KDE and many applications</dt>
<dt>GRASS 6.0.0beta with Spearfish dataset</dt>
<dd>Raster/vector GIS, image processing system, and graphics production system</dd>
<dt>UMN MapServer 4.4.1</dt>
<dd>Environment for building spatially enabled Internet applications</dd>
<dt>PostgreSQL 7.4.6 / PostGIS</dt>
<dd>Postgres database with PostGIS extension</dd>
<dt>MySQL 4.1.9</dt>
<dd>MySQL database with spatial extensions</dd>
<dt>QGis 0.6</dt>
<dd>Viewer and data analysis for vector and raster data with PostGIS support</dd>
<dt>TerraView 2.0</dt>
<dd>Viewer and data analysis for vector with DB support</dd>
<dt>Jump 1.1.2</dt>
<dd>Workbench for viewing, editing, and processing spatial datasets</dd>
<dt>Thuban 1.0.0</dt>
<dd>Interactive geographic data viewer</dd>
<dt>MapLab 2.0.1</dt>
<dd>Suite of web-based tools to create and manage MapServer applications and map files</dd>
<dt>MapDesk 0.7</dt>
<dd>Viewer/editor for UMN Mapserver</dd>
<dt>Interlis</dt>
<dd>Compiler and UML editor for Interlis formats</dd>
<dt>and other software</dt>
<dd>GPSdrive, GPSMan, ...</dd>
</dl>

Current version is 1.2, which you can buy trough an e-store. Version 1.0 has less applications and they aren't the latest releases (GRASS 5.4, etc), but can be downloaded as an ISO image. Please note this is free/libre open source software.
