---
layout: single
title: Archaeoinformatics Lectures
author: steko
created: 1193246817
---
<p>via <a href="http://www.alexandriaarchive.org/blog/?p=84">Digging Digitally</a>:</p>
<blockquote><p>The <strong><a href="http://archaeoinformatics.org/">Archaeoinformatics Consortium</a></strong> is pleased to announce the participants in the 2007-2008 Virtual Lecture Series schedule. The Virtual Lecture series involves leaders from around the world and many disciplines who each will be presenting information on their cyberinfrastructure initiatives and strategies and the ways in which their lessons learned may be useful to archaeology. In addition there will be presentations from archaeologists describing their successful cyberinfrastructure efforts.</p>

<p>These lectures are presented every other week using the NSF funded Access GRID video conferencing system. Many universities across the US, UK and Australia have Access GRID or compatible facilities. It is also possible to participate in the lectures by downloading the presentation slides and participating via a telephone bridge. Information on how to connect to the Access GRID system and alternatives are provided at <a href="http://archaeoinformatics.org/lecture_series.html">http://archaeoinformatics.org/lecture_series.html</a>. <em>For those who don't have access to GRID</em>, the lectures from the 2006-2007 series and this year’s lectures are also available as streaming video from the archaeoinformatics web site.</p>
</blockquote>
<p>This is a good way to promote the diffusion of open technologies in the archaeological field, even though not very standard (well, experiments are often non standard). Lectures' slides are in PDF format and videos are in the (crap) MPEG-4 format. Please, do use <a href="http://www.theora.org/">OGG Theora</a> next time!</p>
