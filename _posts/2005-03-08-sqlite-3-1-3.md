---
layout: single
title: SQLite 3.1.3
author: giofish
created: 1110311114
---
19 Feb. 2005: is out the new version of SQLite.<br />
SQLite is a small C library that implements a self-contained, embeddable, zero-configuration SQL database engine. Features include: 
<ul>
<li>Transactions are atomic, consistent, isolated, and durable (ACID) even after system crashes and power failures.</li>
<li>Zero-configuration - no setup or administration needed. </li>
<li>Implements most of SQL92. (Features not supported)</li> 
<li>A complete database is stored in a single disk file.</li>
<li>Database files can be freely shared between machines with different byte orders.</li>
<li>Supports databases up to 2 terabytes (241 bytes) in size. </li>
<li>Sizes of strings and BLOBs limited only by available memory. </li>
<li>Small code footprint: less than 30K lines of C code, less than 250KB code space (gcc on i486)</li>
<li>Faster than popular client/server database engines for most common operations.</li>
<li>Simple, easy to use API.</li>
<li>TCL bindings included. Bindings for many other languages available separately.</li>
<li>Well-commented source code with over 95% test coverage. </li>
<li>Self-contained: no external dependencies.</li>
<li>Sources are in the public domain. Use for any purpose. </li>
</ul>
The SQLite distribution comes with a standalone command-line access program (sqlite) that can be used to administer an SQLite database and which serves as an example of how to use the SQLite library.

For more information, you can see at URL: <a href="http://www.sqlite.org/" target="_blank">http://www.sqlite.org</a>
