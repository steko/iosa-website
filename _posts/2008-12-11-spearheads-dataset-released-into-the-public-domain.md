---
layout: single
title: ! '"spearheads" dataset released into the public domain'
author: steko
created: 1228991568
---
It's a great pleasure for me to announce that the "spearheads" dataset used in the "Digging Numbers" textbook by M. Fletcher and G. Lock has been released by the authors into the public domain.
<!--break-->
As you might know, the <a href="http://wiki.iosa.it/">Quantitative Archaeology Wiki</a> contains a <a href="http://wiki.iosa.it/diggingnumbers:start">section dedicated to the book's exercises</a>.

Until now, one had to get a copy of the book to have access to the example "spearheads" dataset. Since today (some days ago, actually) anyone can download the dataset in CSV format from the wiki page. Of course all readers are encouraged to continue using the textbook for everything else than the exercises.

We are grateful to Prof. Gary Lock who has kindly accepted to release the dataset into the public domain. I'd like to thank <a href="http://www.dur.ac.uk/a.r.millard/">Dr. Andrew R. Millard</a> who contacted personally Prof. Lock and made the first upload of the original dataset on the Quantitative Archaeology Wiki, and John Dougherty who pointed out some problems with the format of the CSV file.

The Digging Numbers exercises on the wiki are almost complete and there are still just a few rough edges. I hope that it will reach soon a final version. As always, everyone is welcome to contribute, even proof-reading helps.
