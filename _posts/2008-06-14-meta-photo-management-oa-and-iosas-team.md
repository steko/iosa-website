---
layout: single
title: Meta Photo Management --- OA and Iosa's team
author: luca
created: 1213438662
---
One of the first results of the collaboration between Iosa's team and Oxford Archaeology is available on https://launchpad.net/mpm . This is the test version of a tool, thought to manage photos and pictures metadata. It's based on the very powerful [Exiftool library](http://www.sno.phy.queensu.ca/~phil/exiftool/). The whole program is written using some Perl modules and is conceived as an archaeological oriented metadata manager. 

It provides some fields you can load, edit and save among the metadata of the photo. You have a preview tool and the possibility of exporting (appending or creating a new document) the data entered in an `*.xls` file.

It has been also tested a new function for recording the geospatial data into the GPS tag of the Exif file. This part of the program has been adapted for using the data coming for the Openmoko's gps.

This is just the first completely functioning version of the program, actually available just first Linux based systems.
