---
layout: single
title: Digital Heritage in the New Knowledge Environment - day 3
author: steko
created: 1226700337
---
The third and last day of the conference saw me and Elisa enter the National Bank hall around 11 in the morning, just in time to hear the CIDOC CRM presentation by the excellent Martin Doerr from ICS-FORTH. Formalization of knowledge in terms of semantic entities is a crucial task for the next decades, but I believe we aren't yet outside the experimental. At least until some easy and efficient tools come out, best if they are free open source software tools.

I foundthe talk by Christos Galanis about a Greek thesaurus very interesting: he was basically exposing the same set of problems and related approach we had been discussing on Saturday: it's absurd to retain copyright over works that date to many centuries ago, and the benefits of fostering free circulation of knowledge are plainly evident, furthermore from a public institution point of view.

Kostas Arvanitis was, if one, the true protagonist of the last session, with a wonderful demonstration of how museums can take great benefits from the adoption of new tools (like Web 2.0), but first and foremost a new attitude towards their public which requires stepping down from a “teaching” perspective and giving people the means to “build their stories” with objects and history. Of course this open attitude can pose some serious problems if it is applied in non-Western societies where excluding some categories (women, paria, etc.) from the transmission of knowledge is a legally and morally appreciated behavior.

Anna Simandiraki's presentation was simply great, not just because she wasn't in the hall and it was the translator who read her text, but her arguments towards a totally open policy for the Hellenic Ministry of Culture actually sounded like a summary of all those points that one part of the audience had been raising in the three days of the conference.

And finally Mrs Tsipopolou did her best to summarize all the themes the conference dealt with. I think it was a great success for the organizators (and I'd like to thank Evangelia Kappa in first place for her restless work), for all the Greek audience that had a unique chance to discuss freely about such topics, and also for the foreign audience: English people learnt a lot about the differences between common law and european law systems, but for me it was interesting to discover how Greek archaeologists work beyond excavation to manage archaeological heritage. I'd like to thank everyone who participated in this conference and I hope to get more people involved next time with this series of reports.
