---
layout: single
title: The Bricks Community
author: steko
created: 1168109123
---

The BRICKS Project – Building Resources for Integrated Cultural Knowledge Services – researches and implements advanced open source software solutions for the sharing and the exploitation of digital cultural resources.

The BRICKS Community is a worldwide federation of cultural heritage institutions, research organisations, technological providers, and other players in the field of digital libraries services. The Community orientates and validates the project results, and co-operates towards the creation of the BRICKS Cultural Heritage Network that will provide access to and foster the European digital memory.
