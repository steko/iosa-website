---
layout: single
title: SIDORA project
author: steko
created: 1113296517
---
Sidora, Open Source Software for Archaeology, presents an Archaeological Information System made for archaeological recording.

This brand new website can be found at <a href="http://www.sidora.org" title="SIDORA Project" onclick="window.open(this.href); return false;">http://www.sidora.org</a>.
