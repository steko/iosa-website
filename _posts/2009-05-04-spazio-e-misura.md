---
layout: single
title: Spazio e Misura
author: steko
created: 1241451820
---
Inoltro con grande piacere l'annuncio di Giancarlo:

<blockquote>
<p>Cari Amici,<br />
scrivo solo per informarvi dell'uscita del volume "Spazio e Misura", pubblicato da Edizioni dell'Università, Siena. Si tratta di una guida di geografia quantitativaapplicata alle scienze umane.</p>
<p>Il testo è distribuito gratuitamente in versione PDF è può essere scaricato dal seguente indirizzo:

http://www.archeogr.unisi.it/spazioemisura/</p>

Cari saluti,

   Giancarlo Macchi
</blockquote>
