---
layout: single
title: ! 'Open source GIS: gvSIG OA Digital Edition 2010 Beta out now'
author: steko
created: 1261046148
---
Forward from <a href="http://oadigital.net/aboutus/benjaminducke">Benjamin Ducke</a> (<a href="http://oadigital.net/">OA Digital</a>):
<blockquote>
Dear GIS users,
a beta version of the imminent <strong>gvSIG OADE 2010</strong> has just been released.
This release is based on gvSIG 1.9 which represents a milestone achievement in bringing a full-featured, free GIS to everyone's desktops.
Although it is marked "beta", this release is mature and functional. The final release will mostly add minor enhancements.
Please see our release announcement for details on the new features and capabilities, as well as known problems for this release: http://www.oadigital.net/software/gvsigoade/gvsigoade2010beta
There is not yet any updated documentation that covers the new features. So for now please continue using the gvSIG 1.1.2 documentation, which still applies by and large: http://oadigital.net/software/gvsigoade/gvsigdownload
Best regards,
Benjamin
</blockquote>
