---
layout: single
title: The Open Archaeology Software Suite
author: steko
created: 1189694873
---
<p>Open Archaeology is an umbrella-project that will host a number of sub-projects. While functional at a component level these sub-projects are intended to interoperate creating a complete AIS (Archaeological Information System), including the components necessary to manage the organisation carrying out the archaeology.</p>

<p>Open Archaeology is first a concept or philosophy, with the 3 primary strands of open data, open standards and open source. More information can be found at the Open Archaeology homepage (<a href="http://openarchaeology.net/">http://openarchaeology.net</a>). The philosophy has been around for some time; the software project started during 2006 by Oxford Archaeology (<a href="http://thehumanjourney.net/">http://thehumanjourney.net</a>), and was moved to Launchpad in 2007.</p>

<p>The initial code contributions came from Oxford Archaeology as part of that organisation's commitment to the Open Archaeology philosophy; there are many more people and organisations interested in participating. If you are one of them, don't hesitate to join in!</p>
