---
layout: single
title: ! ' Transferring of cultural heritage with new technology: modelling buildings
  with Inkscape and Blender'
author: steko
created: 1144943131
---
Lurking on <a href="http://www.blendernation.com" title="BlenderNation: daily Blender news">BlenderNation</a> I found today <a href="http://www.arthis.jyu.fi/bridge/inkscape.php" title="Transferring of cultural heritage with new technology: modelling buildings with Inkscape and Blender">this good tutorial</a> that can be useful for a simple approach to the study of ancient architectures, using two of our favourite free software tools: Inkscape and Blender.
