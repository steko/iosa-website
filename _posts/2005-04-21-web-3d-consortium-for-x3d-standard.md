---
layout: single
title: Web 3D Consortium for X3D Standard
author: steko
created: 1114115154
---
Web3D is to 3D what W3C is to the Web.

The X3D ISO standard defines a runtime system and delivery mechanism for real time 3D content and applications running on a network. It supports several file format encodings and programming languages, providing unsurpassed interoperability for 3D data and significant flexibility in manipulating, communicating and displaying scenes interactively. X3D incorporates the latest advances in graphics hardware, compression and data security to provide the best performance and visual impact in an extensible architecture that supports ongoing evolution. X3D's XML-encoded scene graph enables 3D to be incorporated into web services architectures and distributed environments, facilitating the movement of 3D data between applications.
