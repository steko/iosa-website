---
layout: single
title: Antiquist, and other mailing lists
author: steko
created: 1217325044
---
Just a few days ago, good news were published that the Antiquist community decided to open up its mailing list archives, making thus accesible to anyone months of discussions and e-mails.

I've been a subscriber of the Antiquist mailing list for many months right now and it is a pleasure to see how active its members are and the interesting discussions that take place there - sometimes also interconnected with the blogs aggregated on Electra Atlantis. There's indeed a true <em>community</em> around these electronic places.

However, I find bizarre the enthusiatic reactions towards the opening of the archives, while any “open-minded” community has its archives open: just look at our unlucky international mailing list (http://list.iosa.it/ - which has lots of users but very low activity) or the newly created Italian mailing list at <a href="http://gfoss.it/cgi-bin/mailman/listinfo/archeologia">gfoss.it</a> which is similar in scope to our earlier international ML and to Antiquist itself.

This is not to say that the Antiquist moderators were wrong in their first choice: on the contrary, their subsequent change of mind is a plain statement about the necessity of openness, starting from the basics. Furthermore, as I have previously noted here, [on+off]line communities (i.e. not only based on virtual places but also geared towards meetings and workshops) serve an important purpose because they allow young people to participate in “political” discussions about the scope and needs for the current archaeological practice.
