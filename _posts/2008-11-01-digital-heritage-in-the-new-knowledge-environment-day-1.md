---
layout: single
title: Digital Heritage in the New Knowledge Environment - day 1
author: steko
created: 1225495833
---
It's late night here in Athens but I still have some seconds to write a brief report about this first day of the International Conference.

The venue is great, I was impressed by the perfect organization of the conference. Even though most presentations today have been in Greek, there was real-time translation in English (and vice-versa when someone was speaking in English). The bulk of talks was about museums and the many different ways of using new digital media for innovating how the museum is perceived and visited. It's always fascinating to see how many applications can be created just using “common” tools like touch screens, websites and audioguides.

In the afternoon, Stuart Eve from L-P Archaeology gave his talk anticipating lots of things I will touch tomorrow, like the natural need to make all your raw data available without any limitation, and that great thing called open source software that everyone should be using for their work (especially when it comes to web-related content).

Tonight we had a reception given by the organizators, and it was time for relax. At least until tomorrow morning, when I should be giving my talk right after Leif Isaksen and Eric Kansa.
