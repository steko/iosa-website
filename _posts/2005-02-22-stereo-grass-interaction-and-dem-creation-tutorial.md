---
layout: single
title: Stereo-GRASS interaction and DEM creation tutorial
author: steko
created: 1109063324
---
Stereo is an accurate 3d measurement software for large (gray-scale tiff / targa)
stereo images, in order to produce CAD drawings, comparable with Photomodeler©.
First of all it aims at high qualiy, but the current version stereo-0.2b is still a little bit unstable, because its creator, Paul Sheer, has left the project in 1997. It has been tested with quite good results. We hope that its problems will be resolved in the near future, by the ITC-irst, which wants to develop it further and integrate it into Grass.
