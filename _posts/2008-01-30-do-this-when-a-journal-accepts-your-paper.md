---
layout: single
title: Do this when a journal accepts your paper
author: steko
created: 1201686337
---
via <a href="http://publishingarchaeology.blogspot.com/2008/01/do-this-when-journal-accepts-your-paper.html">publishing archaeology</a>:

<blockquote>
<em>This text is from:

http://www.arl.org/sparc/author/addendum.html </em>

<blockquote>Your article has been accepted for publication in a journal and, like your colleagues, you want it to have the widest possible distribution and impact in the scholarly community. In the past, this required print publication. Today you have other options, like online archiving, but the publication agreement you’ll likely encounter will actually prevent broad
distribution of your work.

You would never knowingly keep your research from a readership that could benefit from it, but signing a restrictive publication agreement limits your scholarly universe and lessens your impact as an author.

Why? According to the traditional publication agreement, all rights —including copyright — go to the journal. You probably want to include sections of your article in later works. You might want to give copies to your class or distribute it among colleagues. And you likely want to place it on your Web page or in an online repository if you had the choice. These are all ways to give your research wide exposure and fulfill your goals as a scholar, but they are inhibited by the traditional agreement. If you sign on the publisher’s dotted line, is there any way
to retain these critical rights?

Yes. The SPARC Author Addendum is a legal instrument that modifies the publisher’s agreement and allows you to keep key rights to your articles. The Author Addendum is a free resource developed by SPARC in partnership with Creative Commons and Science Commons , established non-profit organizations that offer a range of copyright options for many different creative endeavors.
</blockquote>
<em>Visit the SPARC site for more information


See some of my earlier blogs for more information on open access.</em>
</blockquote>
