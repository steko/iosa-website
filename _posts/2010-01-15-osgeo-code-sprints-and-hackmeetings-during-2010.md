---
layout: single
title: OSGeo code sprints and hackmeetings during 2010
author: steko
created: 1263587967
---
How many code sprints and hackmeetings are going to happen in the OSGeo world during this new year? The short answer is: a lot of them. If you are near one city, don't miss the chance to meet some OSGeo developers and see them hacking live.
<table>
  <tr>
    <th>Project</th>
    <th>Where</th>
    <th>When</th>
    <th>Info</th>
  </tr>
  <tr>
    <th>Mapbender</th>
    <td>Bonn, Germany</td>
    <td>February</td>
    <td><a href="http://www.mapbender.org/2010-02_Mapbender_Development_Sprint">Mapbender wiki</a></td>
  </tr>
  <tr>
    <th>QuantumGIS</th>
    <td>Pisa, Italy</td>
    <td>March</td>
    <td>http://www.qgis.org/wiki/3._QGIS_Hackfest_in_Pisa_2010</td>
  </tr>
  <tr>
    <th>C tribe</th>
    <td>New York, USA</td>
    <td>February 20-23</td>
    <td><a href="http://wiki.osgeo.org/wiki/New_York_Code_Sprint_2010">OSGeo wiki</a></td>
  </tr>
  <tr>
    <th>OSGeo tribe</th>
    <td>Bolsena, Italy</td>
    <td>June 6-12</td>
    <td><a href="http://wiki.osgeo.org/wiki/Bolsena_Code_Sprint_2010">OSGeo wiki</a></td>
  </tr>
</table>

And don't forget <a href="http://2010.foss4g.org/">FOSS4G 2010</a> in Barcelona!
<!--break-->
