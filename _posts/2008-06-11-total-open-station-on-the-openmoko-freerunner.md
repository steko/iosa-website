---
layout: single
title: Total Open Station on the OpenMoko FreeRunner
author: steko
created: 1213172781
---
<p>Joseph Reeves has <a href="http://blogs.thehumanjourney.net/finds/entry/2">showed us</a> something that wasn't even planned when we started the <a href="http://totalopenstation.sharesource.org/">Total Open Station</a> project.</p>
<p>He had our “helper” application running on the <a href="http://www.openmoko.org/">OpenMoko</a> FreeRunner. A few words from his blog post are enough to get the (beatiful) idea:</p>
<blockquote><p>The FreeRunner has a USB host port; you simply plug your phone into the Total Station, download the data and send it over the mobile network to our survey database in the office.</p></blockquote>
<p>And if you don't feel like using a smart phone because it's too small, just replace FreeRunner with <a href="http://eeepc.asus.com/">Asus EeePC</a> or <a href="http://www.laptop.org/">One Laptop Per Child</a>.</p>
<p>Free mobile archaeology.</p>
