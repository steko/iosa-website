---
layout: single
title: ! 'Archaeoinformatics Survey: Current Conditions and Needs in the Field'
author: steko
created: 1198090712
---
Via <a href="http://ancientworldbloggers.blogspot.com/2007/12/archaeoinformatics-survey-current.html">Ancient World Bloggers</a>:

> Archaeoinformatics.org has started a web survey focused on the needs and requests of archaeologists involved in the field of informatics. The survey is not directly on their website, but at the URL http://www.zoomerang.com/survey.zgi?p=WEB2275M2PM6ZH
> As part of its initiative to develop a cyberinfrastructure, Archaeoinformatics.org is interested in obtaining input on the current conditions and needs in the field. To that end the consortium, working with the SAA Digital Archaeology Interest Group and others, has developed an online survey.</cite> (from the ai.org website)

I don't know the details of the organization of the AI consortium and their aims within the SAA, but nevertheless this kind of initative seems quite useless to me. Collecting information from people doesn't mean that they will be able to develop tools and a <em>cyberinfrastructure</em> (I thought the <em>cyber-</em> prefix was dead in the early 90s) that fit the needs of archaeologists. This initiative sounds very much like a closed top-down process to me. However, I filled the survey (it takes about 10-15 minutes) and asked for the raw data to be published and possibly publicly discussed.
