---
layout: single
title: Open Access Archaeology is spreading
author: steko
created: 1180626974
---
<p><em>(through <a href="http://www.earlham.edu/~peters/fos/fosblog.html">Open Access News</a> and <a href="http://www.archaeogeek.com/blog/">Jo Cook's blog</a>)</em>:</p>
<blockquote><p><a href="http://www.wessexarch.co.uk/">Wessex Archaeology</a> have just announced that they will be using a Creative Commons license for the 600+ photos that they have on Flickr and in <a href="http://news.wessexarch.co.uk/gallery/">their gallery</a>.</p>
<p>Let’s hope that other heritage organisations follow suit. The “All Rights Reserved” copyright model is very restrictive when you study and record the past, and want to share some of that work with others to aid and encourage further learning.</p>
<p>By adopting the Creative Commons “Attribution-NonCommercial-ShareAlike 2.0″ license, they are actively saying to people “we want you to use our photos”. Which for a heritage organisation, is fairly novel!</p></blockquote>

<p>This is not the only good news: the <a href="http://intarch.ac.uk/">Internet Archaeology</a> online journal is <a href="http://www.jisc-collections.ac.uk/news_and_events/news_articles/internetarch_current_announce">moving towards OA</a>, too.</p>
<p>We are strongly committed to publish as open access digital documents the proceedings of the <a href="http://workshop07.iosa.it/">workshop about archaeology and open source</a> that <a href="http://www.grupporicerche.it">grupporicerche</a> organized some weeks ago in Genova. More news to come in the next weeks.</p>
