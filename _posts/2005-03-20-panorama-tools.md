---
layout: single
title: Panorama Tools
author: steko
created: 1111335933
---
Panorama Tools is a free program which can be used to 
generate, edit and transform many kinds of panoramic images.
Its main functionalities are...<!--break--> 

- Correction of images

  - Automatic cropping of images to brightest rectangle
  - Remove all sorts of barrel and/or pincussion distortion
  - Correct light fall-off at edges of your wide angle shots
  - Correct chromatic errors (colour separation) of your lens
  - Correct chromatic errors (colour separation) of your scanner
  - Skew/Unskew your images horizontally or vertically
  - Correction of scanning slit cameras
  - Fourier Transform Filtering/Wiener Filter

- Perspective Control

  - Simulates a shift lens in software
  - For normal and fisheye lenses

- Remap from any projection to any projection

  - Warp and unwarp normal, panoramic and fisheye images
  - Convert equirectangular LivePicture panos to QTVR-panos and vice-versa
  - Convert mirror images (BeHere-setup or similar) to any panorama and  vice-versa
  - Convert fisheye images to any panorama and vice-versa
  - Unwarp panorama sections (QTVR and LivePicture) to edit in Gimp, then rewarp and seamlessly insert back
  
- Adjust images into a panoramic view

  - Generate full panoramic view using any mixture of normal, fisheye and panoramic images
  - Built-in optimizer to find optimum pitch/roll/yaw for a given image to fit a panorama
  - Built-in optimizer to find optimum correction settings for a given image
  - Built-in stitching tool to automatically merge images into a panoramic view
  - Automatic colour adjustment of to be merged images
  - Extract any view (normal/fisheye/panoramic) from any 	panorama (RealVR/QTVR/rectilinear)
  - Insert any image (normal/fisheye/panoramic) into any panorama (RealVR/QTVR/rectilinear)
  
- Realtime Panorama Editor (only Gimp version) The realtime Panorama Editor is a separate Plug-in which makes the Gimp window an editable VR-viewer. You can pan left and right, tilt up and down, and zoom in and out. At any time you can use all Gimp tools to edit the image and instantly apply the changes to the warped panoramic image while keeping the interactive window open. This can be done with any view ("camera") and any panorama (see 'adjust' above).
