---
layout: single
title: MapServer Foundation
author: steko
created: 1133256198
---
The MapServer Foundation has been created to support and build the highest-quality open source geospatial software. The foundation's goal is to encourage the use and collaborative development of community-led projects. Their <a href="http://www.mapserverfoundation.org" title="MapServer Foundation" onclick="window.open(this.href); return false;">website</a> serves as a portal for users and developers to share their ideas and contribute to project development.
Among the founders of this project, the University of Minnesota, home of the famous open source <a href="http://www.iosa.it/node/11" title="University of Minnesota Mapserver description page on iosa.it">UMN Mapserver</a>, University of Iowa, Autodesk (yes, Autodesk, you read right), and DM solutions.
UMN Mapserver has been renamed MapServer Cheetah, but it is still the same, solid, open source web mapping software.
