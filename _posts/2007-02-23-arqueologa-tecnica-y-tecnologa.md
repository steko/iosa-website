---
layout: single
title: ! 'ARQUEOLOGÍA: Tecnica y Tecnología'
author: steko
created: 1172224101
---
These pages on the website of prof. Juan A. Barceló deal in detail with some of the hot topics in archaeological computing, like
<ul>
<li>Artificial Intelligence and Neural Networks used to analyze archaeological datasets</li>
<li>Statistical methods</li>
<li>Spatial Analysis techniques to be performed within GIS</li>
<li>and more</li>
</ul>

Content is available both in Catalá and English and can be useful to give students a complete and exhaustive overview of some of the most advanced techniques that can be used to improve and speed up archeological research.
