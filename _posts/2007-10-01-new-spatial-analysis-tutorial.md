---
layout: single
title: New spatial analysis tutorial
author: steko
created: 1191257749
---
<p>Today I have added a new tutorial in the <em>Spatial analysis</em> section of the <a href="http://wiki.iosa.it/">Quantitative Archaeology Wiki</a>.</p>
<p>Using <a href="http://grass.itc.it/">GRASS</a>, a simple method is shown for performing a basic exploratory analysis on the relationship between settlement sites and landscape variables like elevation and aspect. Further updates to the tutorial will include more screenshots and advanced quantitative analysis.</p>
<p>The tutorial is available at <a href="http://wiki.iosa.it/dokuwiki/spatial_analysis:settlements_and_landscape">http://wiki.iosa.it/dokuwiki/spatial_analysis:settlements_and_landscape</a>.</p>
