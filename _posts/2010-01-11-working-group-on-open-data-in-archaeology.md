---
layout: single
title: Working Group on Open Data in Archaeology
author: steko
created: 1263225537
---
A few days ago, thanks to Jonathan Gray of the <a href="http://www.okfn.org/">Open Knowledge Foundation</a>, a proposal for a Working Group on Open Data in Archaeology was drafted. There's a <a href="http://wiki.okfn.org/wg/archaeology">wiki page</a> for coordinating and collecting ideas &mdash; which acts also as a brief call for participation until a true one is sent.
The idea <a href="http://identi.ca/conversation/18590797">came out</a> after I discovered (and <a href="http://identi.ca/notice/18508246">liked it a lot</a>) the <a href="http://ckan.net/">Comprehensive Knowledge Archive Network</a> (CKAN ... a name familiar to anyone using <a href="http://www.cpan.org/">Perl</a> or <a href="http://cran.r-project.org/">R</a>), an OKF project defined as “Debian of data”. There are already a few packages there <a href="http://ckan.net/tag/read/archaeology">tagged “archaeology”</a> and it will be interesting to see how many open data resources are <em>already</em> available in archaeology. Then the working group purpose is going to be promoting best practices for making data open.
There's an <a href="http://wiki.okfn.org/meetings">informal meeting</a> on IRC tomorrow (1830 BST), see you there for those who want to dive in right now. More updates will follow via blog, mailing lists and other.
I'm really looking forward this working group as a major effort for bringing the freedom and opportunities of open data into archaeological research and resource management.
