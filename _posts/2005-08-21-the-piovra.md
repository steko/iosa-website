---
layout: single
title: the Piovra
author: steko
created: 1124658323
---
Piovra 1.2 is a python scripts set that allow you to distribuite a rendering process on a local network.
It can be used from a shell or a GUI, and it is possible to execute other applications or commands.
To better understand how to use piovra and what it can do have a look to the QUICKSTART section.
