---
layout: single
title: ! 'Open Source at the Lazaret '
author: steko
created: 1241442252
---
Looks like IOSA is not the only group of passionate archaeologists who develop free/open source software to fulfil their research needs and release their source code for the community.
The <a href="http://cambrien.unice.fr/index.html">LDPL</a> (Laboratoire Départemental de Préhistoire du Lazaret) maintains open source software mostly developed internally, but their source code is released to the Open Source Community under the GPL 2 licence.
<a href="http://cambrien.unice.fr/opensource/wiki/Archeobases">Archeobases</a> is an archaeological database manager written in Python (another choice we share), it's developed mainly for the research project at Lazaret even though a new refactored version is due in the next future. You can find more details about Archeobases and the LDPL committment to free archaeological software at http://cambrien.unice.fr/opensource/wiki .
