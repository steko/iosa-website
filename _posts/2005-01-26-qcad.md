---
layout: single
title: QCad
author: steko
created: 1106764542
---
QCad is an application for computer aided drafting in two dimensions. With QCad you can create technical drawings such as plans for buildings, interiors or mechanical parts. QCad works under Linux, Unix Systems, Mac OS X and Windows. The source code of QCad is released under the GPL (Open Source).
QCad was designed with modularity, extensibility and portability in mind. But what people notice most often about QCad is its intuitive user interface. QCad is a simple 2D CAD system for everyone. You don't need any CAD experience to get started with QCad immediately.
