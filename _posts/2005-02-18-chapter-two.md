---
layout: single
title: chapter two
author: steko
created: 1108765237
---
well next monday my lessons restart... they're the last lessons for my 3-years degree (or what else it is..). so i'm also starting to work on my thesis, which should be something like a 3d reconstruction of a mediaeval castle which I was digging in the past two years, with my teacher.
it sounds fine, and I hope it will be.
next week we will begin to rearrange hardware and software to put up some kind of open source archaeology lab, so that young students can see with their eyes what we are talking about here on this site.
hope I'll be able to post a picture of the work as it goes.
