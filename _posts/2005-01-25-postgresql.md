---
layout: single
title: PostgreSQL
author: steko
created: 1106677089
---
PostgreSQL is a highly-scalable, SQL compliant, open source object-relational database management system. With more than 15 years of development history, it is quickly becoming the de facto database for enterprise level open source solutions.
