---
layout: single
title: ! 'ArcheoFOSS 2010: back from Foggia'
author: steko
created: 1273443345
---
ArcheoFOSS 2010, the 5th Italian workshop on “Free software, open source e open format nei processi di ricerca archeologica” took place in Foggia, 6 and May. First of all, it was very good. I'm satisfied with this meeting. Why? Here are some thoughts I sketched while traveling back to Siena.

Lots of talks were about the results and methods of research done by MA and PhD students (myself included) - and this means one of the most important pieces of research, perhaps the most important at all, and the most underrated at the same time. Our community shows a strong connection between education and research. Making this connection stronger is part of our habits, I believe

There was a lot of discussion about methodology, and thanks to the firm experience of our friends in Foggia we have gone beyond some stereotypes of the past years. Take for example the recognition that methodology means much more than recording, documentation or technical tools. Add the acceptance of plurality as a (positive) fact rather than a problem. End up with the epiphany that using similar tools (e.g. databases, GIS) doesn't mean working with the same underlying methodological mindset. In Italy we have a very bad habit of not having a debate about method and theory, but with this workshop we're clearly building a place open for discussion.

We are well distributed geographically (from many regions of Italy) and chronological/disciplinary (from prehistoric to medieval archaeology, both excavation and landscape archaeologists). Despite this variability, there are some strong groups that are references for the whole community. I firmly believe that the University of Foggia should be listed among these groups since now. Even more interestingly, there are new groups of people that look very promising for their novel approach (I am glad to see that even my department could now be listed here). The ArcheoFOSS workshop is already acting as an incubator for innovation, and in the future we will see more of that, because of the large number of young researchers involved, the friendly and encouraging environment that is perhaps even more interesting than “open archaeology” for Italian academia. Or maybe it's just part of the “open archaeology” agenda.

Free software works. It works from a technical perspective, obviously, but also from a social one. We have been learning its limits, its potential and the ways to improve it and share it. There's a political vein in free software, and it's so well combined with the need for a new way of doing research in archaeology. On the technical side, I am more and more excited about how creativity is encouraged, instead of being pre-ordered. We are doing humanities - it would be so silly to lose our creativity (also when it goes towards chaos and anarchy), in the name of a pseudo-scientific strictness born out of a great misunderstanding. We already won one bet since the early 2000, but now we can play with something even more important: not just sharing software and methods, but sharing knowledge. This is our target for 2020, and what we are going to do for the next decade.

Lastly, we're learning how to <em>act in the real world</em>, and not just discuss among ourselves. Take for example the creation of common tools for creating catalogues. we can do that from the bottom up, with a wide perspective that is going to comprise technical standard, conservation and research needs - all as free software and open formats. grupporicerche already proposed some work in this direction last year, and we invite again all those who have developed databases for archaeological purposes to share them.

What's missing? Of course, we have lots of areas for improvement. This is also because of the “multidimensional” approach of this initiative. Here I list some topics that I'm particularly interested in:

<ul>
<li>quantitative and statistical methods: let's take back maths into archaeology through computing! This is not to say that archaeology can be reduced in numerical terms, but on the contrary to better define the complexity we are dealing with, giving the right weight to “data” (whatever that means) and developing proper archaeological ideas</li>
<li>an inter-regional and international approach, to deal with big not-so-big research themes in a collaborative way</li>
<li>encouraging the upgrade of old databases from obsolete, proprietary formats to open and free formats, ready for dissemination on the web</li>
<li>build a technological infrastructure for sharing our work, in the many forms it can take - or at least develop best practices for doing that on our own, taking accessibility and sustainability into account since day #0</li>
</ul>

More comments, insights and excerpts from the round table to follow in the next few days.
