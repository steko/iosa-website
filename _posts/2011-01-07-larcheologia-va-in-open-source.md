---
layout: single
title: L'Archeologia va in Open Source
author: steko
created: 1294387578
---
Today an article that I wrote was published on Nòva24, a weekly supplement to “Il Sole 24 ore”, a major Italian newspaper. You can read it online at <a href="http://www.ilsole24ore.com/art/tecnologie/2011-01-06/larcheologia-open-source-065025.shtml">ilsole24ore.com</a>. Despite its title, the article deals with the challenges that society is posing to the cultural heritage sector, and why technology matters to the debate. I advocate an approach made of openness towards the public as a means to give value to archaeology. This is obviously the approach of the IOSA project, that has now entered its seventh year and encompasses a wide range of topics, from free software to open formats and standards for digital storage, and open data. Thanks to Raimondo Iemma who kindly asked me to write this article.
