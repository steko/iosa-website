---
layout: single
title: Statistical analysys of archaeometrical data should be done with R
author: steko
created: 1227107404
---
<p>Quote from the last page of the recent article <em>On statistical approaches to the study of ceramic artefacts using geochemical and petrographic data</em> by Baxter MJ, Beardah CC, Papageorgiou I, Cau MA, Day PM, Kilikoglou V appeared on «Archaeometry» 2008 February;50(1):142-157. Available from: http://dx.doi.org/10.1111/j.1475-4754.2007.00359.x .</p>
<blockquote><p><em>All the analyses reported in this paper were undertaken using S-PLUS 2000 for WINDOWS
(Venables and Ripley 1999), and our implementation is described in Beardah et al. (2003).
This system is now obsolete, having been superseded by later versions of S-PLUS, described
in the fourth edition of Venables and Ripley’s (2002) book. Were we to start this research anew
<strong>we would use the R system, which has similar functionality to S-PLUS with the additional
advantage of being Open Source</strong>. Venables and Ripley (2002) is a good guide to R as well as
S-PLUS, though other texts at various levels are increasingly appearing.</em></p></blockquote>
<p>and they also provide some insight into the software procedures they have been using to carry out the analysis. Another good reason to choose free and open source software when you are planning your next research project.</p>
