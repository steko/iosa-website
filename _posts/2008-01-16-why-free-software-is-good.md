---
layout: single
title: Why free software is good
author: steko
created: 1200480305
---
<p>Some time ago I posted here the <a href="http://www.iosa.it/content/harris-matrix-graphviz">first</a> of a series of tutorials about the use of Graphviz for producing an Harris matrix. The second part I'm writing is a bit more technical, and shows how to use the Python language to create a custom application that makes recording easier and faster for most users.</p>
<p>The Python-Graphviz bindings I chose are the <a href="https://networkx.lanl.gov/wiki/pygraphviz">pygraphviz</a> ones. One thing I immediately noticed was that a method for using the <code>tred</code> command was missing. And I had just showed you how much it was useful...</p>
<p>So, I filed an <a href="https://networkx.lanl.gov/ticket/133">enhancement request</a> for the pygraphviz library. Shortly after that, I wrote a small patch that implemented a <code>tred()</code> method. And a few days later, one of the developers came out with a much better patch and implementation of the whole thing.</p>
<p>This changeset was included in the <a href="https://networkx.lanl.gov/wiki/pygraphviz/News">0.36 release of pygraphviz</a>, which came out on January, 13th 2008. Today I found the <a href="http://packages.debian.org/sid/python-pygraphviz">update for Debian Sid</a> in the repositories.</p>
<p>I like free software. And, yes, part 2 will be soon ready.</p>
