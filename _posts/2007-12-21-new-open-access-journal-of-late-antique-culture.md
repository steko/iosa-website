---
layout: single
title: New Open Access journal of late antique culture
author: steko
created: 1198252266
---
<blockquote><cite>The <a href="http://www.cardiff.ac.uk/clarc/jlarc/jlarc-home.html">Journal for Late Antique Religion and Culture</a> is a new peer-reviewed OA journal from Cardiff University.  The <a href="http://www.cardiff.ac.uk/clarc/jlarc/Table of Content/volume-1-2007.html">inaugural issue</a> is now online.</cite></blockquote>
Can please late antique archaeologists take example from this?
(via the always excellent <a href="http://www.earlham.edu/~peters/fos/2007/12/new-oa-journal-of-late-antique-culture.html">Open Access News</a>)
