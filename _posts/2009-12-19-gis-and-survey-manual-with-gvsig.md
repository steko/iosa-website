---
layout: single
title: GIS and survey manual with gvSIG
author: steko
created: 1261259672
---
Good news from Anna Hodginkson of Oxford Archaeology North via <a href="http://groups.google.it/group/antiquist/browse_thread/thread/9a432b6ac06dfd04#">Antiquist</a>:
<blockquote>
I would like to announce the release of the Survey and GIS manual produced at Oxford Archaeology North during the last few months after development of new on-site survey and GIS methodologies applicable to any archaeological project.
[ ...]
The downloading software used in this manual is Leice GeoOffice (can be replaced by whatever software comes with the Total Station used) and GIS software is the gvSIG OA Digital Edition. I furthermore recommend Inkscape as a vector editing software for touching up maps produced by gvSIG.
This document is available for download as PDF at http://www.openarchaeology.net/project/survey-and-gis-manual
I would be extremely grateful for feedback!
</blockquote>
The manual is very detailed, with literally dozens of program screenshots, field pictures and step-by-step instructions. Of course it can sound very specific for those who already have a working procedure similar to the one described, but it can be nevertheless of great help for those who are learning or would like to see how things work in the real world.
Given that <a href="http://tops.berlios.de/">Total Open Station</a> supports Leica TCR formats, we're going to work with Oxford Archaeology North to upgrade the manual also for our software. That would also mean that it can be run on any operating system.
