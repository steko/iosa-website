---
layout: single
title: Thuban
author: steko
created: 1113941156
---
Thuban is an Interactive Geographic Data Viewer  with the following features:
<ul>
<li>Vector Data Support: Shapefile, PostGIS Layer</li>
<li>Raster Data Support: GeoTIFF Layer</li>
<li>Comfortable Map Navigation</li>
<li>Object Identification and Annotation</li>
<li>Legend Editor and Classification</li>
<li>Table Queries and Joins</li>
<li>Projection Support</li>
<li>Printing and Vector Export</li>
<li>API for Add-Ons (Extensions)</li>
<li>Multi-Language Support: English, French, German, Hungarian, Italian, Russian and Spanish</li>
<li>User Manual (English)</li>
</ul>
Thuban is extensible and multi-platform (GNU/Linux, Windows, ...). It is Free Software under the GNU General Public License (GNU GPL).
