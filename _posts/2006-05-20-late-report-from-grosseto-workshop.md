---
layout: single
title: (late) report from Grosseto workshop
author: steko
created: 1148130928
---
<em>This is a late and very personal report about the workshop we had in Grosseto on May, 8th.</em>

I came back to Siena quite late in the evening and I was very, very tired after such a day. But I was also satisfied. I'll try to explain why.
The workshop was great and at some point exciting. The idea was itself an experiment, which Giancarlo Macchi and ASIAA Lab took courage enough to make. It was a successful experiment indeed. We had an intense discussion that in the end was stopped just because the meeting hall had to be closed, but it had been lasting for 2 hours at that moment. In the hours before we heard to more than 12 talks about things such:
<ul><li>this website :-)</li>
<li>free/open source applications for archaeology, including <a href="/node/180" title="ArcheOS Akhenaton GNU/Linux live DVD">ArcheOS</a> by Arc-Team</li>
<li>webgis applications for the public presentation of the research results</li>
<li>open standards for documentation (mainly XML)</li>
<li>archaeological methodology and informatics</li>
<li>latest developments in the free software world (primarily <a href="http://grass.itc.it" title="GRASS GIS home page">GRASS</a> with Markus Neteler talking)</li>
<li>something else I've forgotten during these days ;-)</li></ul>

There were more than 100 people attending, most of them archaeologists but also informatics and geographers, all mostly from the academic Italian entourage. Also a good number of students were there, that was encouraging for me (though I'm still a student too).

What clearly was said is that there's a need for a stronger methodological approach to this large and somewhat confused topic, and our main effort will be from now on in the direction of less technical talks and discussions, to the advantage of a true archaeological (which actually means: methodological) approach. Thus, <strong>free software</strong> should IMHO be somewhat an a priori choice, not to be discussed furthermore so that we can focus on our research. This is really a need in Italy where we have so many people working in archaeology, but with a strong lack in the scientific community debate.

We decided all together 2 main things for the future:
<ol><li>Next year there will be a second workshop, that has to be different from this first in order to achieve the forementioned objectives. It will be held in Genoa by <a href="http://www.grupporicerche.it" title="GruppoRicerche - Home page [it]">our research group</a> within <a href="http://www.iisl.it" title="Istituto Internazionale di Studi Liguri - Home page [it]">Istituto Internazionale di Studi Liguri</a> organization.</li>
<li>We decided to put up a <strong>mailing list</strong> with which we can keep continuously in contact, and a choice for english language was made as an attempt to make easier the growing of a larger, international community. You can find the info page of the mailing list here: http://list.iosa.it where you can subscribe and search in the archives (still empty at the moment!)</li></ol>

You see, I had some good reasons to be satisfied indeed. Our hope (at least mine) is that this is the beginning of something really new that helps the archaeological discipline and research. Next year we'll try our best. For now, the next step in this <q>process</q> is at the <a href="/www2/node/215" title="International Congress Cultural Heritage and New Technologies Workshop 11 Archäologie und Computer">Vienna Workshop</a> in October, where a specific section on <q>Archaeology and Free Software</q> has been put up. See you there.
