---
layout: single
title: ! 'Quantitative Archaeology Wiki: some recent fixes to the platform'
author: steko
created: 1227544082
---
<p>The Quantitative Archaeology Wiki is perhaps one of the most important sub-projects of IOSA, and this explains for the continuous efforts made in enhancing the wiki platform, adding new content and enabling users and contributors to take advantage of this collaborative environment at its best.</p>
<p>I have been doing some work behind the scenes recently, and now I'd like to share the results with all iosa.it readers:</p>
<ul>
<li>a new look based on the dokubook template, more similar to Wikipedia, that should make people more comfortable with the wiki</li>
<li>a change of license: we have switched from the GNU Free Documentation License to the more clear and simple to use Creative Commons Attribution-Share Alike 3.0 Unported License, thanks to the addendum of the <a href="http://www.gnu.org/copyleft/fdl.html">new 1.3 version of the license</a></li>
<li>in our continuous effort to keep spammers away from the wiki, a new unobtrusive captcha control has been added to the <em>edit</em> page, just for unregistered users</li>
</ul>

<p>As always, the wiki is open not only for those who want to read and learn, but also for who has the skills to write new documentation. In the meanwhile, we are polishing the “Digging Numbers” exercises and hope to come out with a final version soon.</p>
