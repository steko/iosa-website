---
layout: single
title: QGIS 0.7 Released
author: giofish
created: 1126346839
---

Version 0.7.0 of QGIS has been released. It features a lot of changes since 0.6, including a raster georeference plugin for creating world files, a highly-customizable GRASS toolbox, improvement in PostgreSQL/PostGIS enhancements, better digitizing tools, raster query and graphing, and [more](http://community.qgis.org/index.php?option=com_content&task=view&id=66&Itemid=60 "QGIS 0.7 release notes").


<p>Binary versions for Windows and OS X are available on <a href="http://qgis.org" title="Quantum GIS official web site" onclick="window.open(this.href); return false;">QGIS official web site</a>. RPM and other packages for Linux are expected to be available in the coming days.</p>
