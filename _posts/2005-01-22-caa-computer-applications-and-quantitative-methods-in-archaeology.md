---
layout: single
title: CAA - Computer Applications and Quantitative Methods in Archaeology
author: steko
created: 1106415487
---
CAA is an international organisation bringing together archaeologists, mathematics and computer scientists. Its aims are to encourage communication between these disciplines, to provide a survey of present work in the field and to stimulate discussion.
