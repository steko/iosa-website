---
layout: single
title: Una via archeologica all'informatica
author: steko
created: 1263641599
---
Marco Valenti from the University of Siena and his <a href="http://archeologiamedievale.unisi.it/">LIAAM</a> (Laboratorio per l'Informatica Applicata all'Archeologia Medievale) recently published [a new manual](http://www.edigiglio.it/scheda.asp?ctg=2&keyw=791) about archaeology and computing, or as they like to say “an archaeological way to computing”.


In his introduction, he also writes:
> In questo contesto il recente interesse dell’archeologia per il mondo <strong>open source</strong> può costituire un buon punto di incontro fra le discipline umanistiche e le scienze informatiche. Il tipo di approccio “mentale” proposto dalle comunità open source, le esperienze di utilizzo degli open format, la circolazione degli open content possono portare contributi importanti alle nostre ricerche; soprattutto se intesi come strumenti per attuare un fecondo interscambio delle informazioni e garantire qualità e trasparenza alla ricerca, incentivando un utilizzo della tecnologia come mezzo per facilitare la costruzione e la diffusione di un sapere storico collettivo, i due obiettivi principali (l’uno scientifico, l’altro sociale) dell’archeologia. In altre parole, traslare i principali concetti del mondo open source, applicandoli alla ricerca archeologica, significa essenzialmente progettare architetture del dato aperte e prevedere la condivisione delle informazioni, due principi alla base di tutte le attività e le soluzioni sviluppate presso il LIAAM.

I cannot but agree with his words, and I hope not only that our university will eventually play again a leading role in bringing technology and archaeology together, but also that new manuals expressing such “radical” views really help students in learning a new, different way for doing public archaeology.
