---
layout: single
title: Open Letter Concerning the Recent Firing of the University of Pennsylvania
  Museum Researchers
author: steko
created: 1231495612
---
http://ancientworldbloggers.blogspot.com/2009/01/open-letter-concerning-recent-firing-of.html

<blockquote><p>To: ARCHAEOLOGISTS AND THE CONCERNED COMMUNITY AT LARGE

To whom it may, it should or it would concern,

We the undersigned, academics and graduate students who are engaged with the future of archaeology, are deeply troubled by the recent announcement of the termination of eighteen research specialist positions at the University of Pennsylvania Museum of Archaeology and Anthropology, the abolishing of those research positions, and the shutting down of their associated laboratories and centers such as MASCA (the Museum Applied Science Center for Archaeology)</p></blockquote>

I'm shocked by this letter for many reasons, the first two coming to my mind are:
<ul>
<li>I've studied on books written by Patrick McGovern during the past years and I've always thought its research was one of those “fundamental” enquiries about the human past that deserve a whole person's life dedicated to it</li>
<li>I hoped that Italy was the only country where archaeology would totally loose in a few years any public funds, with the notable exception of buying looted vessels from abroad</li>
</ul>

Please sign the petition.
