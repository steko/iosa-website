---
layout: single
title: È disponibile il programma del 4° workshop
author: steko
created: 1238085518
---
Il programma completo del <a href="http://www.iosa.it/content/iv-workshop-italiano-free-software-open-source-e-open-formats-nei-processi-di-ricerca-archeo">"4° workshop italiano “Open source, free software e open formats nei processi di ricerca archeologica”</a>  è ora <a href="http://www.archeo-foss.org/Programma/ProgrammaArcheoFoss.pdf">disponibile</a>.
Ricordiamo che il workshop si svolgerà i giorni 27 e 28 aprile 2009, presso la sede centrale del CNR di Roma.
Il progetto IOSA sarà presente con due interventi e un laboratorio introduttivo sulla statistica in archeologia, basato sul <a href="http://wiki.iosa.it/it:start">Wiki di Archeologia Quantitativa</a>.
