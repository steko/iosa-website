---
layout: single
title: X3D in archeology presentation at the Indo-U.S. Science & Technology Forum
author: steko
created: 1134382450
---

The [Indo-U.S. Science & Technology Forum on Digital Archaeology](http://www.indousstf.org/) was held at Mussoorie, India on November 11-13, 2005. Among technology issues discussed at the forum, several presentations were given on the role of X3D in archaeology.
