---
layout: single
title: from parachutist to truffle hunter
author: Loqutus
created: 1147334634
---
My question to the forum community is related to the need to back to the earth.
I was very well impressioned by Grosseto's workshop, but, in the same time, a bit concerned about.
I tried to explain my thought, but I'm not sure I was cristall clear. I try again.
I asked about costs. An archaeologist must spend some 5 months per year on a digging site (never forget that no digging no applied computer science), 3 months on potsherds and is always late for reports. An arcaheologist cannot spend 12 months per year facing a flat monitor, searching for new solutions. So...If I leave the old path for the new one...how much does it cost to me in terms of self-learning, data portability (we used to have tons of data)? And, above all, where to turn myself? I felt that computer scientists do agree in terms of generalities (parachutists), but they offer different solutions (truffle hunters).
I turned all my GIS bases into Grass for linux. But at the moment just one person of my research group can manage the new "age". So the transition is not free at all. And I ask to you all to discuss more about how to allow a soft and low cost transition to OS. If you really want to have more appeal, I think this is the right way.
Otherwise one can think that within one or two years computer scientists should find new, more elegant ways, that will mean for archaeologists to change again and learn again starting from level "0", which is not possible.
I think that this sounds like the relationship between mathematics and physicians: the latters use maths for very concrete porpouses, even if the formers never find their solutions very elegant.
I need GIS not to look better, but to answer some very precise questions. I don't mind elegance, I don't care about the last frontier, unfortunately...I must be a truffle hunter.
I hope to have news and replies and start a very intriguing e-cohoperation.
