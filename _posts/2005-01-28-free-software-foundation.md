---
layout: single
title: Free Software Foundation
author: steko
created: 1106938456
---
The GNU Project was launched in 1984 to develop a complete UNIX style operating system which is free software: the GNU system. (GNU is a recursive acronym for “GNU's Not UNIX
