---
layout: single
title: Publishing digital collections. An experiment with “Ancient Marbles”
author: steko
created: 1290061931
---
Everyone is taking thousands of digital photographs each year. For an archaeologist, it's common practice to collect pictures of museum artifacts, archaeological sites and landscapes.

I want to push the concept of “digital collection” going beyond the traditional habit of creating a database. Federico Marri and I have started to build *something* about Ancient marbles, that is all semi-precious building and decorative stones that were used in the Greek and Roman Classical antiquity and afterwards.

<div style="text-align:center"><a href="http://www.flickr.com/photos/steko/5181245089/" title="Greenstone from Thessaly di archeosteko, su Flickr"><img src="http://farm2.static.flickr.com/1419/5181245089_247ffd30cd_m.jpg" width="180" height="240" alt="Greenstone from Thessaly" /></a></div>

We are building *what*? Something. Let me me explain in more detail.

For example, take the [Ancient Marbles Wiki](http://ancientmarbles.wikia.com/) (hosted at Wikia). As a self-appointed <span lang="el">Υπουργος της βικιποιεσις</span> I have a strong motivation to use a wiki as a place to create shared knowledge. It already has some basic content, and we are pulling in descriptions from public domain scholarly reference texts like those by Pullen and Corsi (both available for free on [Archive.org](http://www.archive.org/) or Google Books). We have a loosely defined set of information we want to collect about all kinds of marble, but when that is done, it's entirely up to the wiki contributors wheter a simple "record sheet" will become a truly enciclopedic page or not. This is not a database, but when content will be more exhaustive it will be usable as a source for Wikipedia itself, or could be directly linked to as a reference for ancient marbles. We believe in linking, but this wiki is for now a single, simple platform. It's not inherently part of the web.

Take #2. Introduce [wordpress.com](http://www.wordpress.com/) and [flickr](http://flickr.com/). The flux of new material about ancient marble is not consistent, and varies in time. Starting yet another blog is just the simplest thing in the world, and it looks like there isn't yet a [blog about ancient marbles](http://ancientmarbles.wordpress.com/). Obviously, wordpress.com gives lots of space and flexibility, but more than that it enables us to switch to a self-hosted webserver in 15 minutes. Other publishing platforms are equally well-equipped (think tumblr or posterous) but don't have a .tar.gz on their home page. Enough for the freedom. Everytime we find some nice column, base, or slab, we are going to post a new short article about it, with a picture. And here comes the “trick”. The image is not hosted on wordpress.com, but on flickr. Flickr gives you unlimited space, 100 MB worth of upload each month, automated CC licensing of your content, and some other interesting features, like [OpenStreetMap tagging](http://code.flickr.com/blog/2009/09/28/thats-maybe-a-bit-too-dorky-even-for-us/). Again, there are dozens of photo uploading websites, but flickr is the only one that offers this capability. OSM tagging means that there's a link between the photograph and the OSM feature, like a church, a museum, an archaeological site, a monument. To know the OSM way id of your selected feature, follow these simple steps:

1. go to the map
2. zoom in as much as possible on the area where the building is
3. from the right toolbar toggle the "Data" layer on
4. when the sidebar is loaded on the left, you will be able to select the building with your pointer
5. you will see a link to its page in the left sidebar
6. from that page, you will find the “way ID”

In OSM jargon, each building is a "way" and flickr wants to know the way ID, which should then be added as a “machine tag” to the photograph, like `osm:way=32260844` for the [Βυζαντινό και Χριστιανικό Μουσείο](http://www.byzantinemuseum.gr/) (Byzantine and Christian Museum) in Athens. From that point, flickr knows that this photograph is about that *place*, it's more than a couple of geographic coordinates. See for example all the pictures tagged with [`osm:way=10973689`](http://www.flickr.com/photos/tags/osm%3Away%3D10973689/), best known as the [Parthenon](http://en.wikipedia.org/wiki/Parthenon). You can get feeds for each tag and literally “follow” your favourite monument.

You may argue that this is way more complicated and geek than just using flickr's built-in map, but what if that map was based on OSM as well? Yes, we all know, coverage is less than desirable especially out of large urban centres, but since Greece is [opening up lots of their geodata](http://geodata.gov.gr/) I'm confident about huge improvements in the next year. Also, don't forget that you can add new content to OpenStreetMap. I just added the [Antiquarium](http://www.openstreetmap.org/browse/way/85535159) of Ostia Antica, where we took some fantastic photographs last month. This “feature” has also its drawbacks: anyone could delete a way and break the system. Alas, nothing is perfect. Comments welcome.
