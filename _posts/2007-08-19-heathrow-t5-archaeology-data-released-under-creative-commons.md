---
layout: single
title: Heathrow T5 archaeology data released under Creative Commons
author: steko
created: 1187550854
---
<p>From <a href="http://www.pastthinking.com/blog/2007/08/17/heathrow-t5-archaeology-data-released-under-creative-commons/">PastThinking</a>:</p>

<blockquote><p>Last week, <a href="http://www.framearch.co.uk/">Framework Archaeology</a> launched an update to their Archaeology at Heathrow T5 website. The update includes an improved version of their (Windows only) <a href="http://www.framearch.co.uk/t5/data-downloads">free GIS</a> that allows you to explore the hard archaeological data collected during “Perry Oaks” phase of the investigations. </p>
<p>Crucially, the <a href="http://www.framearch.co.uk/t5/data-downloads/raw-data-downloads/" onclick="javascript:urchinTracker('/outbound/www.framearch.co.uk');">raw data</a> behind the GIS has been released in a <a href="http://www.framearch.co.uk/t5/data-downloads/raw-data-downloads/">variety of useful formats</a>. It is released under a <a href="http://creativecommons.org/">Creative Commons</a> license (<a href="http://creativecommons.org/licenses/by-nc/2.0/uk/">Attribution-NonCommercial</a>), so it’s free to use for whatever you like so long as you don’t make any money from it.</p>

<p>Formats include CSV, XML, SHP and GML. The photographs and scanned section drawings are also available to download.</p>
<p>Despite being announced on a few mailing lists, it appears to have slipped under the radar of most people. To release this much raw data about such an important and interesting archaeological site, under a flexible license that encourages reuse, is quite a milestone.</p>
</blockquote>

<p>This sounds like great news. The next step is to see what others will eventually do with the T5 data. I'm thinking about the creation of a free dataset for the <a href="http://wiki.iosa.it/" title="Quantitative Archaeology Wiki">Quantitative Archaeology Wiki</a> tutorials.</p>
