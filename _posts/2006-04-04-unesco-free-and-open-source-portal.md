---
layout: single
title: UNESCO Free and Open Source Portal
author: steko
created: 1144169005
---
The UNESCO Free Software Portal gives access to documents and websites which are references for the Free Software/Open Source Technology movement. It is also a gateway to resources related to Free Software.

With the Free Software Portal, UNESCO provides a single interactive access point to pertinent information for users who wish to acquire an understanding of the Free Software movement, to learn why it is important and to apply the concept. Visitors to the UNESCO Free Software Portal can browse through pre-established categories or search for specific words. They can add a new link or modify an already existing link.
