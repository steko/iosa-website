---
layout: single
title: R Spatial Projects
author: steko
created: 1169319730
---

This collection of web pages is intended to be a guide to some of the resources for the analysis of spatial data using <a href="http://www.r-project.org/">R</a>, and other associated software. Another useful resource is the CRAN <a href="http://cran.r-project.org/src/contrib/Views/Spatial.html" target="_self">Spatial</a> Task View.

It includes packages like **spBayes**, an R package for  hierarchical spatial modelling, **rgdal** and **spgrass6**, which are meant to manage spatial information using the most powerful open source tools. There is a mailing list, that of the <a href="https://www.stat.math.ethz.ch/mailman/listinfo/r-sig-geo" title="R Spatial Special Interest Group mailing list">R-sig-geo</a>.
