---
layout: single
title: happydigger 1.2 released
author: steko
created: 1111389829
---
Happydigger is a program which can be used for cataloging archaeological finds. It is intended both for semi-professional use and by amateurs (e.g. metal detector users) who want to keep track of their finds. Data is stored in a database with extensive find and findspot details. If images are available, they will be displayed together with the find information. 
