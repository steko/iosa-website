---
layout: single
title: autumn news
author: steko
created: 1127935218
---
<p>Comes a day when it's time to give some piece of news about what we're doing, and today it is.
We're writing down a brief tutorial on Walldrawer, a free photo-rectifying tool, soon it will be available.
Our laboratory is growing up, today I started installing Debian Sarge on two computers. 
Our users are more and more everyday and we hope this web site is really useful for professional archaeologists, students, and everyone interested in this field. Today I can put to your attention Prof. Carvalho who teaches <em>Informática aplicada à Arqueologia</em> at Universidade de Coimbra, Portugal.
<!--break-->
In his course's reference materials he put also this site, and we would be really glad to get some feedback from students visiting IOSA.it.</p>
<ul><li><a href="http://www.uc.pt/ects/cursos/cadeiras/detalhe.php?id=2198" title="Informática aplicada à Arqueologia - Cadeira do curso de Arqueologia e História da FLUC" onclick="window.open(this.href); return false;">Prof. Carvalho's Home Page</a></li></ul>
