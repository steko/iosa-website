---
layout: single
title: Risorse on line per l’archeologia
author: steko
created: 1205752479
---
**iosa.it** is cited in an interesting paper by Antonella D'Ascoli (<a href="http://www.jiia.it/"><acronym title="Journal of Intercultural and Interdisciplinar Archaeology">JIIA</acronym></a>) which was presented in 2006 as part of the project <a href="http://documenti.rinascimento-digitale.info/index.php?title=Biblioteche_Digitali_in_Italia">Biblioteche Digitali in Italia</a>. Here's an excerpt from the text:

>iosa.it, che nasce, nel 2004, sul versante esterno della collaborazione e supporto alle Istituzioni (Gruppo Ricerche, afferente alla sezione di Genova del prestigioso Istituto Internazionale di Studi Liguri di Bordighera - I.I.S.L.), unico nel suo genere, perché esclusivamente orientato alla promozione del Free/Libre Open Source Software in archeologia, dei Free and open formats and standards, e dell’Open Access to scientific and scholarly publications,  includendo anche il supporto all’iniziativa Science Commons ed in particolar modo attento agli sviluppi dell’Open GIS.

Thanks Antonella for supporting our project!
