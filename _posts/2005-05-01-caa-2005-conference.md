---
layout: single
title: CAA 2005 Conference
author: steko
created: 1114947861
---
Shame on me, I didn't notice this important meeting! The CAA 2005 Conference was held in Tomar (Portugal) 21-24 March 2005.
The home page can be found on <a href="http://www.caa2005.ipt.pt/GeneralInfo.htm" title="CAA 2005 Conference Home Page" onclick="window.open(this.href); return false;">http://www.caa2005.ipt.pt/GeneralInfo.htm</a>
There you can find also abstracts for all the papers.
