---
title: ! 'ArcheoFOSS 2012: one week left to submit your abstracts'
author: steko
created: 1332235864
---
There is one week left to submit your abstracts for the 2012 ArcheoFOSS workshop. The deadline is on Monday 26th March 2012. The call for papers and posters (in Italian) is available on the main website [archeofoss.org](http://www.archeofoss.org/) together with details on the submission procedure.

This year we're aiming for a wider participation. On one hand, this means that we expect more people to help us with spreading the word about the call for papers, and to submit proposals on their own. On the other hand, we will need to operate a selection for the more interesting abstracts, to keep the quality of this event higher than ever.

If you are doing archaeological research in italy and you use free and open source software or you're interested in discussing it, this is the one meeting you should go to.
