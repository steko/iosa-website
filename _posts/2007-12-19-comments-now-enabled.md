---
layout: single
title: Comments now enabled
author: steko
created: 1198085981
---
Our effort to keep iosa.it always updated and filled with useful content keeps us very busy.
Some months ago, comments from non-registered users were disabled due to excessive spam load which we weren't able to manage. Now, we have found some time to work on the Drupal installation and anonymous comments are again enabled, with a (simple) captcha protection. Should we encounter again spam problems, we will raise the difficulty of the tests.
I hope this can lead to a more open discussion about our published works. Since 2004, when we started the IOSA project, the number of blogs about archaeology has been always increasing and now there are lots of them. Comments are an important part of the blog concept, and even tough iosa.it is not <em>only</em> a blog, a part of it looks much like it.
See you in the blogosphere, the IOSA team
