---
layout: single
title: Archaeocommons
author: steko
created: 1130631000
---
Recently I asked the <a href="/node/140" title="Science Commons">Science Commons</a> staff about similar initiatives in the archaeology-related field of studies and publishing.

John Wilbanks kindly answered to me and he gave me some nice URIs to start from. Here they are:
<ul>
<li><a href="http://www.culturalheritageinternational.org/anthrocommons/" title="Anthrocommons" onclick="window.open(this.href); return false;">Anthrocommons</a></li>
<li>the <a href="http://www.alexandriaarchive.org/" title="the Alexandria Archive" onclick="window.open(this.href); return false;">Alexandria Archive</a> </li>
<li><a href="http://www.archaeocommons.org/" title="Archaeocommons" onclick="window.open(this.href); return false;">Archaeocommons</a></li>
</ul>
<!--break-->
All these projects seem to be at the startline, but they do have a solid background. Consider the Alexandria Archive, it's a non-profit consulting group that has taken part in the University of Chicago <a href="http://oi.uchicago.edu/OI/PROJ/XSTAR/XSTAR.html" title="XSTAR home page at University of Chicago" onclick="window.open(this.href); return false;"><acronym title="XML System for Textual and Archaeological Research">XSTAR</acronym> project</a>, one of the few cases in archaeology where XML has been used paying some attention to what it <em>really</em> is, producing some DTDs, and software.

The Archaeocommons home page looks a bit <em>static</em>, and it's not easy to understand what are the main purposes of the initiative, maybe that should be clearer.
