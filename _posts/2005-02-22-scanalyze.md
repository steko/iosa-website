---
layout: single
title: Scanalyze
author: steko
created: 1109063052
---
Scanalyze is an application for viewing, editing, aligning, and merging Laserscanner data. It has been developed and continuously improved from the Computer Graphics Laboratoryof the Stanford University since the 1990ties. You can process triangle meshes or range images encoded as rectangular arrays of points. 

Scanalyze is using his own file-format called `*.ply`, but there exist already data conversion modules for exemple for Cyberware, 3D Scanners Ltd., and Cyra Technologies scanners.  Scanalyze offers different capabilities of displaying, clipping and reducing polygon meshes, or filling of holes in range data.
