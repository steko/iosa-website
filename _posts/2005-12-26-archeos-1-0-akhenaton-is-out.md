---
layout: single
title: ArcheOS 1.0 "Akhenaton" is out!
author: steko
created: 1135615966
---

ArcheOS 1.0 "Akhenaton" is the first live bootable CD built with archaeology in mind. Version 1.0 is out now, thanks to the effort of the <a href="http://www.arc-team.com" title="Arc-Team Homepage [en,it.de]">Arc-Team</a>.

It is a GNU/Linux live distribution built for archaeological aims and based on PCLinuxOS.  It is also possible to install ArcheOs on your hardisk.

You can get it right <a href="http://www.spcnet.it/arc-team/"  title="ArcheOS download page">here</a> from the Arc-Team website, or from the mirror provided by <a href="http://www.linux.it"  title="Italian Linux Society home page">Italian Linux Society</a>, <a href="ftp://ftp.linux.it/pub/archeos/"  title="ArcheoOS download mirror on ftp.linux.it">here</a>.

ArcheOS is developed following the <acronym title="OPen ARChaeology">OpArc</acronym> project guidelines and it's released under the GNU <acronym title="General Public License">GPL</acronym>.<!--break-->
The applications included in ArcheOS are:
<ul>
<li>CAD
<ul><li>QCad</li></ul></li>
<li>DATABASE
<ul><li>PostgreSQL</li><li>PostGIS</li><li>Pgaccess</li></ul>
</li>
<li>GIS<ul><li>GRASS</li><li>QuantumGIS</li><li>SAGA</li><li>Jump</li></ul></li>
<li>GPS<ul><li>GPSDrive</li></ul></li>
<li>GRAPHIC<ul><li>Blender (3D)</li><li>The GIMP (2D advanced raster editing)</li><li>Inkscape (2D SVG vectors)</li><li>Sodipodi (2D SVG vectors)</li></ul></li>
<li>OFFICE<ul><li>OpenOffice.org</li></ul></li>
<li>PHOTOGRAMMETRY<ul><li>Stereo</li><li>e-foto</li></ul></li>
<li>STATISTIC<ul><li>R</li></ul></li>
</ul>
