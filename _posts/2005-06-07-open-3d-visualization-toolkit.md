---
layout: single
title: Open 3D Visualization Toolkit
author: steko
created: 1118166375
---
On this site you will find areas to learn more about visualization approaches, free software, a gallery of images created by the Science Museum of Minnesota and members of this site, pre-made textures, models, and scenes to use as building blocks for your own animations, and more, released under a Creative Commons license.
The 3D toolkit includes Blender.
