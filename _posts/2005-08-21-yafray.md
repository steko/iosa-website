---
layout: single
title: YafRay
author: steko
created: 1124657180
---
YafRay is a powerful raytracer, under the LGPL license. It enables you to create fantastic images and animations of a photorealistic quality.
