---
layout: single
title: JIIA & ADR
author: steko
created: 1114331740
---
The "Journal of Intercultural and Interdisciplinary Archaeology" (Acronym: JIIA) is an online serial publication on archaeology, antiquity sciences and archaeological applied sciences.
It is interdisciplinary and concentrates particularly on the problems of interculturality in the ancient world.
It uses scientific contributions offered freely by scholars from the university and scientific research world and organises the open-access dissemination of the research results.
