---
layout: single
title: UMN MapServer
author: steko
created: 1106415741
---
MapServer is an OpenSource development environment for constructing spatially enabled Internet-web applications. The software builds upon other popular OpenSource or freeware systems including Shapelib,  FreeType, Proj.4, GDAL/OGR. MapServer will run where most commercial systems won't or can't, on Linux/Apache platforms. MapServer is known to compile on most versions of UNIX/Linux, Microsoft Windows and even MacOS.
