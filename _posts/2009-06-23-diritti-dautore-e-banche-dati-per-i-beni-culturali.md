---
layout: single
title: Diritti d'autore e banche dati per i beni culturali
author: steko
created: 1245763710
---
Il giorno 7 maggio 2009 si è tenuto a Genova, presso l'aula magna della Facoltà di Lettere e Filosofia, il seminario sul tema “Diritti d'autore e banche dati per i beni culturali”, terzo ed ultimo appuntamento del ciclo di incontri: “Nuove ricerche e giovani ricercatori” organizzato, per il secondo anno consecutivo, dal <a href="http://www.grupporicerche.it/">grupporicerche</a> della sezione di Genova dell'<a href="http://www.iisl.it">Istituto Internazionale di Studi Liguri</a> (IISL).

Lo spunto per l'organizzazione di tale incontro, di interesse per molte figure professionali che operano nel settore dei beni culturali, è nato da alcune delle problematiche emerse nel corso del progetto IOSA, un progetto dello stesso grupporicerche, primariamente dedicato alla promozione e allo sviluppo del software libero e open source in ambito archeologico. Il progetto, che è andato maturando nel corso del tempo, è recentemente giunto ad affrontare problematiche quali la diffusione e l'utilizzo di formati e standard aperti per lo scambio dei dati, il miglioramento della circolazione del sapere tramite l'apertura delle banche dati e l'adozione del modello “Open Access”per la produzione scientifica.

Se, infatti, tecnicamente non esistono limiti all'adozione di un modello di sviluppo della ricerca che possa essere basato sulle caratteristiche sopra dette, notevoli limiti sono dati proprio dai  problemi di tipo legale, normativo e burocratico connessi alla proprietà e al diritto d'uso dei dati raccolti nelle ricerche archeologiche.

Dal progetto IOSA è nata così la necessità di rendere liberamente disponibili ed utilizzabili i dati derivanti dalle ricerche scientifiche o la necessità di utilizzare i dati pubblicati da altri ricercatori senza per questo violare alcun diritto degli stessi. Sulla scorta di quanto già - almeno in parte - fatto nell'ambito di alcuni workshop nazionali sul free software, si è deciso di organizzare una giornata dedicata esclusivamente alla suddetta problematica.

Un breve resoconto

Il seminario, aperto alla partecipazione di tutti gli interessati, ha visto la presenza di numerosi studenti delle università di Genova e Torino, e di alcuni professionisti del settore, direttamente toccati dai temi della discussione.  All'incontro, che ha avuto come moderatore <strong>Stefano Costa</strong> (coordinatore del progetto IOSA), sono stati invitati a partecipare:

- Gianluca Pesce, membro del grupporicerche e del progetto IOSA;
- Giulio Montinari, della <a href="http://archeoge.arti.beniculturali.it/">Soprintendenza per i Beni Archeologici della Liguria</a>;
- Federico Morando, del <a href="http://nexa.polito.it/">Centro NEXA</a> del Politecnico di Torino;
- Augusto Palombini, ricercatore CNR-ITABC e membro della <a href="http://www.archeologi-italiani.it/">Confederazione Italiana Archeologi</a>.


Grande spazio è stato dato alla discussione pubblica, che è stata avviata al termine delle relazioni e aperta a tutti i partecipanti. La forma poco convenzionale delle relazioni, che sono state spesso inframezzate dalle osservazioni dei diversi relatori e la tematica alquanto sentita ha permesso di toccare elevate punte di
partecipazione collettiva durante le quali il pubblico non solo ha posto domande ai relatori ma portato anche esempi concreti di esperienze vissute.

Entrando più nel dettaglio delle singole relazioni, è possibile sottolineare come <strong>Gianluca Pesce</strong> abbia cercato di introdurre le diverse problematiche legate al tema della giornata presentando le riflessioni emerse da un lavoro sperimentale volto a sviluppare il massimo grado di interazione tra software libero, standard informatici aperti realizzati apposta per il settore dei beni culturali e licenze libere. Tale sperimentazione, più in particolare, si è basata sull'implementazione di un database utile a raccogliere i dati provenienti da diversi tipi di schede ICCD di livello inventariale. Uno strumento, questo, sviluppato sulla scorta dell'esperienza
maturata in un più complesso progetto di schedatura svolto negli anni precedenti per conto della Direzione Regionale per i Beni Culturali e Paesaggistici della Liguria. Il lavoro si è concretizzato nello sviluppo di uno script, rilasciato sotto licenza libera GNU GPL, realizzato con il linguaggio standard ISO SQL92 per la definizione distrutture dati.

**Giulio Montinari** ha, invece, presentato la sua esperienza presso l'Ufficio Catalogo della Soprintendenza, mostrando nel dettaglio il sistema informativo utilizzato per la gestione territoriale di tutti i beni catalogati, interamente basato su software libero. Il sistema è attualmente disponibile solo per il personale della soprintendenza tramite la rete Intranet, ma Montinari non ha escluso che nel prossimo futuro una parte dei dati possano essere liberamente consultabili, compatibilmente con le esigenze di tutela.

**Federico Morando** ha presentato una breve ma efficace panoramica sul diritto d'autore in ambito digitale, soffermandosi in particolare sulle possibilità offerte dalle licenze Creative Commons. Sullo specifico tema delle banche dati, ha inoltre illustrato i progressi ottenuti nell'ambito dell'iniziativa Science Commons per la libera
circolazione di dati scientifici. Infine ha evidenziato come la problematica principale sia nella individuazione dei titolari dei diritti. In particolare, nell'ambito lavorativo ed editoriale, taluni contratti possono prevedere la cessione di tutti i diritti di sfruttamento economico al committente, impedendo di fatto agli autori degli studi di far circolare liberamente le proprie scoperte.

**Augusto Palombini** ha approfondito la tematica dell'accesso alle banche dati e agli archivi e magazzini statali che custodiscono beni culturali, evidenziando la discrezionalità che è di fatto consentita al funzionario nel concedere o meno tale accesso a studenti e ricercatori in genere. Sotto diversi profili legislativi ci sono
motivate ragioni perché questo accesso sia consentito dietro presentazione di una opportuna richiesta. La Confederazione Italiana Archeologi ha predisposto un modello di questa richiesta che verrà presto messo a disposizione per tutti coloro che avranno necessità di effettuare tali richieste.

Durante la discussione finale, molto partecipata, sono stati ulteriormente esaminati:

- i problemi legati ai metodi tradizionali di pubblicazione rispetto a quelli resi disponibili dalle tecnologie della rete;
- la scarsa chiarezza relativa alla cessione di alcuni diritti in ambito contrattuale;
- i timori in larga parte ingiustificati da parte dei detentori di banche dati a renderle disponibili;
- i vantaggi offerti dalla circolazione libera di dati e pubblicazioni nell'ambito della ricerca;

Come è stato chiaramente espresso in apertura di seminario, gli sforzi svolti per ottenere un riconoscimento dei diritti d'autore non sono stati pensati per consentire di “sigillare” la conoscenza, perpetrando la situazione attuale, ma al contrario sono mirati a favorire una sempre maggiore circolazione delle informazioni – in tutte le forme disponibili oggi e in futuro – in grado di creare circoli virtuosi di condivisione del sapere e nuovi percorsi di scoperta e ricerca, come è ormai ampiamente dimostrato da quei settori che già stanno percorrendo questa via.
