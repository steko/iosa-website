---
layout: single
title: QuantumGIS
author: steko
created: 1106764574
---
Quantum GIS (QGIS) is a Geographic Information System (GIS) built for Linux/Unix, that works also on Windows and MacOS X. QGIS supports vector, raster, and database formats. QGIS is licensed under the GNU Public License.

Some of the major features include:
<ol>
   <li>Support for spatially enabled PostGIS tables</li>
   <li>Support for shapefiles, ArcInfo coverages, Mapinfo, and other formats supported by OGR</li>
   <li>Raster support for a large number of formats</li>
   <li>Identify features</li>
   <li>Display attribute tables</li>
   <li>Select features</li>
   <li>GRASS Digitizing</li>
   <li>Feature labeling</li>
</ol>
