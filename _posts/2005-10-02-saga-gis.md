---
layout: single
title: SAGA GIS
author: steko
created: 1128281521
---
SAGA  – System for Automated Geoscientific Analyses- is a hybrid GIS software. The first objective of SAGA is to give (geo-)scientists an effective but easy learnable platform for the implementation of geoscientific methods, which is achieved by SAGA's unique Application Programming Interface (API). The second is to make these methods accessible in a user friendly way. This is mainly done by the Graphical User Interface (GUI). Together this results in SAGA's true strength: a fast growing set of geoscientifc methods, bundled in exchangeable Module Libraries.
