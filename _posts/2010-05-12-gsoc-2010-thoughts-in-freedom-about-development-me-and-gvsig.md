---
layout: single
title: ! 'GSOC 2010: thoughts in freedom about development. Me & gvSIG.'
author: luca
created: 1273676685
---
It's time to get into this new adventure and sailing towards new lands! I've been accepted as student for the Google Summer of Code 2010, and with me the IOSA team.

The project is really interesting (giving support to SqLite and SpatiaLite) and gvSig is related, not less then other gis, to archaeological researches.

It's quite scaring thinking about the huge quantity of work it needs for fullfilling my task and at the same time it's really funny and amazing realising that together with the sweat of coding we'll get for sure a lot of fun!

The gvSig team that helps me with the project looks like being very pleasant and nice.

For mantaining some trace of the steps I'm going to follow and keeping a rough documentation of the most interesting links and approaches and codes I find all around this period of work I've decided to write some posts on Iosa.it, not to forget this fantastic adventure I'm going to begin.

