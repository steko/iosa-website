---
layout: single
title: Interested in open science?
author: steko
created: 1220876661
---
<span style="color:#aaa; font-size: 0.8em">from the <a href="http://sciencecommons.org/weblog/archives/2008/09/05/interested-in-open-science/">Science Commons Blog</a>:</span>

<blockquote>If so, there’s a <strong><a href="http://lists.okfn.org/cgi-bin/mailman/listinfo/open-science">new discussion list</a></strong> you may want to join, brought to you by the good folks at the <a href="http://www.okfn.org/">Open Knowledge Foundation</a>.

Writes OKF’s Jonathan Gray:

<blockquote>As far as we could tell, there wasn’t a general mailing list for people interested in open science. Hence the new list aims to cover this gap, and to strengthen and consolidate the open science community.

    We hope it will be a relatively low volume list for relevant announcements, questions and notes. We also hope to get as full as possible representation from the open science community — so please forward this to anyone you think might be interested to join!</blockquote>
</blockquote>
