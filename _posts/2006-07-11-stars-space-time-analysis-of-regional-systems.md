---
layout: single
title: STARS - Space-Time Analysis of Regional Systems
author: giofish
created: 1152603016
---
What is STARS ? - Space-Time Analysis of Regional Systems (STARS) is an open source package designed for the analysis of areal data measured over time. STARS brings together a number of recently developed methods of space-time analysis into a user-friendly graphical environment offering an array of dynamically linked graphical views. It is intended to be used as an exploratory data analysis tool. STARS can also be used from the command line to support more flexible and specialized types of analyses by advanced users. As such STARS should appeal to a wide array of users. Written entirely in Python, STARS is crossplatform and easy to install (and expand). For more informatio you can see at url: <a href="http://regal.sdsu.edu/index.php/Main/STARS" target="_blank">http://regal.sdsu.edu/index.php/Main/STARS</a>
