---
layout: single
title: OpenScience
author: steko
created: 1107037056
---
The OpenScience project is dedicated to writing and releasing free and Open Source  scientific software. It's a group of scientists, mathematicians and engineers who want to encourage a collaborative environment in which science can be pursued by anyone who is inspired to discover something new about the natural world.

Much of the work of science depends on having appropriate tools available to analyze experimental data and to interract with theoretical models. Powerful computers are now cheap enough so that significant processing power is within reach of many people. The missing piece of the puzzle is software that lets the scientist choose between models and make sense of his or her observations. That is where the OpenScience project can help. 
