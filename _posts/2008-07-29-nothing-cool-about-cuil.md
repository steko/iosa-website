---
layout: single
title: Nothing cool about cuil
author: steko
created: 1217338315
---
Lots of rumors these days about the new hyper-tastic search engine named cuil.

So after having read some bad reviews about it I decided to give it a spin using my all-times favourite query, i.e. “open source archaeology”. Here's the result:

<img src="http://www.iosa.it/www/files2/CUIL.png" />

It's really amazing! The title and the link point to iosa.it, but the long-text (one of the most praised innovations of cuil vs google) is from https://launchpad.net/openarchaeology and the image on the right is a really cool wood-made man wearing an OSGeo t-shirt... but again not from iosa.it.
