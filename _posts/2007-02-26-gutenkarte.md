---
layout: single
title: Gutenkarte
author: steko
created: 1172503072
---
**Gutenkarte** is a geographic text browser, intended to help readers explore the spatial component of classic works of literature. Gutenkarte downloads public domain texts from <a href="http://www.gutenberg.org/" title="Project Gutember Home Page">Project Gutenberg</a>, and then feeds them to MetaCarta's GeoParser API, which extracts and returns all the geographic locations it can find. Gutenkarte stores these locations in a database, along with citations into the text itself, and offers an interface where the book can be browsed by chapter, by place, or all at once on an interactive map. Ultimately, Gutenkarte will offer the ability to annotate and correct the places in the database, so that the community will be able construct and share rich geographic views of Project Gutenberg's enormous body of literary classics.

This is a very good example of putting together different sources that are freely available (most are in the public domain) building something new that actually helps scholars. IT hasn't to be necessarily hi-tech hype to become useful.

Gutenkarte accomplishes all this with the help of several notable pieces of Free and Open Source software, including PostGIS, MapServer, GDAL/OGR, and Python.
