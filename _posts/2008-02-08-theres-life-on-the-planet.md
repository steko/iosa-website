---
layout: single
title: There's life on the Planet!
author: steko
created: 1202508137
---
<p>Thanks to <a href="http://homepages.nyu.edu/~te20/">Tom Elliot</a>, now we have two <span style="text-decoration: line-through">planets</span> aggregators for a number of ancient world and digital archaeology blogs at http://planet.atlantides.org/ Tune your RSS readers!</p>
<p>But there's more: also iosa.it is featured on the Maia Atlantis aggregator. We hope to bring some free software and open access on this new planet, so to make it as good as possible for our readers.</p>
