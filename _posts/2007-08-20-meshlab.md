---
layout: single
title: MeshLab
author: steko
created: 1187633293
---
<p><strong>MeshLab</strong> is an open source, portable, and extendible system for the processing and editing of unstructured 3D triangular meshes. <br />The system is aimed to help the processing of the typical not-so-small unstructured models arising in 3D scanning, providing a set of tools for editing, cleaning, healing, inspecting, rendering and converting this kind of meshes.<br />The project is supported by the European Network of Excellence <a href="http://www.epoch-net.org/">Epoch</a> and <a href="http://www.aimatshape.net/">Aim@Shape</a>.</p>
