---
layout: single
title: Walldrawer
author: steko
created: 1106938749
---
Walldrawer - an easy alternative to on-site wall drawing for archaeology. This software aims at putting together the tools needed to produce scaled, rectified, drawing-like pictures from digital photographs in archaeological excavations.
This software was developed in Java using Java Advanced Imaging toolkit, but at the moment development is stopped. Functionality is not 100% however try using it, it is worth.
