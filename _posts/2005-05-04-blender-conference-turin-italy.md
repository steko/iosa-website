---
layout: single
title: Blender Conference, Turin, Italy
author: steko
created: 1115189751
---
It seems to me I'm always late... I missed another important piece of news.
On May, 3rd, at the Architecture University of Turin, Italy, a Blender Conference was held, focused about the relationship about Blender and architecture.
You can find [the original announce](http://www.blender.org/modules.php?op=modload&name=News&file=article&sid=213&mode=thread&order=0&thold=0 "Blender.org website").
I hope there will be some documentation about this conference, as it would be very useful for archaeological purposes IMO.
