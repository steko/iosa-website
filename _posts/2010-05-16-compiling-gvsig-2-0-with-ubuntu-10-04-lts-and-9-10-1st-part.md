---
layout: single
title: ! 'Compiling gvSIG 2.0 with Ubuntu 10.04 LTS (& 9.10) (1st part) '
author: luca
created: 1274045227
---
CONFIGURE JAVA
It needs having installed on your system a Sun Java 5 compatible compiler.
You can install it parallel to the usual open-jdk, usually already present on your machine.
Check that's not already on your machine for any reason with: 

`java -version`

If you work with Lucid Lynx: go to your sources.list file and uncomment:

```
deb http://archive.canonical.com/ubuntu lucid partner
deb-src http://archive.canonical.com/ubuntu lucid partner
```

You have to follow some easy steps:

`sudo apt-get install sun-java6-jdk`
`sudo update-alternatives --config java` and chose the sun-java one 
Set two values into your `~/.bash_profile` or your `~/.bashrc` (according to your system you could find any of them) :

- `export JAVA_HOME=<path to the folder of your jdk>` (ex. )
- `export PATH=$PATH:<path to the bin folder within your jdk's dir>` (ex. )

CONFIGURE MAVEN

- create an " .m2/ " folder within your home directory
- create a " repository/ " folder within the .m2/ folder you've just created
- create a text file called " settings.xml " in the .m2/ folder you've created within your home/ directory with the following content:

   native_platform=linux
   native_distribution=Ubuntu-9.10
   native_compiler=gcc4
   native_arch=i386
   native_libraryType=dynamic
   export native_classifier=${native_platform}-${native_distribution}-${native_compiler}-${native_arch}-${native_libraryType   }

Well, you should change each value with the one corresponding to your machine's values!

INSTALL LIBRARIES

Install last JNI libraries and dependencies downloading version of org.gvsig.maven.base and executing `mvn install` from the folder `org.gvsig.maven.base/`.

CONFIGURE ECLIPSE's WORKSPACE AND COMPILING a GVSIG
Set the following properties:

- Window > Preferences: General > Workspace > Text file encoding = ISO-8859-1
- Window > Preferences > Java > Compiler > Compiler Compliance Level = 1.5 (or 5.0)
You must have the official java sun jre 1.5 compatible (that means 1.6 or > but with 1.5 compliance according to Java compiler)
- Window > Preferences > General > Network Connection set up to Manual or System
- download last gvSIG from https://svn.forge.osor.eu/svn/gvsig-desktop with Window > Open Perspective > Other : SVN Respository Exploring
- Window > Show view > ant
- execute the build.xml via ant perspective, following commands:
`mvn-install
mvn-configure-eclipse-workspace`
- Exit Eclipse and open it again
- Window > Preferences : Java > Build Path > Classpath Variables `M2_REPO = [USER_HOME]/.m2/repository`

Execute build/projects/gvsig-standard/build.xml from ant perspective:

    svn.checkout.all (it downloads and compiles a gvSIG standard version)

If everything goes right, after having got BUILD SUCCESSFULL, with build project selected click on run java application. You will be able to select which platform to chose for your gvSIG.


REFERENCES

- <http://www.greatwhiteit.com/web/guest/technology-blog/-/blogs/how-to-install-oracle-sun-java-jdk-and-jre-in-ubuntu-10-04-lucid-lynx;jsessionid=fd2e50665d22d051fea12f928c55?_33_redirect=%2Fweb%2Fguest%2Ftechnology-blog%3Fp_p_id%3D58%26p_p_lifecycle%3D0%26p_p_state%3Dnormal%26p_p_mode%3Dview%26p_p_col_id%3Dcolumn-2%26p_p_col_pos%3D1%26p_p_col_count%3D4%26_58_struts_action%3D%252Flogin%252Fforgot_password>
- <http://www.gvsig.org/web/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/trabajar-con-el-nucleo-de-gvsig/gvsig-compilation/initial-configuration/initial-configuration>
- <http://www.gvsig.org/web/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/trabajar-con-el-nucleo-de-gvsig/gvsig-compilation/jni-compilation>
- <http://www.gvsig.org/web/>
- <http://www.gvsig.org/web/projects/gvsig-desktop/docs/devel/gvsig-devel-guide/2.0.0/trabajar-con-el-nucleo-de-gvsig/gvsig-compilation/how-to-compile-gvsig>
