---
layout: single
title: WW2D
author: steko
created: 1128250674
---
WW2D is cross-platform, free and open-source version of NASA World Wind software, see http://worldwind.arc.nasa.gov for details. <strong>WW2D</strong>allows you to explore Earth using satellite imagery, topographic maps and image from other data sources also providing large placenames and boundaries database and allowing you to install community-made add-ons for even more information about our planet.<br></p><p align="justify">In basic configuration <strong>WW2D</strong> uses images from <a href="http://earthobservatory.nasa.gov/Newsroom/BlueMarble/">Blue Marble</a> (1 km/pixel), <a href="http://onearth.jpl.nasa.gov/">LandSat7</a> (15 m/pixel), USGS Topo Maps, USGS Digital Ortho imagery, USGS Urban Area imagery.<br></p><strong>WW2D</strong> is designed to dynamically download needed data from internet, however you can download data you want for faster access and offline usage.
