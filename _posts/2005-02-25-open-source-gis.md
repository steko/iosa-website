---
layout: single
title: Open Source GIS
author: steko
created: 1109368105
---
This site represents an attempt to build a complete index of Open Source / Free GIS related software projects. The effort has some way to go, especially for projects in languages other than English. The definition of GIS has been kept loose to encompass a broad range of projects which deal with spatial data.
