---
layout: single
title: ! '3D Bridge Open Source Software: Open source applications in Seminaarinmäki
  project'
author: steko
created: 1144943442
---
This site presents the open source tools used and developed for visualisation of culture heritage. The flow starts with IDA which is a documentation tool. With it the material can be organised and the reconstruction process could be documented. Visualisation is made with Inkscape and Blender. The visualisation is shown by using application that renders the view with OpenSceneGraph.
