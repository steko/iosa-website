---
layout: single
title: GDF Hannover
author: steko
created: 1106936958
---
GDF Hannover (Gesellschaft für Datenanalyse und Fernerkundung) offers solutions for spatial data analysis and remote sensing. As an innovative and competent consulting company, they focus particularly on the development of new methods of geo data analysis. Their special fields of work are remote sensing (aerial photointerpretation and satellite remote sensing) and data modeling as well as GIS training courses.

Here you can order updated manuals both in English and German for GRASS GIS.
