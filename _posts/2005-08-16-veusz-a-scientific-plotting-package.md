---
layout: single
title: Veusz - A Scientific Plotting Package
author: steko
created: 1124211126
---
Veusz is a scientific plotting package written in Python. It uses PyQt (Wiki) and Numarray. Veusz is designed to produce publication-ready Postscript output.

Veusz provides a GUI, command line and scripting interface (based on Python) to its plotting facilities. The plots are built using an object-based system to provide a consistent interface.

Features include:
<ul>
<li>X-Y plots (with errorbars)</li>
<li>Images (with colour mappings)</li>
<li>Stepped plots (for histograms)</li>
<li>Line plots</li>
<li>Function plots</li>
<li>Stacked plots and arrays of plots</li>
<li>Plot keys</li>
<li>Plot labels</li>
<li>LaTeX-like formatting for text</li>
<li>EPS output</li>
<li>Simple data importing</li>
<li>Scripting interface</li>
<li>Save/Load plots</li>
<li>Dataset manipulation</li>
<li>Embed Veusz within other programs</li>
</ul>
