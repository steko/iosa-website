---
layout: single
title: ! 'Open Source Geospatial ''05 Conference: GRASS 6 workshop material'
author: steko
created: 1124659302
---
Workshop material by Markus Neteler on http://mpa.itc.it/markus/osg05/ , A tutorial (both in english and spanish) and the slides are available for download, together with the usual Spearfish demo dataset and some extra maps. Be sure to grab it!
