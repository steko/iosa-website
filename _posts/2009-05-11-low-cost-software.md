---
layout: single
title: Low cost software ???
author: steko
created: 1242035736
---
Start reading here http://www.iris.ba.cnr.it/Corso.html “Corso di formazione "Low cost software & Cultural Heritage” (12 maggio - 9 giugno)” organized by an office of the Italian National Council of Research (CNR). I stumbled upon that from <a href="http://fradeve.netsons.org/2009/05/openstreetmap-e-itc-cnr-di-bari-convivenza/">here</a>.
Then, remind yourself that just <em>two</em> weeks ago we had our <em>fourth</em> workshop on “Free software, open source and open formats nei processi di ricerca archeologica”, organized by another CNR team in Rome.
Eventually, ask yourself what does that mean: are we the “free software archaeologists” so bad at disseminating our knowledge and ideas? Is it really possible that someone in 2009 (it is two-thousand-and-nine) still thinks that open source = low cost? After so many years spent talking about the actual benefits of going free-as-in-freedom with software, and also about the fact that, it's true, there are no license costs, but you have to put your money where your mouth is and start investing your resources on free software? And, still, how can you be so unaware to be writing in the same sentence “open source” and “freeware”?
Really.
