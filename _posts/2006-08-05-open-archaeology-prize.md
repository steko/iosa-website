---
layout: single
title: Open Archaeology Prize
author: steko
created: 1154782322
---

The <a href="http://www.alexandriaarchive.org/">Alexandria Archive Institute (AAI)</a>, a nonprofit organization dedicated to promoting and developing open resources of world cultural heritage, has just announced an “Open Archaeology prize”.
