---
layout: single
title: ImageMagick
author: steko
created: 1174649976
---
ImageMagick® is a software suite to create, edit, and compose bitmap images. It can read, convert and write images in a variety of formats (about 100) including DPX, GIF, JPEG, JPEG-2000, PDF, PhotoCD, PNG, Postscript, SVG, and TIFF. Use ImageMagick to translate, flip, mirror, rotate, scale, shear and transform images, adjust image colors, apply various special effects, or draw text, lines, polygons, ellipses and Bézier curves.

<h4>Features and Capabilities</h4>

<p>Here are just a few examples> of what ImageMagick can do:</p>
<ul>
  <li>Format conversion: convert an image from one format to another (e.g. PNG to JPEG)</li>
  <li>Transform: resize, rotate, crop, flip or trim an image</li>
  <li>Transparency: render portions of an image invisible</li>
  <li>Draw: add shapes or text to an image</li>

  <li>Decorate: add a border or frame to an image</li>
  <li>Special effects: blur, sharpen, threshold, or tint an image</li>
  <li>Image calculator: apply a mathematical expression to an image or image channels</li>
  <li>Text &amp; comments: insert descriptive or artistic text in an image</li>
  <li>Image identification: describe the format and attributes of an image</li>

  <li>Animation: create a GIF animation sequence from a group of images</li>
  <li>Composite: overlap one image over another</li>
  <li>Montage: juxtapose image thumbnails on an image canvas</li>
  <li>Large image support: read, process, or write mega- and giga-pixel image sizes</li>
</ul>


