---
layout: single
title: Open Geospatial Consortium
author: steko
created: 1107091650
---
The Open Geospatial Consortium, Inc. (OGC) is a non-profit, international, voluntary consensus standards organization that is leading the development of standards for geospatial and location based services. Through our member-driven consensus programs, OGC works with government, private industry, and academia to create open and extensible software application programming interfaces for geographic information systems (GIS) and other mainstream technologies. Adopted specifications are available for the public's use at no cost. 
