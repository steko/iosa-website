---
layout: single
title: Recent News in Open Access Archaeology
author: steko
created: 1229429157
---
Lots of archaeological news these days in <a href="http://www.earlham.edu/~peters/fos/fosblog.html">Open Access News</a>, the primary collector of Open Access related information and discussion.
This is probably a direct consequence of the rapid advancements in the field. My main hope now is that established journals begin moving to OA, especially academy-based ones (i.e. journals that are not tied to a publisher but are published directly by a research institution).
<!--break-->
<dl>
<dt><a href="http://www.earlham.edu/~peters/fos/2008/12/presentations-from-archaeology.html">Presentations from archaeology conference</a></dt>
<dd>The presentations from Web 2.0 and Beyond: New Tools for Archaeological Collaboration and Communication are now online.</dd>
<dt><a href="http://www.earlham.edu/~peters/fos/2008/12/oa-index-on-archaeology-and-early.html">French OA index on archaeology and early history</a></dt>
<dd>DAPHNE (Données en Archéologie, Préhistoire et Histoire sur le NEt - Data in Archeology, Prehistory and History on the NEt) is a new OA index of metadata, launched in November 2008.</dd>
<dt><a href="http://www.earlham.edu/~peters/fos/2008/12/oa-dataset-from-archaeology-textbook.html">OA dataset from archaeology textbook</a></dt>
<dd>The Quantitative Archaeology Wiki has posted the dataset from the textbook Digging Numbers by Mike Fletcher and Gary Lock, along with a tutorial of using the statistical software R to analyze the data. (OK, I already knew this one)</dd>
</dl>
