---
layout: single
title: Workshop "Open Source, Free Software e Open Formats nei processi di ricerca
  archeologica"
author: steko
created: 1136237806
---
This workshop aims at putting together all archaeologists (actually, just from Italy) that would like to discuss about the adoption, use and development of Open Source Software, and its even more important counterpart: Open File Formats.
Promoted by the ASIAA Lab, this meeting will take place in Grosseto. <a href="/node/181" title="This event's page on iosa.it">Read more about this event</a>.
