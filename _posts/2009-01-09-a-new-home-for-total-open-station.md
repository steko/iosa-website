---
layout: single
title: a new home for Total Open Station
author: steko
created: 1231528052
---
We have moved the development infrastructure of <strong><a href="http://tops.berlios.de/">Total Open Station</a></strong> from <a href="http://sharesource.org">Sharesource</a> to <a href="http://developer.berlios.de/">Berlios</a>. Berlios gives us more control on website and repositories administration, and it offers both Mercurial (which is what we're using now) and git (which is what we will be using in the future) hosting.
Despite the lack of news, Total Open Station's development is going on. An alpha quality 0.1 release is expected soon, and after that the whole program will undergo a major rewrite to better suit what we have learned about total stations during the past 11 months (yes, it's just 11 months ago that I had this crazy idea and I''m still surprised by the fact that it works so well even at this development stage).
If you own a total station and want to feel <em>free</em> of using it anywhere, just contact us and send us some sample raw data from your device.
