---
layout: single
title: ! 'Open Data Licensing: an OpenStreetMap point of view'
author: steko
created: 1199967637
---
The Opengeodata blog has an interesting post about choosing the right license for geodata produced by open data lovers like the OSM project is: <a href="http://www.opengeodata.org/?p=262">http://www.opengeodata.org/?p=262</a>.
First point there is recognizing that all geodata is subject to database right where this exists (EU), but it's quite unclear in the US, for example.
The second notable argument is the distinction between the scientific world, where <em>social</em> (vs <em>legal</em>) rules of <q>attribution</q> are well established, and other fields like geodata, where a rigorous <em>legal</em> attribution is to be preferred. Thus, public domain for scientific data may be a good choice, but it isn't for geodata.
Archaeology of course falls in the first category...
