---
layout: single
title: Repast Agent Simulation Toolkit
author: steko
created: 1158232035
---
 The Recursive Porous Agent Simulation Toolkit (Repast) is one of several agent modeling toolkits that are available. Repast borrows many concepts from the Swarm agent-based modeling toolkit. Repast is differentiated from Swarm since Repast has multiple pure implementations in several languages and built-in adaptive features such as genetic algorithms and regression. For reviews of Swarm, Repast, and other agent-modeling toolkits, see the 2002 survey by Serenko and Detlor, the 2002 survey by Gilbert and Bankes, and the 2003 toolkit review by Tobias and Hofmann.  Repast is a free open source toolkit that was originally developed by Sallach, Collier, Howe, North and others. Repast was created at the University of Chicago. Subsequently, it has been maintained by organizations such as Argonne National Laboratory. Repast is now managed by the non-profit volunteer Repast Organization for Architecture and Development (ROAD). ROAD is lead by a board of directors that includes members from a wide range of government, academic and industrial organizations. The Repast system, including the source code, is available directly from the web.  Repast seeks to support the development of extremely flexible models of living social agents, but is not limited to modeling living social entities alone.
