---
layout: single
title: The archaeology of open source software in archaeological research
author: steko
created: 1269014709
---
In 2010, using and developing free and open source software for archaeological research is not interesting news: lots of us do that nowadays, and the quality and quantity of available software and programming libraries is something not questionable. But was it the same 5 years ago ? It was very different, believe me. In 2005 the IOSA project was less than one year old, GRASS GIS 6.0 beta was right there and it looked to us like just having a human graphical interface to a free GIS program would help solving any problem. Ubuntu Linux was just a Warty Warthog. But this is history.
What I'm going to write today is instead the <em>archaeology</em> of free and open source software in archaeology. A few weeks ago I found two unrelated items that will fit perfectly in such an archaeological study.

## Archaeology and GIS &mdash; the Linux way
[This article from Linux Journal](http://www.linuxjournal.com/article/2983) by R. Joe Brandon, Trevor Kludt and Markus Neteler dates back to 1 July 1999. It explains in detail a survey project carried by the University of Texas - El Paso in western Texas and southern New Mexico, that took advantage of GRASS GIS from the early stages of research. In the authors' words:

> In the end, we accomplished what had at times seemed impossible. We are convinced the powerful scripting and developmental tools available under Linux, coupled with the sophisticated GIS routines available under GRASS, enabled us to successfully tackle the difficult and interesting challenges encountered during our project. Equally important, we were able to complete this project on time and under budget while creating one of the most dynamic spatial archaeological databases anywhere. In retrospect, we realized that even with a greater budget, we could not have fared better than we did utilizing the strengths of Linux and GRASS.

Nowadays GRASS GIS is used by a large number of archaeological research projects all over the world, there are two archaeologists in the main development team and the free GIS ecosystem has grown dramatically, to the point where GRASS GIS can be used just when its powerful analytical capabilities are required.

## Nearest neighbors

I came across this second item while searching for "archaeology" on Google Code Search (a very bad idea, indeed: lots of programmers user this term exactly to mean looking into old, perhaps obscure code). The <a href="http://www.staff.uni-marburg.de/~germano/guido/archaeology/">Archaeophysical tools</a> are basically two small programs, one in C++ and the other in Perl, that perform dedicated tasks.
The <a href="http://www.staff.uni-marburg.de/~germano/guido/archaeology/nn.html">Nearest neighbour analysis</a> tool is the most interesting. It dates to 5 February 2002 and is just a few dozen lines of C++ code, but you really should appreciate at least 3 facts about this program:

1. it is a compact, single-file program, intended for performing a single and well defined task
2. 8 years after, you can get the code, apply a trivial patch and it will compile perfectly (just add `#include <cstdlib>` at the beginning of the file to make `g++` 4.4 happy)
3. it is released under the GNU GPL: even if I didn't know Augusto Palombini (who is instead a good friend of mine) I could take it and do whatever I want with it

These two are of course just examples, and I am quite sure there are lots of them. Were you using or developing free and open source software more than 8 years ago? Let us know!
