---
layout: single
title: Open Access News
author: steko
created: 1126219182
---
The Open Access News Blog, mantained by Peter Suber. is a primary resource about the Open Access movement, updated every day with lots of news from all over the world. The main purpose of Suber is to promote Open Access for science and scholarship literature, as said in a statement "<em>Scholarly literature ought to be free and online</em>".
There is also a newsletter to keep you up-to-date and discover in its depth the Open Access movement and its struggle for freedom.
