---
layout: single
title: IOSA on Newsforge
author: steko
created: 1152217548
---
Some days ago Newsforge published an article by Marco Fioretti entitled [Uncovering FOSS in archaeology](http://trends.newsforge.com/trends/06/06/22/1714202.shtml?tid=138&tid=150), which deals quite specifically with a great number of themes that we are discussing and studying.

The Grosseto workshop was covered, too, and of course IOSA is mentioned as well as other good friends of ours: Arc-Team, ASIAA Lab.

If some of our readers find it worthy, it would be nice to have some more comments on the Newsforge page, to show there's some interest.
