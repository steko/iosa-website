---
layout: single
title: See you in Athens
author: steko
created: 1225227158
---
Just a quick note before flying to Athens. I'll be there with Elisa from tomorrow until November, 4th to attend the international conference “Digital Heritage in the new knowledge environment: shared spaces & open paths to cultural content” that was organized by the Hellenic Ministry of Culture and was covered earlier on iosa.it and other archaeoblogs.

I'm going to give a talk (also on behalf of professor Enrico Zanini, blocked in Italy) about “Sharing knowledge in archaeology: looking forward the next decade(s)” which focuses on the issus posed by the current knowledge transmission system and tries to investigate one possible approach using wikis as rich, multivocal environments. Wikis can contain almost anything, from “raw” data to subjective feelings, videos, pictures, drawings, tables, hyperlinks and are part of the Web 2.0 era.

Slides are available <a href="http://steko.iosa.it/files/athens.pdf">here</a> for those who won't be in Athens.

I'll try to post some update from the conference venue if wifi will be available as promised. Otherwise, see you there on Friday morning.
