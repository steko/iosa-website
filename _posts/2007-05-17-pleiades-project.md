---
layout: single
title: Pleiades Project
author: steko
created: 1179413862
---
<p>Pleiades is an international research community, devoted to the study of ancient geography, organized by the Ancient World Mapping Center at the University of North Carolina at Chapel Hill, U.S.A. The Pleiades project is open and aims at bringing together a global community of scholars, students and enthusiasts.</p>
<p>Their <a href="http://pleiades.stoa.org/about-pleiades"><q>about</q></a> page says:
<blockquote><em>The Pleiades web portal is being built atop the open-source <a href="http://plone.org/"><strong>Plone Content Management System</strong></a>, with the addition of a number of plugin components (Plone "Products"). All modifications and special-purpose plugins developed by the Pleiades Community will be released to the public for free re-use under compatible, open-source licensing.</em></blockquote>
It's encouraging to see that such a large project benefits from free software and, at the same time, gives back to the community.</p>
<p>It would be great if also Pleiades data were available under a free license.</p>
