---
layout: single
title: New Translation feature for Quantitative Archaeology Wiki
author: steko
created: 1220876379
---
Last week I took some time to add a new feature to the <a href="http://wiki.iosa.it/">Quantitative Archaeology Wiki</a>, namely the Translation plugin. This allows for easy translation of the existing content while preserving the existing page names and structure. Just use the translation toolbar at top-right on each page, following the guidelines at http://wiki.iosa.it/wiki:translation .

For now I have added the following languages: German (de), Spanish (es), French (fr) and Italian (it). If you think you need more languages, just let me know.

I know there aren't many contributions to the wiki but the number of readers is always increasing so maybe translating some of the pages could be useful for making content more available.
