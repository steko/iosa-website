---
layout: single
title: The STOA Consortium
author: steko
created: 1126815294
---
The STOA Consortium is a blog-based web portal whose goal is "Serving news, projects, and links for digital classicists everywhere" as said in their mission statement. Stolen from their about page, they intend:
<ul type="square">
<li> to foster a new style of refereed scholarly publications in the humanities not only of interest to specialists but also -- and just as importantly -- accessible by design and choice of medium to wide public audiences.</li>
<li> to develop and refine new models for scholarly collaboration via the internet.</li>
<li> to help insure the long-term interoperability and archival availability of electronic materials.</li>
<li> to support resolutions to copyright and other issues as they arise in the course of scholarly electronic publication.</li>
</ul>
It seems great, and because I just love back-linking, I put a link here so that everyone can come and see. Wait, these are <em>not our</em> goals, but we believe that such things are very important too and I'd like to thank people at STOA Consortium for their work.
