---
layout: single
title: JIIA Eprints Repository
author: steko
created: 1144306419
---
The new EPrints repository http://eprints.jiia.it/ linked to <acronym title="Journal of  Intercultural and Interdisciplinar Archaeology">JIIA</acronym> & <acronym title="Archaeological Disciplinary Repository">ADR</acronym>, is now available. In a few weeks the old MyOPIA repository will be discarded and all the e-prints will be put into the new GNU Eprints software.

JIIA Eprints is registered in the following listings:
<ul><li>http://www.eprints.org/software/archives/</li>
<li>http://www.openarchives.org/Register/BrowseSites</li>
<li>http://re.cs.uct.ac.za/</li></ul>

