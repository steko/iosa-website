---
layout: single
title: Using R for statistical analysis of archaeological objects
author: steko
created: 1259152645
---
The latest issue of <a href="http://soi.cnr.it/~archcalc/">“Archeologia e Calcolatori”</a> has an interesting paper by Mike Baxter and Hilary Cool about the statistical analysis of some loomweights from Pompeii (the paper is <a href="http://www.progettocaere.rm.cnr.it/databasegestione/open_oai_page.asp?id=oai:www.progettocaere.rm.cnr.it/databasegestione/A_C_oai_Archive.xml:484" title="">available</a> from the OAI repository of the journal) Besides being an interesting example of how clever data analysis of 
<blockquote><em>apparently mundane items can provide useful information about the people who made and used them if analysed appropriately</em></blockquote>
the paper has an extended appendix about the computational details of their study.
They start by stating that
<blockquote><em>R is a powerful open source statistical software system. Open source software is of increasing interest in archaeology (Pescarin 2006); apart from being free, R has the additional advantages that many statisticians would regard it as “state-of-the-art”, and it is regularly updated.
</em></blockquote>
and then provide details about the libraries and commands used for creating each data plot and to perform each analysis. It's a wonderful example of the benefits archaeologists could gain by choosing well established free software tools and clearly defined analysis processes, not to mention the advantages for those who are still at the beginning of their studies and try to learn from the basics.
Now I just wonder whether the authors know our <a href="http://wiki.iosa.it/">Quantitative Archaeology Wiki</a>, and how much it would take to create a tutorial there based on their paper. It would be a nice option for a short course in archaeological data analysis.
