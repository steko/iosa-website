---
layout: single
title: Archaeological resarch is poor, why make it poorer?
author: luca
created: 1205540302
---
I think that there's only a couple of words more often approached to "Archaeology" than "pyramids": don't you know which they are?
 If I say "lack of funds", don't tell me you're surprised!

Yes, in archaeological world almost everybody complains that the "Research has no funds", "that excavation can't go on because there are no money","the publication of finds is expensive, it's better wait to do it(?!?!): we're short of funds" and I can continue listing lots of phrases like these.

We all know that the funds for Cultural Heritage are not enough almost everywhere and there are no great hopes that in future things will get better.
However there are lots of funds that probably could be spent in a better way: I speak of software naturally.
In a world constantly lack of funds it's a real foolish act to go on choosing of spending lots of money in something you could get freely.

Yes, there are lots of good and very simple alternatives both about Operating System, thinking to one of Ubuntu's distributions, both about the most commonly used applications like office's ones and GIS' ones. So why only a few people in archaeology using OpenOffice, that you can find already installed with Linux systems, why there are people who goes on using very expensive GIS application, when there are so many and so good free(Grass, QGis)?

There could be someone who could say: "Linux is matter of programmers","free software geek's stuff". It's not true at all: it's above all a problem of habit! You think to know something(Do you really think to know it?Are you sure????) and don't want to go above the routine schemes.

Think about this when you must use a not free and, usually, very expensive software for which it could be a free alternative: "The money spent for this software could have payed my bills this month".
