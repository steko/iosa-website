---
layout: single
title: ! 'COMMUNIA Conference "University and CyberSpace": I''m going'
author: steko
created: 1276794720
---
There is a great conference in Turin in 11 days: [COMMUNIA Conference "University and CyberSpace"](http://communia-project.eu/). The list of speakers is just amazing, if you would like to talk about open archaeology and cultural heritage in general, please come and find me.
