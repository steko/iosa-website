---
layout: single
title: A Protocol for Implementing Open Access Data
author: steko
created: 1198003280
---
Some days ago, the <a href="http://sciencecommons.org/">Science Commons</a> initiative published this first version of their memo (think of this like a sort of RFC) named “<a href="http://sciencecommons.org/projects/publishing/open-access-data-protocol/">Protocol for Implementing Open Access Data</a>”. It's not a document describing a technical internet standard, but rather gives the minimum requirements for releasing and distributing data and databases as Open Access, as defined by the Open Knowledge Definition and the Budapest Declaration on Open Access.
It shouldn't be too hard to adapt this document to fit the need of archaeologists who are interested in sharing their data - a recurrent issue at least in some countries, where the legal status of archaeological data is quite unclear. I'm thinking about Italy when I write “<em>unclear</em>”: we have even problems understanding <strong>who</strong> is the owner of the rights on archaeological excavation data, and if any rights actually exist.
This is an important topic to discuss, because without access to data, most software is <em>useless</em>, no matter if it is free software. I hope we can get the situation more clear in the next workshop in Padua.
<strong>Update:</strong> <a href="http://www.alexandriaarchive.org/blog/?p=85">Eric Kansa goes more in detail</a> (and from an USA point of view) discussing the benefits and problems of the Science Commons approach to open access data. The basic problem I see, after some days of further reading, is that the protocol is mostly <em>negative</em>, while we need simple, <em>positive</em> statements that explain what has to be done. I'll write more on this later.
