---
layout: single
title: Archaeological Computing Newsletter
author: steko
created: 1122226882
---
This newsletter has been in production in paper form since 1985. In this time it has been widely recognised within its field as an important reference source.
From issue 61 (December 2004), ACN will be published as a six-monthly supplement to the journal Archeologia e Calcolatori. The Web version of the Newsletter does not yet contain content, but includes lists of contents of the most recent issues.
