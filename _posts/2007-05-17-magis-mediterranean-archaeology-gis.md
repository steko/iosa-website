---
layout: single
title: MAGIS Mediterranean Archaeology GIS
author: steko
created: 1179412877
---
<p>MAGIS is an inventory of regional archaeological survey projects in the greater Mediterranean region. The website features a spatial search engine, a database search, and data entry interface for registered scholars.</p>

<p>What's special about MAGIS is that its whole infrastructure is based on free software:</p>

- Apache might seem an obvious choice, and it works best with PHP & MySQL. The server is not GNU/Linux but an Apple Xserve.
- [UMN Mapserver](http://mapserver.gis.umn.edu), by far the most advanced web mapping server in the world. Currently MAGIS doesn't make use of any shiny AJAX mapping tools like <a href="http://www.openlayers.org">OpenLayers</a>, but the spatial search engine is however fully functional, with quick zooming for all Mediterranean countries.
- [GRASS](http://grass.itc.it) was used to process elevation data from the USGS EROS Data Center. On the other side, ArcGIS was used to process all vector data. It would be interesting to know what are the reasons behind this double choice, and what are the missing functionalities in GRASS for vector data processing.

<p>MAGIS is a project by the Collaboratory for GIS and Mediterranean Archaeology.</p>
