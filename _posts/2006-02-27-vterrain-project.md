---
layout: single
title: VTerrain Project
author: steko
created: 1141059567
---
The goal of VTP is to foster the creation of tools for easily constructing any part of the real world in interactive, 3D digital form.

This goal will require a synergetic convergence of the fields of CAD, GIS, visual simulation, surveying and remote sensing.  VTP gathers information and tracks progress in areas such as procedural scene construction, feature extraction, and rendering algorithms.  VTP writes and supports a set of software tools, including an interactive runtime environment (VTP Enviro).  The tools and their source code are freely shared to help accelerate the adoption and development of the necessary technologies.
