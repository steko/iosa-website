---
layout: single
title: QGIS 0.8 released
author: steko
created: 1167757498
---
The QGIS development team released version 0.8 (&#39;Titan&#39;) on December 29th. At present this is primarily a source release for those that want to build QGIS. Packages for most Linux distributions, Windows, and Mac OS X are being assembled and should be available in the next few days. <p> In addition to the source, they currently have available packages for Debian Testing (Etch) and SuSE 10.2. </p> All packages and source are available at <a href="http://download.qgis.org/">http://download.qgis.org</a>
