---
layout: single
title: IDA
author: steko
created: 1144943551
---
Ida is an open-source documentation tool especially designed for culture historical use that is being developed in 3D-bridge project. It is *not* an image database nor it is a document database. IDA allows different perspectives to the same material and it offers tools to the documentation of the research process. The aim is to provide system, that allows you to own the research material. More precisely, you can act like you owned the material. This means, that you could treat the material freely,you can add your own notes to the material and you can organise it as you like without causing any interference to the original data. 
