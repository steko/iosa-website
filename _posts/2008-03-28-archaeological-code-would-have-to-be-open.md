---
layout: single
title: Archaeological code would have to be OPEN
author: luca
created: 1206703765
---
There are lots of people assuming archaeological users are not an interesting market(and probably they're right), in particular if they cannot access the money and the projects carried on with government and big institutions, like universities and research groups. In this way the "poor" ordinary archaeologist couldn't hope having updated and modern software instruments, because the rare modern ones own to private companies that sell them at really high prices.

There's a way to get out the nightmare of not having modern tools for our researches and jobs: referring to open source software.
If people developing archaeological software release it with open licences, it's possible to maintain those tools updated, because everyone who needs them can use them and everyone that thinks they're not enough good, can improve them.
This is maybe the most important thing: the flexibility open code allows! How many times have you thought during your job that you would have needed to improve, make faster or implement some operations the software you're using doesn't allow?

Open Code allows you, or some programmer  you could get in touch with, implementing new functions, providing new tools and correcting some aspects you don't like.
Open Code allows to compare solutions to problems already found with other ones you're reflecting about, in this  way you could refine the searching instruments day by day and you're not obliged to go on using something you cannot make more suitable to your needs.

The free stream of ideas and instruments is the only way for people, who cannot afford commercial softwares, to create a real changing in the relationship between archaeology and computer science. It must not be only a luxury product for who has lots of money to spend in it, it's the right we would have to make our job in the best way because a just knowledge of the Past is a benefit for everybody.

We have to trust free software, because it's the only way to escape market's constraints. Escaping them and their moods it would be a good step towards a better archaeology.
