---
layout: single
title: CAA UK 2010 - 2nd Call for Papers and Registration Now Open!
author: steko
created: 1259007009
---
Through <a href="http://groups.google.com/group/antiquist/" title="Antiquist discussion list">Antiquist</a>:
<blockquote>
<p>Please find below the Call for Papers for the 2010 CAA UK being hosted at UCL, London. We are still accepting abstracts and the deadline is the end of this week. We have opened registration, so please visit the website at http://www.ucl.ac.uk/caauk/ and download and return the registration form.</p>
<p>Many thanks and we look forward to seeing you there!</p>
</blockquote>
For more information please visit http://www.ucl.ac.uk/caauk/. Any queries please email caauk@ucl.ac.uk
